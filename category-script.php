<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
use Magento\Framework\App\Bootstrap;
require __DIR__ . '/app/bootstrap.php';

$bootstrap = Bootstrap::create(BP, $_SERVER);

$objectManager = $bootstrap->getObjectManager();

$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$baseUrl = $storeManager->getStore()->getBaseUrl();
$categories = $objectManager->get('\Magento\Catalog\Helper\Category')->getStoreCategories();
$categoryRepoObj = $objectManager->get('\Magento\Catalog\Model\CategoryRepository');
$_outputhelper = $objectManager->get('Magento\Catalog\Helper\Output');
$html = ""; ?>
		<?php $categories = $objectManager->get('\Magento\Catalog\Helper\Category')->getStoreCategories(); ?>
        <?php foreach($categories as $category): ?>
        <?php $_category = $objectManager->create('Magento\Catalog\Model\Category')->load($category->getId()); ?>
        <?php if(!$_category->getCategoryOnHomepage()): ?>
        <?php $_category = $objectManager->create('Magento\Catalog\Model\Category')->load($_category->getId()); ?>
        <?php $html .= "<li class='level0 nav-1 first level-top parent ui-menu-item mega-menu-main catalog-category-view womens'>
			<a id='ui-id-3' class='level-top ui-corner-all' tabindex='-1' href='".$_category->getUrl()."'>
				<span class='ui-menu-icon ui-icon ui-icon-carat-1-e'></span>
				<span>".$_category->getName()."</span>
			</a>
			<ul class='level0 submenu mega-menu ui-menu ui-widget ui-widget-content ui-corner-all' style='display: none; top: 47px; left: 174px;' aria-expanded='false' aria-hidden='true'>
				<li class='activities-menu level1 nav-1-1 first ui-menu-item'>
					<div>"; ?>
						<?php 
						$categoryObj = $categoryRepoObj->get($category->getId());
						$subcategories = $categoryObj->getChildrenCategories();
						?>
						 <?php $html .= "<div class='col-sm-5 featured-categories'>
							
								<h4 class='filterproduct-title'><span class='content'>Featured Categories</span></h4>
								<div class='col-sm-12 nopadding'>" ?>
									<?php if(count($subcategories)): ?>
									<?php $count = 0; ?>
									<?php foreach($subcategories as $subcategorie): ?>
									<?php if($count == 6) break; ?>
									<?php 
									$_subcategorie = $objectManager->create('Magento\Catalog\Model\Category')->load($subcategorie->getId());
									?>
										 <?php $html .= "<div class='item col-sm-4 col-xs-6 nopadding'>
											<div class='imgcount'>
												<a class='cat-link' href='".$_subcategorie->getUrl()."'>
													<div class='category-title category-title-text'>
														<span class='mega-name'>".$_subcategorie->getName()."</span>
														<span class='mega-icon'>
															<i class='icon-arrow-right'></i>
														</span>
													</div>
													<img src='".$_subcategorie->getImageUrl()."' alt='' width='100' height='100'>
												</a>
											</div>
										</div>" ?>
										<?php $count++; ?>
									<?php endforeach; ?>
									<?php endif; ?>
									 <?php $html .= "<div class='sales-block-bottom'>
										<a class='button seeall' href='".$_category->getUrl()."'>See all categories</a>
									</div>
								</div>
								<div class='clear'></div>
							
						</div>
                   <div class='col-sm-4 activities-brands'>";
                   $blockId = 'top-menu-popular-brands';
                   $cms_block_content = '';
                   if ($blockId)
                   {
					   $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
					   $blockFactory = $objectManager->get('\Magento\Cms\Model\BlockFactory');$filterProvider = $objectManager->get('\Magento\Cms\Model\Template\FilterProvider');
					   $storeId = $storeManager->getStore()->getId();
					   $cms_block = $blockFactory->create();
					   $cms_block->setStoreId($storeId)->load($blockId);
					   $cms_block_content = $filterProvider->getBlockFilter()->setStoreId($storeId)->filter($cms_block->getContent());
				}
				$html .= $cms_block_content;
                    
                $html .="</div>
               <div class='col-sm-3 mega-submenu-list'>";
                   $blockId = 'top-menu-popular-searches';
                   $cms_block_content = '';
                   if ($blockId)
                   {
					   $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
					   $blockFactory = $objectManager->get('\Magento\Cms\Model\BlockFactory');$filterProvider = $objectManager->get('\Magento\Cms\Model\Template\FilterProvider');
					   $storeId = $storeManager->getStore()->getId();
					   $cms_block = $blockFactory->create();
					   $cms_block->setStoreId($storeId)->load($blockId);
					   $cms_block_content = $filterProvider->getBlockFilter()->setStoreId($storeId)->filter($cms_block->getContent());
				}
				$html .= $cms_block_content;
                    
                $html .="
                </div>

        </div>
           
        </li>
    </ul>"; ?>
		<?php endif; ?>
		<?php endforeach; ?>
		
		
 <?php $html .= "<li class='level0 nav-1 first level-top parent ui-menu-item mega-menu-main catalog-category-view activities'>
	<a id='ui-id-3' class='level-top ui-corner-all' tabindex='-1' href='".$baseUrl."work-safety.html'>
		<span class='ui-menu-icon ui-icon ui-icon-carat-1-e'></span>
		<span>WORK &amp; SAFETY</span>
	</a>
	<ul class='level0 submenu mega-menu ui-menu ui-widget ui-widget-content ui-corner-all loop megamenu-owl-carousel activities-megaMenu catalog-category-view owl-carousel owl-theme owl-hidden owl-loaded' style='display: none;' aria-hidden='true' aria-expanded='false'>
		<div class='owl-stage-outer ui-menu-item' role='presentation'>
			<div class='owl-stage' style='width: 2004.75px; transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s;'>
				<div class='owl-item' style='width: 212.75px; margin-right: 10px;'>
					<li class='item'>
						<div class='item col-md-12 nopadding'>
							<div class='imgcount'>
								<div class='category-title category-title-text'>
									<span class='mega-name'>Men’s boots </span>
									<span class='mega-icon'><i class='icon-arrow-right'></i></span>
								</div>
								<a class='cat-link' href='#'>
									<img src='http://stage.shoesensation.com/pub/media/wysiwyg/Mega_Menu/boots.jpg' alt=' ' width='100px' height='100px'>
								</a>
							</div>
						</div>
					</li>
				</div>
				<div class='owl-item' style='width: 212.75px; margin-right: 10px;'>
					<li class='item'>
						<div class='item col-md-12 nopadding'>
							<div class='imgcount'>
								<div class='category-title category-title-text'>
									<span class='mega-name'>Women’s boots</span>
									<span class='mega-icon'><i class='icon-arrow-right'></i></span>
								</div>
								<a class='cat-link' href='#'>
									<img src='http://stage.shoesensation.com/pub/media/wysiwyg/Mega_Menu/boots.jpg' width='100px' height='100px'>
								</a>
							</div>
						</div>
					</li>
				</div>
				<div class='owl-item' style='width: 212.75px; margin-right: 10px;'>
					<li class='item'>
						<div class='item col-md-12 nopadding'>
							<div class='imgcount'>
								<div class='category-title category-title-text'>
									<span class='mega-name'>Girls’ sandals</span>
									<span class='mega-icon'><i class='icon-arrow-right'></i></span>
								</div>
								<a class='cat-link' href='#'>
									<img src='http://stage.shoesensation.com/pub/media/wysiwyg/Mega_Menu/boots.jpg' width='100px' height='100px'>
								</a>
							</div>
						</div>
					</li>
				</div>
				<div class='owl-item' style='width: 212.75px; margin-right: 10px;'>
					<li class='item'>
						<div class='item col-md-12 nopadding'>
							<div class='imgcount'>
								<div class='category-title category-title-text'>
									<span class='mega-name'>Boys’ sneakers</span>
									<span class='mega-icon'><i class='icon-arrow-right'></i></span>
								</div>
								<a class='cat-link' href='#'>
									<img src='http://stage.shoesensation.com/pub/media/wysiwyg/Mega_Menu/boots.jpg' width='100px' height='100px'>
								</a>
							</div>
						</div>
					</li>
				</div>
				<div class='owl-item' style='width: 212.75px; margin-right: 10px;'>
					<li class='item'>
						<div class='item col-md-12 nopadding'>
							<div class='imgcount'>
								<div class='category-title category-title-text'>
									<span class='mega-name'>Men’s boots</span>
									<span class='mega-icon'><i class='icon-arrow-right'></i></span>
								</div>
								<a class='cat-link' href='#'>
									<img src='http://stage.shoesensation.com/pub/media/wysiwyg/Mega_Menu/boots.jpg' width='100px' height='100px'>
								</a>
							</div>
						</div>
					</li>
				</div>
				<div class='owl-item' style='width: 212.75px; margin-right: 10px;'>
					<li class='item'>
						<div class='item col-md-12 nopadding'>
							<div class='imgcount'>
								<div class='category-title category-title-text'>
									<span class='mega-name'>Women’s boots</span>
									<span class='mega-icon'><i class='icon-arrow-right'></i></span>
								</div>
								<a class='cat-link' href='#'>
									<img src='http://stage.shoesensation.com/pub/media/wysiwyg/Mega_Menu/boots.jpg' width='100px' height='100px'>
								</a>
							</div>
						</div>
					</li>
				</div>
				<div class='owl-item' style='width: 212.75px; margin-right: 10px;'>
					<li class='item'>
						<div class='item col-md-12 nopadding'>
							<div class='imgcount'>
								<div class='category-title category-title-text'>
									<span class='mega-name'>Girls’ sandals</span>
									<span class='mega-icon'><i class='icon-arrow-right'></i></span>
								</div>
								<a class='cat-link' href='#'>
									<img src='http://stage.shoesensation.com/pub/media/wysiwyg/Mega_Menu/boots.jpg' width='100px' height='100px'>
								</a>
							</div>
						</div>
					</li>
				</div>
				<div class='owl-item' style='width: 212.75px; margin-right: 10px;'>
					<li class='item'>
						<div class='item col-md-12 nopadding'>
							<div class='imgcount'>
								<div class='category-title category-title-text'>
									<span class='mega-name'>Boys’ sneakers</span>
									<span class='mega-icon'><i class='icon-arrow-right'></i></span>
								</div>
								<a class='cat-link' href='#'>
									<img src='http://stage.shoesensation.com/pub/media/wysiwyg/Mega_Menu/boots.jpg' width='100px' height='100px'>
								</a>
							</div>
						</div>
					</li>
				</div>
				<div class='owl-item' style='width: 212.75px; margin-right: 10px;'>
					<li class='item'>
						<div class='item col-md-12 nopadding'>
							<div class='imgcount'>
								<div class='category-title category-title-text'>
									<span class='mega-name'>Men’s boots</span>
									<span class='mega-icon'><i class='icon-arrow-right'></i></span>
								</div>
								<a class='cat-link' href='#'>
									<img src='http://stage.shoesensation.com/pub/media/wysiwyg/Mega_Menu/boots.jpg' width='100px' height='100px'>
								</a>
							</div>
						</div>
					</li>
				</div>
			</div>
		</div>
	</ul>
</li>
"; 
echo $file_name = getcwd().'/app/design/frontend/Kensium/Shoesensation/Magento_Theme/templates/html/topmenu_html.phtml';
$file = fopen($file_name, "w");
echo fwrite($file,$html);
fclose($file);

?>
