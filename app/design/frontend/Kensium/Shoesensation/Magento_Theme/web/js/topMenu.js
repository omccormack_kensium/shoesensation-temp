if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    jQuery('.megamenu-owl-carousel').owlCarousel({
        stagePadding: 50,
        loop: true,
        margin: 10,
        nav: true,
        responsive: {
            360: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 4
            }
        }
    });

}else{
    jQuery('.megamenu-owl-carousel').owlCarousel({
        stagePadding: 0,
        loop: true,
        margin: 10,
        nav: true,
        responsive: {
            360: {
                items: 2
            },
            600: {
                items: 4
            },
            1000: {
                items: 7
            }
        }
    });
}
totalmenuItems = jQuery('.activities-megaMenu').find('.owl-item').size();
clonedmenuItems = jQuery('.activities-megaMenu').find('.owl-item.cloned').size();
menuItems = totalmenuItems - clonedmenuItems;
if(menuItems <= 7){
    jQuery('.activities-megaMenu').find('.owl-controls').hide();
}