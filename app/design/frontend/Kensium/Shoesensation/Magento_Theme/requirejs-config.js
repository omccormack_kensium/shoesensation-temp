var config = {
    map: {
        '*': {
            kensticky:      'Magento_Theme/js/jquery.sticky',
            kensiumtabs:    'Magestore_Shopbybrand/js/easy-responsive-tabs',
            kenslider:      'Magento_Theme/js/owl.carousel'
        }
    },
    shim: {
        kenslider: {
            deps: ['jquery']
        }
    }
};
