<?php
/**
 * Copyright © 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\OrderEditor\Controller\Adminhtml\Form;

class Load extends \Magento\Framework\App\Action\Action
{
    /**
     * Page factory
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;

    /**
     * Raw factory
     *
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $rawFactory;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;
    
    /**
     * Order Editor helper
     *
     * @var \MageWorx\OrderEditor\Helper\Data
     */
    protected $helperData;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\Controller\Result\RawFactory $rawFactory,
        \MageWorx\OrderEditor\Helper\Data $helperData,
        \Magento\Framework\Registry $registry
    ) {
        $this->rawFactory = $rawFactory;
        $this->pageFactory = $pageFactory;
        $this->helperData = $helperData;
        $this->coreRegistry = $registry;
        return parent::__construct($context);
    }
    /**
     * Path to countries and regions grid template
     *
     * @var string
     */
//    protected $_template = 'address\form.phtml';

    /**
     * Edit order address form
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $blockId = $this->getRequest()->getParam('block_id');
        $orderId = $this->getRequest()->getParam('order_id');

        $order = $this->_objectManager->create('Magento\Sales\Model\Order')->load($orderId);
        if (!$order) {
            return $this;
        }

        $this->coreRegistry->register('ordereditor_order', $order);

        if ($blockId == 'billing_address') {
            $addressId = $order->getBillingAddressId();
        } elseif ($blockId == 'shipping_address') {
            $addressId = $order->getShippingAddressId();
        }

        if (isset($addressId)) {
            $address = $this->_objectManager->create('Magento\Sales\Model\Order\Address')->load($addressId);
            if ($address->getId()) {
                $this->coreRegistry->register('order_address', $address);
            }
        }

        $resultPage = $this->pageFactory->create();
        $resultPage->addHandle('ordereditor_load_block_' . $blockId);
        $formContainer = $resultPage->getLayout()->renderElement('content');
        return $this->rawFactory->create()->setContents($formContainer);
    }
}
