<?php
/**
 * Copyright © 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\OrderEditor\Controller\Adminhtml\Edit;

class CancelChanges extends \Magento\Framework\App\Action\Action
{
    /**
     * Helper
     *
     * @var \MageWorx\OrderEditor\Helper\Data
     */
    protected $helperData;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \MageWorx\OrderEditor\Helper\Data $helperData
    ) {
        $this->helperData = $helperData;
        return parent::__construct($context);
    }

    public function execute()
    {
        try {
            $orderId = $this->getRequest()->getParam('order_id');

            $this->helperData->resetPendingChanges($orderId);

            $this->messageManager->addSuccess(__('The order changes have been canceled'));

            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('sales/order/view', ['order_id' => $orderId]);
        } catch (\Exception $e) {
            $result = ['message' => $e->getMessage()];
            $this->getResponse()->setBody(json_encode($result));
        }
    }
}
