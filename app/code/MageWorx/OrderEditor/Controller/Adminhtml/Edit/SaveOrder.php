<?php
/**
 * Copyright © 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\OrderEditor\Controller\Adminhtml\Edit;

class SaveOrder extends \Magento\Framework\App\Action\Action
{
    /**
     * Page factory
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;

    /**
     * Raw factory
     *
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $rawFactory;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * Helper
     *
     * @var \MageWorx\OrderEditor\Helper\Data
     */
    protected $helperData;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \MageWorx\OrderEditor\Helper\Data $helperData,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\Controller\Result\RawFactory $rawFactory,
        \Magento\Quote\Model\Quote\Address\ToOrderAddress $quoteAddressToOrderAddress,
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->pageFactory = $pageFactory;
        $this->rawFactory = $rawFactory;
        $this->helperData = $helperData;
        $this->quoteAddressToOrderAddress = $quoteAddressToOrderAddress;
        return parent::__construct($context);
    }

    public function execute()
    {
        try {
            $orderId = $this->getRequest()->getParam('order_id');
            $order = $this->_objectManager->create('Magento\Sales\Model\Order')->load($orderId);
            $this->coreRegistry->register('ordereditor_order', $order);
            $quote = $this->_objectManager->get('MageWorx\OrderEditor\Model\Edit\Order')->getQuoteByOrder($order);

            $pendingChanges = $this->helperData->getPendingChanges($orderId);

            if ($pendingChanges) {
                $origOrder = clone $order;

                $quote = $this->_objectManager->get('MageWorx\OrderEditor\Model\Edit\Quote')
                    ->applyDataToQuote($quote, $pendingChanges);
                $this->_objectManager->get('MageWorx\OrderEditor\Model\Edit\Order')
                    ->saveOrder($quote, $order, $pendingChanges);

                $invoices = $order->getInvoiceCollection();
                if ($order->getGrandTotal() > ($origOrder->getGrandTotal() - $origOrder->getBaseTotalRefunded()) &&
                    count($invoices)) { // Create invoice if needed
                    $this->_objectManager->get('MageWorx\OrderEditor\Model\Edit\Invoice')
                        ->invoiceChanges($origOrder, $order);
                } elseif ($order->getGrandTotal() < ($origOrder->getGrandTotal() - $origOrder->getBaseTotalRefunded()) &&
                    count($invoices)) { // Create refund if needed
                    $this->_objectManager->get('MageWorx\OrderEditor\Model\Edit\Creditmemo')
                        ->refundChanges($origOrder, $order);
                }

                $this->helperData->resetPendingChanges($orderId);
            }

            $this->messageManager->addSuccess(__('The order changes have been saved'));

            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('sales/order/view', ['order_id' => $orderId]);
        } catch (\Exception $e) {
            $this->messageManager->addError(
                __('An error occured while saving the order' . $e->getMessage())
            );
            $this->_redirect('sales/order/view', ['order_id' => $orderId]);
            return;
        }
    }
}
