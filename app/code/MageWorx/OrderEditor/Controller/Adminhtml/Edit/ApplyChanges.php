<?php
/**
 * Copyright © 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\OrderEditor\Controller\Adminhtml\Edit;

class ApplyChanges extends \Magento\Framework\App\Action\Action
{
    /**
     * Page factory
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;

    /**
     * Raw factory
     *
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $rawFactory;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * Helper
     *
     * @var \MageWorx\OrderEditor\Helper\Data
     */
    protected $helperData;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \MageWorx\OrderEditor\Helper\Data $helperData,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\Controller\Result\RawFactory $rawFactory,
        \Magento\Quote\Model\Quote\Address\ToOrderAddress $quoteAddressToOrderAddress,
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->pageFactory = $pageFactory;
        $this->rawFactory = $rawFactory;
        $this->helperData = $helperData;
        $this->quoteAddressToOrderAddress = $quoteAddressToOrderAddress;
        return parent::__construct($context);
    }

    public function execute()
    {
        try {
            $orderId = $this->getRequest()->getParam('order_id');
            $blockId = $this->getRequest()->getParam('edited_block');

            $order = $this->_objectManager->create('Magento\Sales\Model\Order')->load($orderId);
            $this->coreRegistry->register('ordereditor_order', $order);

            $quoteId = $order->getQuoteId();
            $quote = $this->_objectManager->create('Magento\Quote\Model\Quote')
                ->setStoreId($order->getStoreId())
                ->load($quoteId);

            $data = [];
            $postData = $this->getRequest()->getPost();
            foreach ($postData as $key => $value) {
                $data[$key] = $value;
            }

            $pendingChanges = $this->helperData->addPendingChanges($orderId, $data);
            $quote = $this->_objectManager->get('MageWorx\OrderEditor\Model\Edit\Quote')
                ->applyDataToQuote($quote, $pendingChanges);

            $this->setOrderAddress($blockId, $quote, $order, $pendingChanges);

            $resultPage = $this->pageFactory->create();
            $blockData = $this->helperData->getBlockById($blockId);
            $block = $resultPage->getLayout()->createBlock($blockData['changedBlock']);

            // Render changed block preview
            if ($blockId == 'order_items') {
                $block->setNameInLayout('order_items');
                $this->setOrderItemRenderer($resultPage);
            } elseif ($blockId == 'shipping_address') {
                $block->setAddressType('shipping');
            } elseif ($blockId == 'billing_address') {
                $block->setAddressType('billing');
            }
            $block->setQuote($quote);
            $block->setOrder($order);

            $result[$blockId] = $block->toHtml();

            // Render temp totals (preview)
            $totals = $quote->getTotals();

            $tempTotalsBlock = $resultPage->getLayout()->createBlock(
                'MageWorx\OrderEditor\Block\Adminhtml\Sales\Order\Edit\Totals',
                'temp_totals',
                [
                    'data' => [
                        'totals' => $totals,
                        'order'  => $order,
                        'quote'  => $quote
                    ]
                ]
            );

            $tempTotalsHtml = $tempTotalsBlock->toHtml();
            $result['temp_totals'] = $tempTotalsHtml;

            $this->getResponse()->setBody(json_encode($result));
        } catch (\Exception $e) {
            $result = ['message' => $e->getMessage()];
            $this->getResponse()->setBody(json_encode($result));
        }
    }

    /**
     * Set changed data to order shipping/billing address
     */
    protected function setOrderAddress($blockId, $quote, $order, $pendingChanges)
    {
        if ($blockId == 'shipping_address') {
            $cloneQuote = clone $quote;
            $this->_objectManager->get('MageWorx\OrderEditor\Model\Edit\Quote')
                ->setAddress($cloneQuote, $pendingChanges[$blockId], 'shipping');
            $convertedQuoteAddress = $this->quoteAddressToOrderAddress->convert($cloneQuote->getShippingAddress());
            foreach ($convertedQuoteAddress->getData() as $key => $value) {
                $order->getShippingAddress()->setData($key, $value);
            }
        } elseif ($blockId == 'billing_address') {
            $cloneQuote = clone $quote;
            $this->_objectManager->get('MageWorx\OrderEditor\Model\Edit\Quote')
                ->setAddress($cloneQuote, $pendingChanges[$blockId], 'billing');
            $convertedQuoteAddress = $this->quoteAddressToOrderAddress->convert($cloneQuote->getBillingAddress());
            foreach ($convertedQuoteAddress->getData() as $key => $value) {
                $order->getBillingAddress()->setData($key, $value);
            }
        }
    }

    /**
     * Set order item renderers for changed order item grid
     */
    protected function setOrderItemRenderer($resultPage)
    {
        $resultPage
            ->getLayout()
            ->addBlock(
                'Magento\Sales\Block\Adminhtml\Order\View\Items\Renderer\DefaultRenderer',
                '',
                'order_items',
                'default'
            )
            ->setTemplate('MageWorx_OrderEditor::order/view/items/renderer/default.phtml');

        $renderers = [
            [
                'blockClass' => 'Magento\Sales\Block\Adminhtml\Items\Column\Qty',
                'template' => 'items/column/qty.phtml',
                'column' => 'column_qty',
            ],
            [
                'blockClass' => 'Magento\Sales\Block\Adminhtml\Items\Column\Name',
                'template' => 'items/column/name.phtml',
                'column' => 'column_name',
            ],
            [
                'blockClass' => 'Magento\Weee\Block\Adminhtml\Items\Price\Renderer',
                'template' => 'items/price/unit.phtml',
                'column' => 'column_price',
            ],
            [
                'blockClass' => 'Magento\Weee\Block\Adminhtml\Items\Price\Renderer',
                'template' => 'items/price/row.phtml',
                'column' => 'column_subtotal',
            ],
            [
                'blockClass' => 'Magento\Weee\Block\Adminhtml\Items\Price\Renderer',
                'template' => 'items/price/total.phtml',
                'column' => 'column_total',
            ],
        ];

        foreach ($renderers as $renderer) {
            $resultPage->getLayout()
                ->addBlock($renderer['blockClass'], $renderer['column'], 'order_items')
                ->setTemplate($renderer['template']);
            $resultPage->getLayout()->addToParentGroup($renderer['column'], 'column');
        }
    }
}
