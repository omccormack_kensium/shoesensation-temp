<?php
/**
 * Copyright © 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\OrderEditor\Controller\Adminhtml\Edit;

class ResetBlock extends \Magento\Framework\App\Action\Action
{
    /**
     * Page factory
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;

    /**
     * Helper
     *
     * @var \MageWorx\OrderEditor\Helper\Data
     */
    protected $helperData;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \MageWorx\OrderEditor\Helper\Data $helperData,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        $this->pageFactory = $pageFactory;
        $this->helperData = $helperData;
        return parent::__construct($context);
    }

    public function execute()
    {
        try {
            $orderId = $this->getRequest()->getParam('order_id');
            $blockId = $this->getRequest()->getParam('block_id');

            $order = $this->_objectManager->create('Magento\Sales\Model\Order')->load($orderId);
            $quoteId = $order->getQuoteId();
            $quote = $this->_objectManager->create('Magento\Quote\Model\Quote')
                ->setStoreId($order->getStoreId())
                ->load($quoteId);
            $pendingChanges = $this->helperData->unsetPendingChangesBlock($orderId, $blockId);

            if (!$pendingChanges || (count($pendingChanges) == 1 && isset($pendingChanges['form_key']))) {
                $result['temp_totals'] = '';
            } else {
                $quote = $this->_objectManager->get('MageWorx\OrderEditor\Model\Edit\Quote')
                    ->applyDataToQuote($quote, $pendingChanges);

                $resultPage = $this->pageFactory->create();
                $totals = $quote->getTotals();

                $tempTotalsBlock = $resultPage->getLayout()->createBlock(
                    'MageWorx\OrderEditor\Block\Adminhtml\Sales\Order\Edit\Totals',
                    'temp_totals',
                    ['data' => [
                        'totals' => $totals,
                        'order'  => $order,
                        'quote'  => $quote
                    ]]
                );

                $tempTotalsHtml = $tempTotalsBlock->toHtml();
                $result['temp_totals'] = $tempTotalsHtml;
            }

            $this->getResponse()->setBody(json_encode($result));
        } catch (\Exception $e) {
            $result = ['message' => $e->getMessage()];
            $this->getResponse()->setBody(json_encode($result));
        }
    }
}
