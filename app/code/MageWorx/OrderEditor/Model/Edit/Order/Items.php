<?php
/**
 * Copyright © 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\OrderEditor\Model\Edit\Order;

class Items
{
    /**
     * Object manager
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var \MageWorx\OrderEditor\Helper\Data
     */
    protected $helper;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \MageWorx\OrderEditor\Helper\Data $helper
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \MageWorx\OrderEditor\Helper\Data $helper
    ) {
        $this->objectManager = $objectManager;
        $this->helper = $helper;
    }

    /**
     * Retrieve order items collection
     *
     * @return \Magento\Sales\Model\ResourceModel\Order\Item\Collection
     */
    public function getItemsCollection($order, $quote)
    {
        $newItemArray = [];
        $newId = 1;

        $converter = $this->objectManager->create('Magento\Quote\Model\Quote\Item\ToOrderItem');
        $quoteItems = $quote->getAllVisibleItems();
        $orderItemsCollection = $this->objectManager->create('Magento\Sales\Model\ResourceModel\Order\Item\Collection')
            ->addIdFilter(-1);
        $changes = $this->helper->getPendingChanges($order->getId());
        if (isset($changes['quote_items'])) {
            $itemChanges = $changes['quote_items'];
        }

        // Sort items
        foreach ($quoteItems as $quoteItem) {
            // Validate items:
            // Removed item
            if (isset($itemChanges[$quoteItem->getItemId()]['action']) &&
                $itemChanges[$quoteItem->getItemId()]['action'] == 'remove') {
                continue;
            }

            // 0 qty item
            if (isset($itemChanges[$quoteItem->getItemId()]['qty']) &&
                $itemChanges[$quoteItem->getItemId()]['qty'] < 0.001) {
                continue;
            }

            // Child item
            if ($quoteItem->getParentItem()) {
                continue;
            }

            $newId++;
            $newItem = $converter->convert($quoteItem, $quoteItem->getData());

            $newItem->setId($quoteItem->getId() ? $quoteItem->getId() : $newId)
                ->setOrder($order);

            if (count($quoteItem->getChildren()) > 0) {
                $childArray = [];
                foreach ($quoteItem->getChildren() as $quoteItemChild) {
                    $newId++;
                    $newChild = $converter->convert($quoteItemChild, $quoteItemChild->getData())
                        ->setId($quoteItemChild->getId() ? $quoteItemChild->getId() : $newId)
                        ->setOrder($order)
                        ->setParentItemId($newItem->getId())
                        ->setParentItem($newItem);
                    $childArray[] = $newChild;
                }

                $newItem->setChildrenItems(
                    array_merge(
                        $childArray,                    // current item
                        $newItem->getChildrenItems()    // all items parent has before
                    )
                );
            }

            // add current item to array
            $newItemArray[] = $newItem;
        }

        foreach ($newItemArray as $orderItem) {
            try {
                $orderItemsCollection->addItem($orderItem);
            } catch (\Exception $e) {
                continue;
            }
        }

        return $orderItemsCollection;
    }
}
