<?php
/**
 * Copyright © 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\OrderEditor\Model\Edit;

class Order
{
    /**
     * Core event manager proxy
     *
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $eventManager = null;

    /**
     * Order Editor helper
     *
     * @var \MageWorx\OrderEditor\Helper\Data
     */
    protected $helperData;

    /**
     * Object manager
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * Order items which have already been saved
     *
     * @var array
     */
    protected $savedOrderItems = [];

    /**
     * The flag shows whether missed quote items have been checked
     *
     * @var bool
     */
    protected $quoteItemsAlreadyChecked = false;

    /**
     * Converter
     *
     * @var \MageWorx\OrderEditor\Model\Converter
     */
    protected $converter;

    /**
     * @var \Magento\Framework\DataObject\Copy
     */
    protected $objectCopyService;

    public function __construct(
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \MageWorx\OrderEditor\Helper\Data $helperData,
        \Magento\Quote\Model\Quote\Address\ToOrderAddress $quoteAddressToOrderAddress,
        \MageWorx\OrderEditor\Model\Converter $converter,
        \Magento\Framework\DataObject\Copy $objectCopyService,
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->eventManager = $eventManager;
        $this->objectManager = $objectManager;
        $this->helperData = $helperData;
        $this->quoteAddressToOrderAddress = $quoteAddressToOrderAddress;
        $this->converter = $converter;
        $this->objectCopyService = $objectCopyService;
        $this->coreRegistry = $coreRegistry;
    }

    /**
     * Get quote model by order
     *
     * @param \Magento\Sales\Model\Order $order
     * @return \Magento\Quote\Model\Quote | null
     */
    public function getQuoteByOrder(\Magento\Sales\Model\Order $order)
    {
        $this->checkOrderQuote($order);

        $quoteId = $order->getQuoteId();
        $storeId = $order->getStoreId();

        $quote = $this->objectManager->create('Magento\Quote\Model\Quote')->setStoreId($storeId)->load($quoteId);

        return $quote;
    }

    /**
     * Check whether the order's quote exists. If no then create it from the order
     *
     * @param \Magento\Sales\Model\Order $order
     * @return $this
     * @throws Exception
     */
    protected function checkOrderQuote(\Magento\Sales\Model\Order $order)
    {
        $quoteId = $order->getQuoteId();
        $quote = $this->objectManager->create('Magento\Quote\Model\Quote')
            ->setStoreId($order->getStoreId())
            ->load($quoteId);
        if ($quote && $quote->getId()) {
            return $this;
        }

        // Copy quote data
        $quote = $this->converter->orderToQuote($order);

        // Copy shipping address data
        if ($order->getShippingAddress()) {
            $shippingAddress = $quote->getShippingAddress();

            $this->objectCopyService->copyFieldsetToTarget(
                'sales_convert_order_address',
                'to_quote_address',
                $order->getShippingAddress(),
                $shippingAddress
            );

            $this->objectCopyService->copyFieldsetToTarget(
                'sales_convert_order',
                'to_quote_address_shipping',
                $order,
                $shippingAddress
            );
        }

        // Copy billing address
        if ($order->getBillingAddress()) {
            $billingAddress = $quote->getBillingAddress();

            $this->objectCopyService->copyFieldsetToTarget(
                'sales_convert_order_address',
                'to_quote_address',
                $order->getBillingAddress(),
                $billingAddress
            );
        }

        // Copy payment
        if ($order->getPayment()) {
            $payment = $quote->getPayment();
            $this->converter->orderPaymentToQuotePayment($order->getPayment(), $payment);
        }

        // Recreate shipping rates
        if ($quote->getShippingAddress() && !$quote->getShippingAddress()->getGroupedAllShippingRates()) {
            $quote->getShippingAddress()->setCollectShippingRates(true)->collectShippingRates();
        }

        $quote->save();

        $order->setQuoteId($quote->getId())->save();

        return $this;
    }

    /**
     * Save billing address
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Sales\Model\Order $order
     * @param array $changes
     * @return $this
     * @throws Exception
     */
    public function saveBillingAddress(\Magento\Quote\Model\Quote $quote, \Magento\Sales\Model\Order $order, $changes)
    {
        $cloneQuote = clone $quote;

        $this->objectManager->get('MageWorx\OrderEditor\Model\Edit\Quote')
            ->setAddress($cloneQuote, $changes, 'billing');
        $convertedQuoteAddress = $this->quoteAddressToOrderAddress->convert($cloneQuote->getBillingAddress());
        foreach ($convertedQuoteAddress->getData() as $key => $value) {
            $order->getBillingAddress()->setData($key, $value);
        }

        $order->getBillingAddress()->save();

        return $this;
    }

    /**
     * Save shipping address
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Sales\Model\Order $order
     * @param array $changes
     * @return $this
     * @throws Exception
     */
    public function saveShippingAddress(\Magento\Quote\Model\Quote $quote, \Magento\Sales\Model\Order $order, $changes)
    {
        $cloneQuote = clone $quote;

        $this->objectManager->get('MageWorx\OrderEditor\Model\Edit\Quote')
            ->setAddress($cloneQuote, $changes, 'shipping');
        $convertedQuoteAddress = $this->quoteAddressToOrderAddress->convert($cloneQuote->getShippingAddress());
        foreach ($convertedQuoteAddress->getData() as $key => $value) {
            $order->getShippingAddress()->setData($key, $value);
        }

        $order->getShippingAddress()->save();

        return $this;
    }

    /**
     * Save changed order products
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Sales\Model\Order $order
     * @param                            $changes
     * @return $this
     */
    public function saveOldOrderItems(\Magento\Quote\Model\Quote $quote, \Magento\Sales\Model\Order $order, $changes)
    {
        foreach ($changes as $itemId => $params) {
            $quoteItem = $quote->getItemById($itemId);
            $orderItem = $order->getItemByQuoteItemId($itemId);

            $orderItemQty = $this->getQtyRest($orderItem);

            if ((isset($params['action']) && $params['action'] == 'remove') ||
                (isset($params['qty']) && $params['qty'] < 1)) {
                if ($this->helperData->getEnablePermanentOrderItemRemoval() && !$orderItem->getQtyInvoiced()) {
                    if ($orderItem->getProductType() == 'bundle' || $orderItem->getProductType() == 'configurable') {
                        foreach ($orderItem->getChildrenItems() as $childOrderItem) {
                            $this->removeOrderItem($childOrderItem);
                        }
                    }
                    $this->removeOrderItem($orderItem);
                } else {
                    $this->returnOrderItem($order, $orderItem);
                    if ($orderItem->getProductType() == 'bundle' || $orderItem->getProductType() == 'configurable') {
                        foreach ($orderItem->getChildrenItems() as $childOrderItem) {
                            $this->cancelOrderItem($childOrderItem, ($this->getQtyRest($childOrderItem, false)));
                        }
                    }

                    $orderItem->setSubtotal(0)
                        ->setBaseSubtotal(0)
                        ->setTaxAmount(0)
                        ->setBaseTaxAmount(0)
                        ->setTaxPercent(0)
                        ->setDiscountAmount(0)
                        ->setBaseDiscountAmount(0)
                        ->setRowTotal(0)
                        ->setBaseRowTotal(0);
                }

                continue;
            }

            $origQtyOrdered = $orderItem->getQtyOrdered();
            $orderItem = $this->converter->itemToOrderItem($quoteItem, $orderItem);

            if (isset($params['qty']) && $params['qty'] != $orderItemQty) {
                $qtyDiff = $params['qty'] - $orderItemQty;

                if ($params['qty'] < $orderItemQty) {
                    $qtyToRemove = $orderItemQty - $params['qty'];
                    $orderItem->setQtyOrdered($origQtyOrdered);
                    $this->returnOrderItem($order, $orderItem, $qtyToRemove);
                } else {
                    $orderItem->setQtyOrdered($origQtyOrdered + $qtyDiff);
                }

                if ($orderItem->getProductType() == 'bundle' || $orderItem->getProductType() == 'configurable') {
                    foreach ($quote->getAllItems() as $childQuoteItem) {
                        if ($childQuoteItem->getParentItemId() != $quoteItem->getId()) {
                            continue;
                        }

                        $childQuoteItem->save();

                        $childOrderItem = $order->getItemByQuoteItemId($childQuoteItem->getId());
                        $childOrderItem->setParentItem($orderItem);
                        $origChildQtyOrdered = $childOrderItem->getQtyOrdered();
                        $childOrderItem = $this->converter->itemToOrderItem($childQuoteItem, $childOrderItem);

                        if ($params['qty'] < $orderItemQty) {
                            $qtyToRemove = $origChildQtyOrdered - $this->getQtyRest($childOrderItem, true);
                            $this->returnOrderItem($order, $childOrderItem, $qtyToRemove);
                            $childOrderItem->setQtyOrdered($origChildQtyOrdered);
                        } else {
                            $childQtyDiff = $qtyDiff * $childQuoteItem->getQty();
                            $childOrderItem->setQtyOrdered($origChildQtyOrdered + $childQtyDiff);
                        }

                        $childOrderItem->save();

                        $this->savedOrderItems[] = $childOrderItem->getItemId();
                    }
                }
            }

            $quoteItem->save();
            $orderItem->save();

            $this->savedOrderItems[] = $orderItem->getItemId();
        }

        return $this;
    }

    /**
     * Add new products to order
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Sales\Model\Order $order
     * @return $this
     */
    public function saveNewOrderItems(\Magento\Quote\Model\Quote $quote, \Magento\Sales\Model\Order $order)
    {
        foreach ($quote->getAllItems() as $quoteItem) {
            $orderItem = $order->getItemByQuoteItemId($quoteItem->getItemId());
            if ($orderItem && $orderItem->getItemId()) {
                continue;
            }

            $quoteItem->save();

            $orderItem = $this->converter->itemToOrderItem($quoteItem, $orderItem);
            $order->addItem($orderItem);
            $orderItem->save();

            $this->savedOrderItems[] = $orderItem->getItemId();
        }

        /** @var \Magento\Quote\Model\Quote\Item $childQuoteItem */
        foreach ($quote->getAllItems() as $childQuoteItem) {
            /** @var \Magento\Sales\Model\Order\Item $childOrderItem */
            $childOrderItem = $order->getItemByQuoteItemId($childQuoteItem->getItemId());

            /*** Add items relations for configurable and bundle products ***/
            if ($childQuoteItem->getParentItemId()) {
                /** @var \Magento\Sales\Model\Order\Item $parentOrderItem */
                $parentOrderItem = $order->getItemByQuoteItemId($childQuoteItem->getParentItemId());

                $childOrderItem->setParentItemId($parentOrderItem->getItemId());
                $childOrderItem->save();
            }
        }

        return $this;
    }

    /**
     * Apply all the changes to order and save it
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Sales\Model\Order $order
     * @param $changes
     * @return $this
     * @throws Exception
     */
    public function saveOrder(\Magento\Quote\Model\Quote $quote, \Magento\Sales\Model\Order $order, $changes)
    {
        if (isset($changes['billing_address'])) {
            $this->saveBillingAddress($quote, $order, $changes['billing_address']);
            unset($changes['billing_address']);
        }

        if (isset($changes['shipping_address'])) {
            $this->saveShippingAddress($quote, $order, $changes['shipping_address']);
            unset($changes['shipping_address']);
        }

        $this->savedOrderItems = [];

        if (isset($changes['product_to_add']) && !empty($changes['product_to_add'])) {
            $this->saveNewOrderItems($quote, $order);
        }

        if (isset($changes['quote_items'])) {
            $this->saveOldOrderItems($quote, $order, $changes['quote_items']);
        }
//
        $address = $quote->getIsVirtual() ? $quote->getBillingAddress() : $quote->getShippingAddress();

        $orderConverted = $this->objectManager->get('Magento\Quote\Model\Quote\Address\ToOrder')->convert($address);
        foreach ($orderConverted->getData() as $key => $value) {
            $order->setData($key, $value);
        }

        foreach ($quote->getAllVisibleItems() as $quoteItem) {
            $orderItem = $order->getItemByQuoteItemId($quoteItem->getItemId());

            if (isset($orderItem) && (in_array($orderItem->getItemId(), $this->savedOrderItems) ||
                    !isset($changes['quote_items']))) {
                continue;
            }

            $orderItem = $this->converter->itemToOrderItem($quoteItem, $orderItem);
            $orderItem->setOrderId($order->getId());
            $orderItem->save();

            $quoteChildrens = $quoteItem->getChildren();
            $orderChildrens = [];
            foreach ($quoteChildrens as $childQuoteItem) {
                $childOrderItem = $order->getItemByQuoteItemId($childQuoteItem->getItemId());

                if (isset($childOrderItem) && in_array($childOrderItem->getItemId(), $this->savedOrderItems)) {
                    continue;
                }

                $childOrderItem = $this->converter->itemToOrderItem($childQuoteItem, $childOrderItem);
                $childOrderItem->setOrderId($order->getId());
                $childOrderItem->setParentItem($orderItem);
                $childOrderItem->setParentItemId($orderItem->getId());
                $childOrderItem->save();
                $orderChildrens[] = $childOrderItem;
            }

            if (!empty($orderChildrens)) {
                foreach ($orderChildrens as $child) {
                    $orderItem->addChildItem($child);
                }
                $orderItem->save();
            }
        }

        // Collect order all items qty
        $changes['total_qty_ordered'] = 0;
        foreach ($order->getAllItems() as $orderItem) {
            $changes['total_qty_ordered'] += $orderItem['qty_ordered'] - $orderItem['qty_canceled'];
        }
        $order->addData($changes);

        $quote->save();
        $order->save();

        return $this;
    }

    /**
     * Cancel specific qty of order item
     *
     * @param \Magento\Sales\Model\Order\Item   $orderItem
     * @param int|null                          $qtyToCancel
     * @return \Magento\Sales\Model\Order\Item
     */
    public function cancelOrderItem(\Magento\Sales\Model\Order\Item $orderItem, $qtyToCancel = null)
    {
        if ($orderItem->getStatusId() !== \Magento\Sales\Model\Order\Item::STATUS_CANCELED) {
            if (!$qtyToCancel) {
                $qtyToCancel = $orderItem->getQtyToCancel();
            }

            $origQtyCancelled = $orderItem->getQtyCanceled();
            $maxQty = max($orderItem->getQtyShipped(), $orderItem->getQtyInvoiced());
            $orderItem->setQtyCanceled($this->getQtyRest($orderItem, false) - $maxQty);
            $this->eventManager->dispatch('sales_order_item_cancel', ['item' => $orderItem]);
            $orderItem->setQtyCanceled($origQtyCancelled + $qtyToCancel);

            $orderItem->setTaxCanceled(
                $orderItem->getTaxCanceled() +
                $orderItem->getBaseTaxAmount() * $orderItem->getQtyCanceled() / $orderItem->getQtyOrdered()
            );
            $orderItem->setHiddenTaxCanceled(
                $orderItem->getHiddenTaxCanceled() +
                $orderItem->getHiddenTaxAmount() * $orderItem->getQtyCanceled() / $orderItem->getQtyOrdered()
            );
        }

        return $orderItem;
    }

    /**
     * Remove completely order item from order
     *
     * @param \Magento\Sales\Model\Order\Item   $orderItem
     * @return $this
     */
    public function removeOrderItem(\Magento\Sales\Model\Order\Item $orderItem)
    {
        $orderItem->delete();

        return $this;
    }

    /**
     * Remove specific qty of order item from order
     *
     * @param \Magento\Sales\Model\Order        $order
     * @param \Magento\Sales\Model\Order\Item   $orderItem
     * @param int|null                          $qtyToReturn
     * @return $this
     */
    public function returnOrderItem(
        \Magento\Sales\Model\Order $order,
        \Magento\Sales\Model\Order\Item $orderItem,
        $qtyToReturn = null
    ) {
        if ($qtyToReturn === null) {
            $qtyToReturn = $orderItem->getQtyToRefund() + $orderItem->getQtyToCancel();
        }

        if ($qtyToReturn > 0 && $orderItem->getQtyToCancel() > 0) {
            if ($orderItem->getParentItem()) {
                $qtyToCancel = $qtyToReturn;
                $qtyToReturn -= $qtyToCancel;
            } else {
                $qtyToCancel = min($qtyToReturn, $orderItem->getQtyToCancel());
                $qtyToReturn -= $qtyToCancel;
            }

            $this->cancelOrderItem($orderItem, $qtyToCancel);
        }

        if ($qtyToReturn > 0 && $orderItem->getQtyToRefund() > 0) {
            $this->refundOrderItem($orderItem, $qtyToReturn);
        }

        return $this;
    }

    /**
     * Refund specific qty of order item
     *
     * @param \Magento\Sales\Model\Order\Item   $orderItem
     * @param  int|null                         $qtyToRefund
     * @return $this
     */
    public function refundOrderItem(\Magento\Sales\Model\Order\Item $orderItem, $qtyToRefund)
    {
        $cmModel = $this->objectManager->get('MageWorx\OrderEditor\Model\Edit\Creditmemo');
        $cmModel->addItemToRefund($orderItem->getId(), $qtyToRefund);

        if ($orderItem->getProductType() == 'bundle') {
            $orderItem->setQtyRefunded($qtyToRefund);
        }

        return $this;
    }

    /**
     * Get rest of available item qty.
     * qty = qty - canceled [ - refunded ] (optional)
     *
     * @param \Magento\Sales\Model\Order\Item $item
     * @param bool|false $excludeRefunded
     * @return float|int
     */
    protected function getQtyRest($item, $excludeRefunded = false)
    {
        $qty = $item->getOrigData('qty_ordered') - $item->getQtyCanceled();
        if ($excludeRefunded) {
            $qty -= $item->getQtyRefunded();
        }

        return $qty;
    }
}
