<?php
/**
 * Copyright © 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\OrderEditor\Model\Edit;

class Creditmemo
{
    /**
     * Order Editor helper
     *
     * @var \MageWorx\OrderEditor\Helper\Data
     */
    protected $helperData;

    /**
     * Object manager
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var \Magento\Sales\Model\Order\CreditmemoFactory;
     */
    protected $creditmemoFactory;

    /**
     * List of order totals that can be changed
     *
     * @var array
     */
    protected $availableTotals = [
        'shipping_tax_amount',
        'base_shipping_tax_amount',
        'base_shipping_tax_amount',
        'subtotal',
        'base_subtotal',
        'subtotal_incl_tax',
        'base_subtotal_incl_tax',
        'grand_total',
        'base_grand_total',
        'tax_amount',
        'base_tax_amount',
        'discount_amount',
        'base_discount_amount',
        'shipping_amount',
        'base_shipping_amount',
        'shipping_incl_tax',
        'base_shipping_incl_tax',
        'hidden_tax_amount',
        'base_hidden_tax_amount'
    ];

    /**
     * Array of order products to be refunded
     *
     * @var array
     */
    protected $itemsToRefund = [];

    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \MageWorx\OrderEditor\Helper\Data $helperData,
        \Magento\Sales\Model\Order\CreditmemoFactory $creditmemoFactory
    ) {
        $this->objectManager = $objectManager;
        $this->helperData = $helperData;
        $this->creditmemoFactory = $creditmemoFactory;
    }

    /**
     * Create creditmemo for order changes
     *
     * @param \Magento\Sales\Model\Order $origOrder
     * @param \Magento\Sales\Model\Order $newOrder
     * @return $this
     */
    public function refundChanges(\Magento\Sales\Model\Order $origOrder, \Magento\Sales\Model\Order $newOrder)
    {
        $cmData = [];
        $cmData['qtys'] = $this->getItemsToRefund();

        $creditmemo = $this->creditmemoFactory->createByOrder($newOrder, $cmData);

        foreach ($this->availableTotals as $code) {
            $diff = $origOrder->getData($code) - $newOrder->getData($code);
            if (!$diff) {
                continue;
            }

            $creditmemo->setData($code, $diff);
        }

        // Return refunded items to stock
        foreach ($creditmemo->getAllItems() as $creditmemoItem) {
            $creditmemoItem->setBackToStock(true);
        }

        $creditmemoManagement = $this->objectManager->create('Magento\Sales\Api\CreditmemoManagementInterface');
        $creditmemoManagement->refund($creditmemo);

        return $this;
    }

    /**
     * Add order item to be refunded
     *
     * @param $itemId
     * @param $qty
     * @return $this
     */
    public function addItemToRefund($itemId, $qty)
    {
        $this->itemsToRefund[$itemId] = $qty;

        return $this;
    }

    /**
     * Get all the order items to be refunded
     *
     * @return array
     */
    public function getItemsToRefund()
    {
        // To prevent creditmemo from adding items;
        if (!count($this->itemsToRefund)) {
            return [0 => 0];
        }

        return $this->itemsToRefund;
    }
}
