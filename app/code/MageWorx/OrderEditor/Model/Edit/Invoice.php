<?php
/**
 * Copyright © 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\OrderEditor\Model\Edit;

class Invoice
{
    /**
     * Order Editor helper
     *
     * @var \MageWorx\OrderEditor\Helper\Data
     */
    protected $helperData;

    /**
     * Object manager
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;
    /**
     * List of order totals that can be changed
     *
     * @var array
     */
    protected $availableTotals = [
        'shipping_tax_amount',
        'base_shipping_tax_amount',
        'base_shipping_tax_amount',
        'subtotal',
        'base_subtotal',
        'subtotal_incl_tax',
        'base_subtotal_incl_tax',
        'grand_total',
        'base_grand_total',
        'tax_amount',
        'base_tax_amount',
        'discount_amount',
        'base_discount_amount',
        'shipping_amount',
        'base_shipping_amount',
        'shipping_incl_tax',
        'base_shipping_incl_tax',
        'hidden_tax_amount',
        'base_hidden_tax_amount'
    ];

    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \MageWorx\OrderEditor\Helper\Data $helperData
    ) {
        $this->objectManager = $objectManager;
        $this->helperData = $helperData;
    }

    /**
     * Create invoice for order changes
     *
     * @param \Magento\Sales\Model\Order  $origOrder
     * @param \Magento\Sales\Model\Order  $newOrder
     * @param array                       $changes
     * @return $this
     */
    public function invoiceChanges(\Magento\Sales\Model\Order $origOrder, \Magento\Sales\Model\Order $newOrder)
    {
        $invoice = $this->objectManager->create('Magento\Sales\Model\Service\InvoiceService')
            ->prepareInvoice($newOrder);

        foreach ($this->availableTotals as $code) {
            $diff = $newOrder->getData($code) - $origOrder->getData($code);
            if (!$diff) {
                continue;
            }

            $invoice->setData($code, $diff);
        }

        $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
        $invoice->register();

        $transaction = $this->objectManager->create('Magento\Framework\DB\Transaction')
            ->addObject($invoice)
            ->addObject($invoice->getOrder());

        $transaction->save();

        $this->updateInvoicedItems($newOrder);

        return $this;
    }

    /**
     * Set correct totals for the items which have just been invoiced
     *
     * @param \Magento\Sales\Model\Order $order
     * @return $this
     */
    public function updateInvoicedItems(\Magento\Sales\Model\Order $order)
    {
        foreach ($order->getAllItems() as $orderItem) {
            $orderItem->setTaxInvoiced($orderItem->getTaxAmount());
            $orderItem->setBaseTaxInvoiced($orderItem->getBaseTaxAmount());

            $orderItem->setRowInvoiced($orderItem->getRowTotal());
            $orderItem->setBaseRowInvoiced($orderItem->getBaseRowTotal());

            $orderItem->save();
        }

        return $this;
    }
}
