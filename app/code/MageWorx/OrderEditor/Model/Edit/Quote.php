<?php
/**
 * Copyright © 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\OrderEditor\Model\Edit;

class Quote
{
    /**
     * Order Editor helper
     *
     * @var \MageWorx\OrderEditor\Helper\Data
     */
    protected $helperData;

    /**
     * Object manager
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \MageWorx\OrderEditor\Helper\Data $helperData,
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->objectManager = $objectManager;
        $this->helperData = $helperData;
        $this->coreRegistry = $coreRegistry;
    }

    /**
     * Apply all the changes to quote
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param array $data
     * @return \Magento\Quote\Model\Quote
     */
    public function applyDataToQuote(\Magento\Quote\Model\Quote $quote, array $data)
    {
        foreach ($data as $key => $value) {
            if ($key == 'quote_items') {
                $this->updateItems($quote, $value);
            } else if ($key == 'product_to_add') {
                $this->addNewItems($quote, $value);
            }
        }

        $quote->setTotalsCollectedFlag(false)->collectTotals();

        return $quote;
    }

    /**
     * Apply shipping/billing address to quote
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param $data
     * @param $addressType
     * @return $this
     */
    public function setAddress(\Magento\Quote\Model\Quote $quote, $data, $addressType)
    {
        $address = ($addressType == 'shipping') ? $quote->getShippingAddress() : $quote->getBillingAddress();
        $address->addData($data);

        // fix for street fields
        $streetArray = [];
        for ($i = 0; $i < 4; $i++) {
            if (isset($data['street[' . $i]) && $data['street[' . $i]) {
                $streetArray[$i] = $data['street[' . $i];
            }
        }
        $street = implode(chr(10), $streetArray);
        $streetData = ['street' => $street];
        $address->addData($streetData);
        // fix end

        return $this;
    }

    /**
     * Apply updated order items to quote
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param $data
     * @return $this
     */
    public function updateItems(\Magento\Quote\Model\Quote $quote, $data)
    {
        foreach ($data as $itemId => $params) {
            $quoteItem = $quote->getItemById($itemId);

            // Set empty action by default to prevent warnings, if no one action was specified
            if (!isset($params['action'])) {
                $params['action'] = '';
            }

            // If item not exist in quote, create a new one
            if (!$quoteItem && $params['action'] != 'remove') {
                $this->addNewItems($quote, [$itemId => $params]);
                $quoteItem = $quote->getItemById($itemId);
                if (!$quoteItem) {
                    continue;
                }
            }

            if ((isset($params['action']) && $params['action'] == 'remove')
                || ((isset($params['qty']) && $params['qty'] < 1))
            ) {
                $childrens = $quoteItem->getChildren();
                foreach ($childrens as $childQuoteItem) {
                    $quote->removeItem($childQuoteItem->getId());
                }
                $quote->removeItem($quoteItem->getId());
                continue;
            }

            if (isset($params['qty'])) {
                $quoteItem->setQty($params['qty']);
            }

            if (isset($params['custom_price']) && $params['custom_price'] > 0) {
                $quoteItem->setCustomPrice((float)$params['custom_price']);
                $quoteItem->setOriginalCustomPrice((float)$params['custom_price']);
            }

            $noDiscount = !isset($params['use_discount']);
            $quoteItem->setNoDiscount($noDiscount);
        }

        return $this;
    }

    /**
     * Apply newly added products to quote
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param $data
     * @return $this
     */
    public function addNewItems(\Magento\Quote\Model\Quote $quote, $data)
    {
        foreach ($data as $productId => $params) {
            if (isset($params['product_id'])) {
                $product = $this->objectManager->create("Magento\Catalog\Model\Product")
                    ->setStoreId($quote->getStoreId())
                    ->load($params['product_id']);
            } else {
                $product = $this->objectManager->create("Magento\Catalog\Model\Product")
                    ->setStoreId($quote->getStoreId())
                    ->load($productId);
            }
            if (!$product || !$product->getId()) {
                continue;
            }

            if (!isset($params['product'])) {
                $params['product'] = $product->getId();
            }

            $quote->addProduct(
                $product,
                $this->objectManager->create("Magento\Framework\DataObject")->addData($params)
            );
        }

        if ($this->coreRegistry->registry('orderseditor_order')) {
            $this->helperData->addPendingChanges($this->coreRegistry->registry('orderseditor_order')->getId(), [
                'product_to_add' => []
            ]);
        }

        return $this;
    }
}
