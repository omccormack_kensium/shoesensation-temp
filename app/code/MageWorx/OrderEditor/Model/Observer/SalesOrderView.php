<?php
/**
 * Copyright © 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\OrderEditor\Model\Observer;

class SalesOrderView implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * Object manager
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * Helper
     *
     * @var \MageWorx\OrderEditor\Helper\Data
     */
    protected $helperData;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;

    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \MageWorx\OrderEditor\Helper\Data $helperData
    ) {
        $this->objectManager = $objectManager;
        $this->helperData = $helperData;
    }

    /**
     * Reset all pending changes of the order on load of the order page
     *
     * @param  \Magento\Framework\Event\Observer $observer
     * @return \MageWorx\OrderEditor\Model\Observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $request = $observer->getEvent()->getControllerAction()->getRequest()->getParams();
        if (isset($request['order_id'])) {
            $orderId = $request['order_id'];
            $this->helperData->resetPendingChanges($orderId);
        }

        return $this;
    }
}
