<?php
/**
 * Copyright © 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\OrderEditor\Block\Adminhtml\Sales\Order\Changed;

use Magento\Eav\Model\AttributeDataFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Model\Order\Address as OrderAddress;

/**
 * Order history block
 * Class Info
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Address extends \Magento\Sales\Block\Adminhtml\Order\AbstractOrder
{
    /**
     * Address form template
     *
     * @var string
     */
    protected $_template = 'changed/address.phtml';

    /**
     * @var \Magento\Sales\Model\Order\Address\Renderer
     */
    protected $addressRenderer;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Helper\Admin $adminHelper
     * @param \Magento\Sales\Model\Order\Address\Renderer $addressRenderer
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Admin $adminHelper,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        array $data = []
    ) {
        $this->addressRenderer = $addressRenderer;
        $this->coreRegistry = $registry;
        parent::__construct($context, $registry, $adminHelper, $data);
    }

    /**
     * Get order address
     *
     * @return OrderAddress
     */
    public function getAddress()
    {
        $order = $this->getOrder();
        if ($this->getAddressType() == 'shipping') {
            $address = $order->getShippingAddress();
        } else {
            $address = $order->getBillingAddress();
        }

        return $address;
    }

    /**
     * Returns string with formatted address
     *
     * @param OrderAddress $address
     * @return null|string
     */
    public function getFormattedAddress(OrderAddress $address)
    {
        return $this->addressRenderer->format($address, 'html');
    }
}
