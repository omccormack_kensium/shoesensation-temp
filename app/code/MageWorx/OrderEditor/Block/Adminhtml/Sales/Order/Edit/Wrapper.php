<?php
/**
 * Copyright © 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\OrderEditor\Block\Adminhtml\Sales\Order\Edit;

use Magento\Framework\View\Element\Template;

class Wrapper extends Template
{
    /**
     * @var \MageWorx\OrderEditor\Helper\Data
     */
    protected $helperData;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var \Magento\Directory\Model\CurrencyFactory
     */
    protected $currencyFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \MageWorx\OrderEditor\Helper\Data $helperData
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Directory\Model\CurrencyFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \MageWorx\OrderEditor\Helper\Data $helperData,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        array $data = []
    ) {
        $this->helperData = $helperData;
        $this->objectManager = $objectManager;
        $this->currencyFactory = $currencyFactory;
        parent::__construct($context, $data);
    }

    /**
     * Get currency symbol for order
     *
     * @return string
     * @throws Exception
     */
    public function getCurrencySymbol()
    {
        $orderId = $this->getRequest()->getParam('order_id', false);
        $order = $this->objectManager->create('Magento\Sales\Model\Order')->load($orderId);

        if (!$order || !$order->getId()) {
            return '';
        }

        $currency = $this->currencyFactory->create()->load($order->getOrderCurrencyCode());
        $symbol = $currency->getCurrencySymbol();

        return $symbol;
    }

    /**
     * Get quote items ids as array for orderEditItems init. (JS)
     *
     * @return array
     */
    public function getQuoteItemIds()
    {
        $order = $this->getLayout()->getBlock('order_items')->getOrder();
        $itemsIds = [];

        if ($order) {
            $quote = $this->objectManager->get('MageWorx\OrderEditor\Model\Edit\Order')->getQuoteByOrder($order);
            $this->setQuote($quote);
            $items = $quote->getAllVisibleItems();

            /** @var Mage_Sales_Model_Quote_item $item */
            foreach ($items as $item) {
                $id = $item->getId();
                if ($id) {
                    $itemsIds[$id] = $item->getBuyRequest()->toArray();
                }
            }
        }

        return $itemsIds;
    }

    /**
     * Get blocks of order which can be edited (JSON)
     *
     * @return string
     */
    public function getBlocksJson()
    {
        $blocks = $this->helperData->getAvailableBlocks();
        return json_encode($blocks);
    }

    public function getEditUrlTemplate()
    {
        return $this->getUrl('ordereditor/form/load', [
            'block_id' => '%block_id%',
            'order_id' => $this->helperData->getOrderId()
        ]);
    }

    public function getApplyChangesUrl()
    {
        return $this->getUrl('ordereditor/edit/applyChanges', [
            'order_id' => $this->helperData->getOrderId(),
            'edited_block' => '%edited_block%'
        ]);
    }

    public function getCancelChangesUrl()
    {
        return $this->getUrl('ordereditor/edit/cancelChanges', [
            'order_id' => $this->helperData->getOrderId()
        ]);
    }

    public function getResetBlockUrl()
    {
        return $this->getUrl('ordereditor/edit/resetBlock', [
            'order_id' => $this->helperData->getOrderId(),
            'block_id' => '%block_id%',
        ]);
    }

    public function getSaveOrderUrl()
    {
        return $this->getUrl('ordereditor/edit/saveOrder', [
            'order_id' => $this->helperData->getOrderId()
        ]);
    }
}
