<?php
/**
 * Copyright © 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\OrderEditor\Block\Adminhtml\Sales\Order\Changed;

class Items extends \Magento\Sales\Block\Adminhtml\Items\AbstractItems
{
    /**
     * Address form template
     *
     * @var string
     */
    protected $_template = 'changed/items.phtml';

    /**
     * Object manager
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        array $data = []
    ) {
        $this->objectManager = $objectManager;
        parent::__construct($context, $stockRegistry, $stockConfiguration, $registry, $data);
    }

    public function _beforeToHtml()
    {
        $this->setParentBlock(
            $this->getLayout()->createBlock(
                'Magento\Framework\View\Element\Template',
                '',
                ['order' => $this->getOrder()]
            )
        );
        parent::_beforeToHtml();
    }

    /**
     * Retrieve order items collection
     *
     * @return Collection
     */
    public function getItemsCollection()
    {
        $orderItemsCollection = $this->objectManager->get('MageWorx\OrderEditor\Model\Edit\Order\Items')
            ->getItemsCollection($this->getOrder(), $this->getQuote());
        return $orderItemsCollection;
    }
}
