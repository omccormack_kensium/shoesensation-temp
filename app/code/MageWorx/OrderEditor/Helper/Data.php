<?php
/**
 * Copyright © 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\OrderEditor\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    /**
     * XML config enable permanent order item removal
     */
    const XML_PATH_ENABLE_PERMANENT_ORDER_ITEM_REMOVAL = 'mageworx_order_management/order_editor/enable_permanent_order_item_removal';

    /**
     * Object manager
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    protected $availableBlocks;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Registry $registry
    ) {
        $this->objectManager = $objectManager;
        $this->coreRegistry = $registry;
        $this->availableBlocks = null;
        parent::__construct($context);
    }

    /**
     * Get enable permanent order item removal
     *
     * @return int
     */
    public function getEnablePermanentOrderItemRemoval()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_ENABLE_PERMANENT_ORDER_ITEM_REMOVAL);
    }

    /**
     * Get blocks of order that can be edited
     *
     * @return array|null
     */
    public function getAvailableBlocks()
    {
        if (is_null($this->availableBlocks)) {
            $this->availableBlocks = [
                [
                    'className' => 'order-billing-address',
                    'blockId' => 'billing_address',
                    'changedBlock' => 'MageWorx\OrderEditor\Block\Adminhtml\Sales\Order\Changed\Address'
                ],
                [
                    'className' => 'order-shipping-address',
                    'blockId' => 'shipping_address',
                    'changedBlock' => 'MageWorx\OrderEditor\Block\Adminhtml\Sales\Order\Changed\Address'
                ],
                [
                    'className' => 'admin__table-wrapper',
                    'blockId' => 'order_items',
                    'changedBlock' => 'MageWorx\OrderEditor\Block\Adminhtml\Sales\Order\Changed\Items'
                ],
            ];
        }

        return $this->availableBlocks;
    }

    /**
     * Get block to edit by its id
     *
     * @param $blockId
     * @return bool
     */
    public function getBlockById($blockId)
    {
        foreach ($this->getAvailableBlocks() as $block) {
            if ($block['blockId'] == $blockId) {
                return $block;
            }
        }

        return false;
    }

    /**
     * Get current order entity id
     *
     * @return mixed
     */
    public function getOrderId()
    {
        if ($this->coreRegistry->registry('current_order')) {
            $order = $this->coreRegistry->registry('current_order');
        }
        if ($this->coreRegistry->registry('order')) {
            $order = $this->coreRegistry->registry('order');
        }

        if (isset($order)) {
            $orderId = $order->getId();
        } else {
            $orderId = null;
        }
        return $orderId;
    }

    /**
     * @param $orderId
     * @param $newChanges
     * @return array
     */
    public function addPendingChanges($orderId, $newChanges)
    {
        $session = $this->objectManager->get('Magento\Customer\Model\Session');
        $sessionKey = $this->getPendingChangesKey($orderId);

        $oldChanges = $this->getPendingChanges($orderId);

        if (is_null($oldChanges)) {
            $oldChanges = [];
        }

        if (isset($newChanges['quote_items'])) {
            foreach ($newChanges['quote_items'] as $quoteId => $quoteItemData) {
                if (isset($quoteItemData['configured']) && $quoteItemData['configured']) {
                    $quoteItem = $this->objectManager->create('Magento\Quote\Model\Quote\Item')->load($quoteId);
                    $productId = $quoteItem->getProductId();
                    unset($quoteItemData['configured']);
                    if (isset($quoteItemData['action'])) {
                        unset($quoteItemData['action']);
                    }
                    if (isset($quoteItemData['custom_price'])) {
                        unset($quoteItemData['custom_price']);
                    }
                    $quoteItemData['product_id'] = $productId;
                    $newChanges['product_to_add'][$quoteItem->getItemId()] = $quoteItemData;
                }
            }
        }

        $changes = array_merge($oldChanges, $newChanges);

        $session->setData($sessionKey, $changes);

        return $changes;
    }

    /**
     * @param $orderId
     * @return string
     */
    public function getPendingChangesKey($orderId)
    {
        return 'ordereditor_edit_changes_' . $orderId;
    }

    /**
     * @param $orderId
     * @return array
     */
    public function getPendingChanges($orderId)
    {
        $session = $this->objectManager->get('Magento\Customer\Model\Session');
        $sessionKey = $this->getPendingChangesKey($orderId);

        $changes = $session->getData($sessionKey);

        return $changes;
    }

    /**
     * @param $orderId
     * @param $blockId
     * @return array
     */
    public function unsetPendingChangesBlock($orderId, $blockId)
    {
        $session = $this->objectManager->get('Magento\Customer\Model\Session');
        $sessionKey = $this->getPendingChangesKey($orderId);

        $changes = $session->getData($sessionKey);
        $session->unsetData($sessionKey);
        if ($blockId == 'billing_address' || $blockId == 'shipping_address') {
            unset($changes['billing_address']);
            unset($changes['shipping_address']);
        } elseif ($blockId == 'order_items') {
            unset($changes['quote_items']);
            unset($changes['product_to_add']);
        }

        $session->setData($sessionKey, $changes);

        return $changes;
    }

    /**
     * @param $orderId
     */
    public function resetPendingChanges($orderId)
    {
        $session = $this->objectManager->get('Magento\Customer\Model\Session');
        $sessionKey = $this->getPendingChangesKey($orderId);

        $session->unsetData($sessionKey);
    }

    /**
     * Retrieve quote model object
     *
     * @return \Magento\Quote\Model\Quote
     */
    public function getQuote()
    {
        $order = $this->coreRegistry->registry('ordereditor_order');
        $quote = $this->objectManager->create('Magento\Quote\Model\Quote')
            ->setStoreId($order->getStoreId())
            ->load($order->getQuoteId());
        return $quote;
    }

    /**
     * Retrieve customer identifier
     *
     * @return int
     */
    public function getCustomerId()
    {
        return $this->coreRegistry->registry('ordereditor_order')->getCustomerId();
    }

    /**
     * Retrieve store model object
     *
     * @return \Magento\Store\Model\Store
     */
    public function getStore()
    {
        return $this->objectManager->create('Magento\Store\Model\Store')->load($this->getStoreId());
    }

    /**
     * Retrieve store identifier
     *
     * @return int
     */
    public function getStoreId()
    {
        return $this->coreRegistry->registry('ordereditor_order')->getStoreId();
    }
}
