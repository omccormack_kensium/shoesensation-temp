var config = {
    map: {
        '*': {
            mass_stock_editor:  'BoostMyShop_AdvancedStock/js/massStockEditor',
            low_stock:  'BoostMyShop_AdvancedStock/js/lowStock'
        }
    }
};