<?php

namespace BoostMyShop\AdvancedStock\Plugin\Supplier\Model;

//Rewrite the way supplier module put product in stock for receptions
class StockUpdater
{
    protected $_stockMovement;
    protected $_backendAuthSession;

    public function __construct(
        \BoostMyShop\AdvancedStock\Model\StockMovement $stockMovement,
        \Magento\Backend\Model\Auth\Session $backendAuthSession
    ){
        $this->_stockMovement = $stockMovement;
        $this->_backendAuthSession = $backendAuthSession;
    }

    public function aroundIncrementStock(\BoostMyShop\Supplier\Model\StockUpdater $subject, $proceed, $productId, $qty, $reason, $po)
    {
        $userId = '?';
        if ($this->_backendAuthSession->isLoggedIn())
            $userId =  $this->_backendAuthSession->getUser()->getId();

        $this->_stockMovement->create(  $productId,
                                        0,
                                        $po->getpo_warehouse_id(),
                                        $qty,
                                        \BoostMyShop\AdvancedStock\Model\StockMovement\Category::purchaseOrder,
                                        $reason,
                                        $userId);
    }
}