<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Model;

class Fqlogin extends Sociallogin
{

    public function newFoursquare()
    {
        try {
            require_once $this->_getBaseDir() . 'lib/Foursquare/FoursquareAPI.class.php';

        } catch (\Exception $e) {

        }

        $foursquare = new \FoursquareApi(
            $this->_dataHelper->getFqAppkey(),
            $this->_dataHelper->getFqAppSecret(),
            urlencode($this->_dataHelper->getAuthUrlFq())
        );
        return $foursquare;
    }

    public function getFqLoginUrl()
    {
        $foursquare = $this->newFoursquare();
        $loginUrl = $foursquare->AuthenticationLink();
        return $loginUrl;
    }
}
