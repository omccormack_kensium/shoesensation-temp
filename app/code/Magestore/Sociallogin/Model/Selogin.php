<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Model;

class Selogin extends Sociallogin
{

    const XML_PATH_SELOGIN = 'sociallogin/sociallogin/selogin';

    public function newSe()
    {
        try {
            require_once $this->_getBaseDir() . 'lib/OpenId/openid.php';
        } catch (\Exception $e) {
        }

        $openid = new \LightOpenID($this->_storeManager->getStore()->getUrl());
        return $openid;
    }

    public function setSeIdlogin($openid)
    {
        $openid->identity = 'https://openid.stackexchange.com';
        $openid->required = [
            'namePerson/first',
            'namePerson/last',
            'namePerson/friendly',
            'contact/email',
        ];

        $openid->returnUrl = $this->_storeManager->getStore()->getUrl(self::XML_PATH_SELOGIN);
        return $openid;
    }

}
