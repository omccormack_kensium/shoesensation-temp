<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Model;

class Twlogin extends \Zend_Oauth_Consumer
{

    protected $_options = null;

    protected $_dataHelper;

    public function __construct(\Magestore\Sociallogin\Helper\Data $Datahelper)
    {
        $this->_dataHelper = $Datahelper;
        $this->_config = new \Zend_Oauth_Config;
        $this->_options = [
            'consumerKey' => $this->_dataHelper->getTwConsumerKey(),
            'consumerSecret' => $this->_dataHelper->getTwConsumerSecret(),
            'signatureMethod' => 'HMAC-SHA1',
            'version' => '1.0',
            'requestTokenUrl' => 'https://api.twitter.com/oauth/request_token',
            'accessTokenUrl' => 'https://api.twitter.com/oauth/access_token',
            'authorizeUrl' => 'https://api.twitter.com/oauth/authorize',
        ];

        $this->_config->setOptions($this->_options);
    }

    public function setCallbackUrl($url)
    {
        $this->_config->setCallbackUrl($url);
    }
}
