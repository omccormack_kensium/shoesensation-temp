<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Model;

class Instagramlogin extends Sociallogin
{

    const XML_PATH_INSTALOGIN = 'sociallogin/sociallogin/instagramlogin/';

    public function newInstagram()
    {
        try {
            require_once $this->_getBaseDir() . 'lib/instagram/instagram.class.php';

        } catch (\Exception $e) {
        }

        $instagram = new \Instagram([
            'apiKey' => trim($this->_dataHelper->getInstaCustomerKey()),
            'apiSecret' => trim($this->_dataHelper->getInstaCustomerSecret()),
            'apiCallback' => $this->_storeManager->getStore()->getUrl(self::XML_PATH_INSTALOGIN, ['_secure' => true]),
        ]);
        return $instagram;
    }

    public function getInstagramLoginUrl()
    {
        return $this->newInstagram()->getLoginUrl();
    }
}
