<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Model;

class Ljlogin extends Sociallogin
{

    const XML_PATH_LJLOGIN = 'sociallogin/sociallogin/ljlogin';

    public function newMy()
    {
        try {
            require_once $this->_getBaseDir() . 'lib/OpenId/openid.php';

        } catch (\Exception $e) {
        }
        $openid = new \LightOpenID($this->_storeManager->getStore()->getUrl());
        return $openid;
    }

    public function getLjLoginUrl($identity)
    {
        $my_id = $this->newMy();
        $my = $this->setLjIdlogin($my_id, $identity);
        $loginUrl = $my->authUrl();
        return $loginUrl;
    }

    public function setLjIdlogin($openid, $identity)
    {
        $openid->identity = "http://" . $identity . ".livejournal.com";
        $openid->required = array(
            'namePerson/first',
            'namePerson/last',
            'namePerson/friendly',
            'contact/email',
        );
        $openid->returnUrl = $this->_storeManager->getStore()->getUrl(self::XML_PATH_LJLOGIN);
        return $openid;
    }
}
