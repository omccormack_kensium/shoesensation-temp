<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Model;

class Yalogin extends Sociallogin
{

    public function hasSession()
    {
        try {

            require_once $this->_getBaseDir() . 'lib/Yahoo/Yahoo.inc';

            error_reporting(E_ALL | E_NOTICE);
            ini_set('display_errors', true);
            \YahooLogger::setDebug(true);
            \YahooLogger::setDebugDestination('LOG');

            session_save_path('/tmp/');

            if (array_key_exists("logout", $this->_request->getParams())) {
                \YahooSession::clearSession($sessionStore = NULL);
            }

        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }
        $consumerKey = $this->_dataHelper->getYaConsumerKey();
        $consumerSecret = $this->_dataHelper->getYaConsumerSecret();
        $appId = $this->getAppId();
        return \YahooSession::hasSession($consumerKey, $consumerSecret, $appId, $sessionStore = NULL, $verifier = NULL);
    }

    public function getAuthUrl()
    {

        $consumerKey = $this->_dataHelper->getYaConsumerKey();
        $consumerSecret = $this->_dataHelper->getYaConsumerSecret();
        $callback = \YahooUtil::current_url() . '?in_popup';
        return \YahooSession::createAuthorizationUrl($consumerKey, $consumerSecret, $callback, $sessionStore = NULL);
    }

    public function getSession()
    {

        $consumerKey = $this->_dataHelper->getYaConsumerKey();
        $consumerSecret = $this->_dataHelper->getYaConsumerSecret();
        $appId = $this->_dataHelper->getYaAppId();
        return \YahooSession::requireSession($consumerKey, $consumerSecret, $appId, $callback = NULL, $sessionStore = NULL, $verifier = NULL);
    }
}