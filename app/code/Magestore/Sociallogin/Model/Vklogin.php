<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Model;

class Vklogin extends Sociallogin
{

    public function getVk()
    {
        $appId = $this->_dataHelper->getVkAppId();
        $secretId = $this->_dataHelper->getVkSecureKey();
        try {
            require_once $this->_getBaseDir() . 'lib/Vk/VK.php';
            require_once $this->_getBaseDir() . 'lib/Vk/VKException.php';

        } catch (\Exception $e) {
        }
        $vk = new \VK($appId, $secretId);
        return $vk;
    }

    public function _construct()
    {

        $this->_init('Magestore\Sociallogin\Model\ResourceModel\Vklogin');
    }
}
