<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Model;

const XML_PATH_GOLOGIN = 'sociallogin/gologin/user';

class Gologin extends Sociallogin
{
    public function Gonew()
    {

        require_once $this->_getBaseDir() . 'lib/Oauth2/service/Google_ServiceResource.php';
        require_once $this->_getBaseDir() . 'lib/Oauth2/service/Google_Service.php';
        require_once $this->_getBaseDir() . 'lib/Oauth2/service/Google_Model.php';
        require_once $this->_getBaseDir() . 'lib/Oauth2/contrib/Google_Oauth2Service.php';
        require_once $this->_getBaseDir() . 'lib/Oauth2/Google_Client.php';

        $this->_config = new \Google_Client;
        $this->_config->setClientId($this->_dataHelper->getGoConsumerKey());
        $this->_config->setClientSecret($this->_dataHelper->getGoConsumerSecret());
        $this->_config->setRedirectUri($this->_storeManager->getStore()->getUrl('sociallogin/gologin/user', ['_secure' => true]));
        return $this->_config;
    }
}
