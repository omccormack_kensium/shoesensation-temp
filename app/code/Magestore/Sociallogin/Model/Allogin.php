<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Model;
class Allogin extends Sociallogin
{

    public function newAol()
    {
        try {
            require_once $this->_getBaseDir() . 'lib/OpenId/openid.php';
        } catch (\Exception $e) {
        }

        $openid = new \LightOpenID($this->_storeManager->getStore()->getUrl());
        return $openid;
    }

    public function getAlLoginUrl($name)
    {
        $aol_id = $this->newAol();
        $aol = $this->setAolIdlogin($aol_id, $name);

        try {
            $loginUrl = $aol->authUrl();

            return $loginUrl;
        } catch (\Exception $e) {
            return null;
        }

    }

    public function setAolIdlogin($openid, $name)
    {

        $openid->identity = 'https://openid.aol.com/' . $name;
        $openid->required = array(
            'namePerson/first',
            'namePerson/last',
            'namePerson/friendly',
            'contact/email',
        );

        $openid->returnUrl = $this->_storeManager->getStore()->getUrl('sociallogin/sociallogin/allogin');
        return $openid;
    }

}
