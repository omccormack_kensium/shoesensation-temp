<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Model\System\Config;

class RedirectPage implements \Magento\Framework\Option\ArrayInterface
{

    const ACCOUNT_PAGE = 0;
    const CART_PAGE = 1;
    const HOME_PAGE = 2;
    const CURRENT_PAGE = 3;
    const CUSTOM_PAGE = 4;

    /**
     * get redirect page value.
     *
     * @return []
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => self::ACCOUNT_PAGE,
                'label' => __('Account Page'),
            ],
            [
                'value' => self::CART_PAGE,
                'label' => __('Cart Page'),
            ],
            [
                'value' => self::HOME_PAGE,
                'label' => __('Home Page'),
            ],
            [
                'value' => self::CURRENT_PAGE,
                'label' => __('Current Page'),
            ],
            [
                'value' => self::CUSTOM_PAGE,
                'label' => __('Custom Page'),
            ],
        ];

    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [
            0 => __('No'),
            1 => __('Yes'),
        ];
    }
}