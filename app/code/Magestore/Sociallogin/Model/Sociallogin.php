<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Model;

use Magento\Framework\App\Filesystem\DirectoryList;

class Sociallogin extends \Magento\Framework\Model\AbstractModel
{

    /**
     * Resource collection
     *
     * @var \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    protected $_resourceCollection;

    /**
     * Name of the resource model
     *
     * @var string
     */
    protected $_resourceName;

    /**
     * \Magento\Store\Model\StoreManagerInterface
     *
     * @var string
     */
    protected $_storeManager;
    /**
     * \Magestore\Sociallogin\Helper\Data
     *
     * @var string
     */
    protected $_dataHelper;
    /**
     * \Magento\Customer\Model\CustomerFactory
     *
     * @var string
     */
    protected $_customerFactory;
    /**
     * \Magestore\Sociallogin\Model\AuthorloginFactory
     *
     * @var string
     */
    protected $_authorloginFactory;
    /**
     * \Magestore\Sociallogin\Model\Resource\Authorlogin\CollectionFactory
     *
     * @var string
     */
    protected $_authorCollectionFactory;

    protected $_directory;

    protected $_request;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param  \Magestore\Sociallogin\Helper\Data $Datahelper
     * @param  \Magento\Customer\Model\CustomerFactory $customerFactory
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magestore\Sociallogin\Helper\Data $Datahelper,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory,
        \Magestore\Sociallogin\Model\AuthorloginFactory $AuthorloginFactory,
        \Magestore\Sociallogin\Model\ResourceModel\Authorlogin\CollectionFactory $authorCollectionFactory,
		\Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,        
        array $data = []
    )
    {
        $this->_storeManager = $storeManager;
        $this->_directory = $filesystem->getDirectoryWrite(DirectoryList::ROOT);
        $this->_resource = $resource;
        $this->_resourceCollection = $resourceCollection;
        $this->_dataHelper = $Datahelper;
        $this->_customerFactory = $customerFactory;
        $this->_customerCollectionFactory = $customerCollectionFactory;
        $this->_authorloginFactory = $AuthorloginFactory;
        $this->_authorCollectionFactory = $authorCollectionFactory;
        $this->_request = $request;

        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    public function _getBaseDir()
    {
        return $this->_directory->getAbsolutePath();
    }



}

