<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Model;

class Linkedlogin extends \Zend_Oauth_Consumer
{
    protected $_options = null;

    protected $_objectManager;

    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager)
    {
        $this->_objectManager = $objectManager;
        $this->_config = new \Zend_Oauth_Config;
        $this->_options = [
            'consumerKey' => $this->_objectManager->create('Magestore\Sociallogin\Helper\Data')->getLinkedConsumerKey(),
            'consumerSecret' => $this->_objectManager->create('Magestore\Sociallogin\Helper\Data')
                ->getLinkedConsumerSecret(),
            'version' => '1.0',
            'requestTokenUrl' => 'https://api.linkedin.com/uas/oauth/requestToken?scope=r_emailaddress',
            'accessTokenUrl' => 'https://api.linkedin.com/uas/oauth/accessToken',
            'authorizeUrl' => 'https://www.linkedin.com/uas/oauth/authenticate',
        ];

        $this->_config->setOptions($this->_options);
    }

    public function setCallbackUrl($url)
    {
        $this->_config->setCallbackUrl($url);
    }

    public function getOptions()
    {
        return $this->_options;
    }
}