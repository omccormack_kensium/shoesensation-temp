<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Model;

class Fblogin extends Sociallogin
{

    public function newFacebook()
    {
        error_reporting(E_ALL ^ E_WARNING);
        error_reporting(E_ALL ^ E_NOTICE);
        try {
            require_once $this->_getBaseDir() . 'lib/Facebook/facebook.php';

        } catch (\Exception $e) {
        }

        $facebook = new \Facebook(array(
            'appId' => $this->_dataHelper->getFbAppId(),
            'secret' => $this->_dataHelper->getFbAppSecret(),
            'cookie' => true,
        ));

        return $facebook;
    }

    public function getFbUser()
    {
        $facebook = $this->newFacebook();
        $userId = $facebook->getUser();

        $fbme = NULL;

        if ($userId) {

            try {
                $fbme = $facebook->api('/me?fields=email,first_name,last_name');
            } catch (FacebookApiException $e) {
            }
        }

        return $fbme;
    }

    public function getFbLoginUrl()
    {
        $facebook = $this->newFacebook();
        $loginUrl = $facebook->getLoginUrl(
            [
                'display' => 'popup',
                'redirect_uri' => $this->_dataHelper->getAuthUrl(),
                'scope' => 'email',
            ]
        );

        return $loginUrl;
    }
}
