<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Model;

class Livelogin extends Sociallogin
{

    public function newLive()
    {

        try {
            require_once $this->_getBaseDir() . 'lib/Author/OAuth2Client.php';

        } catch (\Exception $e) {
        }
        try {
            $live = new \OAuth2Client(
                $this->_dataHelper->getLiveAppkey(),
                $this->_dataHelper->getLiveAppSecret(),
                $this->_dataHelper->getAuthUrlLive()
            );
            $live->api_base_url = "https://apis.live.net/v5.0/";
            $live->authorize_url = "https://login.live.com/oauth20_authorize.srf";
            $live->token_url = "https://login.live.com/oauth20_token.srf";
            $live->out = "https://login.live.com/oauth20_logout.srf";
            return $live;
        } catch (\Exception $e) {
        }
    }

    public function getUrlAuthorCode()
    {
        $live = $this->newLive();
        return $live->authorizeUrl();
    }
}
