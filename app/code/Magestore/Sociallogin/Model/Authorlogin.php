<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Model;

class Authorlogin extends Sociallogin
{

    public function _construct()
    {
        $this->_init('Magestore\Sociallogin\Model\ResourceModel\Authorlogin');
    }

    public function addCustomer($authorId = null)
    {
        $customer = $this->_customerCollectionFactory->create();
        $customer = $customer->getLastItem();
        $customer_id = $customer->getData('entity_id');

        $model = $this->_authorloginFactory->create();
        $model->setData('author_id', $authorId)
            ->setData('customer_id', $customer_id)
            ->save();

        return true;
    }

}