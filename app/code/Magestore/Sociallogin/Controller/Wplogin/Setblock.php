<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Controller\Wplogin;
class Setblock extends \Magestore\Sociallogin\Controller\Sociallogin
{

    public function execute()
    {

        try {
            $this->_view->loadLayout();
            $this->_view->renderLayout();

            $this->setBlogName();

        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }

    }

    public function setBlogName()
    {
        $data = $this->getRequest()->getPost();
        $name = $data['name'];
        if ($name) {
            $url = $this->_objectManager->create('Magestore\Sociallogin\Model\Wplogin')->getWpLoginUrl($name);
            $this->getResponse()->setRedirect($url);
        } else {
            $this->messageManager()->addError('Please enter Blog name!');
            die("<script type=\"text/javascript\">try{window.opener.location.reload(true);}catch(e){window.opener.location.href=\"" . $this->_storeManager->getStore()->getBaseUrl() . "\"} window.close();</script>");
        }
    }

}