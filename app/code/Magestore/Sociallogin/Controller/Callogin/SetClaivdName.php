<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Controller\Callogin;
class SetClaivdName extends \Magestore\Sociallogin\Controller\Sociallogin
{

    public function execute()
    {

        try {
            $this->_setClaivdName();

        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }

    public function _setClaivdName()
    {
        $data = $this->getRequest()->getPost();

        if ($data) {
            $name = $data['name'];
            $url = $this->getCalModel()->getCalLoginUrl($name);
            $this->getResponse()->setRedirect($url);
        } else {
            $this->_getSingtone()->addError('Please enter Blog name!');
            die("<script type=\"text/javascript\">try{window.opener.location.reload(true);}catch(e){window.opener.location.href=\"" . $this->_storeManager->getStore()->getBaseUrl() . "\"} window.close();</script>");
        }

    }

    public function getCalModel()
    {
        return $this->_objectManager->create('Magestore\Sociallogin\Model\Callogin');
    }

}