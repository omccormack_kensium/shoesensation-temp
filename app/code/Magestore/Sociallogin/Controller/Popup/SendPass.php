<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Controller\Popup;

use Magento\Customer\Model\AccountManagement;

class SendPass extends \Magestore\Sociallogin\Controller\Sociallogin
{

    public function execute()
    {

        try {

            $this->_sendPass();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }

    }

    public function _sendPass()
    {
        $email = $this->getRequest()->getPost('socialogin_email_forgot', false);
        $model = $this->_objectManager->create('Magento\Customer\Model\Customer');
        $customer = $model->setWebsiteId($this->_storeManager->getStore()->getWebsiteId())
            ->loadByEmail($email);

        if ($customer->getId()) {
            try {
                $newPass = $this->_objectManager->create('Magento\Customer\Api\AccountManagementInterface');
                $newPass->initiatePasswordReset(
                    $email,
                    AccountManagement::EMAIL_RESET
                );
                // $newPassword = $customer->generatePassword();
                // $customer->changePassword($newPassword, false);
                // $customer->sendPasswordReminderEmail();
                $result = ['success' => true];
            } catch (\Exception $e) {
                $result = ['success' => false, 'error' => $e->getMessage()];
            }
        } else {
            $result = ['success' => false, 'error' => 'Not found!'];
        }
        // $this->messageManager->addSuccess(__('We\'ll email you a link to reset your password.'));
        $jsonEncode = $this->_objectManager->create('Magento\Framework\Json\Helper\Data');
        $this->getResponse()->setBody($jsonEncode->jsonEncode($result));
    }

}