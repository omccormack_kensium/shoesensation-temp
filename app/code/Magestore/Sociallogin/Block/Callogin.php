<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Block;

class Callogin extends Sociallogin
{
    public function getLoginUrl()
    {
        return $this->getUrl('sociallogin/sociallogin/callogin');
    }

    public function getAlLoginUrl()
    {
        return $this->getUrl('sociallogin/callogin/setClaivdName');
    }

    public function getCheckName()
    {
        return $this->getUrl('sociallogin/callogin/setblock');
    }

    public function getName()
    {
        return 'Name';
    }

    public function getEnterName()
    {
        return 'ENTER YOUR CLAVID NAME';
    }

    protected function _beforeToHtml()
    {

        return parent::_beforeToHtml();
    }
}