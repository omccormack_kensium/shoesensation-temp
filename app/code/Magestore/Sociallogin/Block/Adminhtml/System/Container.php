<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Block\Adminhtml\System;

use Magento\Framework\App\Filesystem\DirectoryList;

class Container extends \Magento\Config\Block\System\Config\Form\Field
{

    /**
     * store manager.
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    protected $_url;
    /**
     * helper data.
     *
     * @var \Magestore\Sociallogin\Helper\Data
     */
    protected $_dataHelper;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface $storeManager
     * @var \Magento\Framework\UrlInterface $url
     * @var \Magestore\Sociallogin\Helper\Data $Datahelper
     * @var \Magento\Framework\Session\SessionManagerInterface $session
     * @var \Magento\Framework\Filesystem $filesystem
     */
    protected $_session;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magestore\Sociallogin\Helper\Data $Datahelper,
        \Magento\Framework\Session\SessionManagerInterface $session,
        array $data = []
    )
    {
        $this->_storeManager = $context->getStoreManager();
        $this->_url = $context->getUrlBuilder();
        $this->_directory = $context->getFilesystem()->getDirectoryWrite(DirectoryList::ROOT);
        $this->_dataHelper = $Datahelper;
        $this->_session = $session;
        parent::__construct($context, $data);

    }

    protected function _getBaseDir()
    {
        return $this->_directory->getAbsolutePath();
    }

    public function _getStore()
    {

        $storeId = (int)$this->getRequest()->getParam('store', 0);

        return $this->_storeManager->getStore($storeId);
    }

}
