<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Block\Adminhtml\System\Config;

class Liveredirecturl
    extends \Magestore\Sociallogin\Block\Adminhtml\System\Container
{
    const XML_PATH_SECURE_IN_FRONTEND = 'web/secure/use_in_frontend';
    const XML_PATH_LIVE_LOGIN = 'sociallogin/sociallogin/livelogin';

    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $storeId = (int)$this->getRequest()->getParam('store', 0);

        $isSecure = $this->_dataHelper->getConfig(self::XML_PATH_SECURE_IN_FRONTEND);
        $redirectUrl = $this->_storeManager->getStore($storeId)->getUrl(self::XML_PATH_LIVE_LOGIN, array('_secure' => $isSecure, 'auth' => 1));
        $array = parse_url($redirectUrl);
        if (isset($array['query']) && $array['query']) {
            $redirectUrl = str_replace('?' . $array['query'], '', $redirectUrl);
        }

        $html = "<input style='width: 100%;'  readonly id='sociallogin_livelogin_redirecturl' class='input-text' value='" . $redirectUrl . "'>";
        return $html;
    }

}