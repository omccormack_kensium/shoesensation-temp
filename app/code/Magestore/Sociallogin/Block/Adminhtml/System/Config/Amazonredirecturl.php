<?php

/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Block\Adminhtml\System\Config;

class Amazonredirecturl extends \Magestore\Sociallogin\Block\Adminhtml\System\Container
{
    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $storeId = (int)$this->getRequest()->getParam('store', 0);

        $redirectUrl = $this->_storeManager->getStore($storeId)->getUrl('', array('_secure' => true));
        $domain = parse_url($redirectUrl);
        $referer = isset($domain['host']) ? $domain['scheme'] . '://' . $domain['host'] : $redirectUrl;

        $html = "<input style='width: 100%;'  readonly id='sociallogin_amlogin_redirecturl' class='input-text' value='" . $referer . "'>";
        return $html;
    }

}