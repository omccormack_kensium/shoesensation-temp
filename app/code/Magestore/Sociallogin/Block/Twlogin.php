<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Block;

class Twlogin extends Sociallogin
{
    const XML_PATH_TWLOGIN = 'sociallogin/sociallogin/twlogin';

    public function getLoginUrl()
    {
        return $this->getUrl(self::XML_PATH_TWLOGIN);
    }

    public function setBackUrl()
    {
        $currentUrl = $this->getCurrentUrl();
        $this->_session->setBackUrl($currentUrl);
        return $currentUrl;
    }

    protected function _beforeToHtml()
    {

        return parent::_beforeToHtml();
    }
}