<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Block;
class Toplinks extends Sociallogin
{

    protected function _beforeToHtml()
    {
        if (!$this->getIsActive()) {
            $this->setTemplate(null);
        }

        if ($this->_getSession()->isLoggedIn()) {
            $this->setTemplate(null);
        }

        return parent::_beforeToHtml();
    }

    public function getShownPositions()
    {
        $shownpositions = $this->_dataHelper->getConfig(\Magestore\Sociallogin\Helper\Data::XML_PATH_POSITION, $this->_storeManager->getStore()->getId());
        $shownpositions = explode(',', $shownpositions);
        return $shownpositions;
    }

    public function getPopupLoginUrl()
    {

        return $this->_storeManager->getStore()->getUrl('sociallogin/popup/login', ['_secure' => true]);
    }

    public function getPopupSendPass()
    {

        return $this->_storeManager->getStore()->getUrl('sociallogin/popup/sendPass', ['_secure' => true]);
    }

    public function getPopupCreateAcc()
    {
        return $this->_storeManager->getStore()->getUrl('sociallogin/popup/createAcc', ['_secure' => true]);

    }

    public function getBaseUrl()
    {
        return $this->_storeManager->getStore()->getUrl('customer/account');
    }
}