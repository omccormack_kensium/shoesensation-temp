<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Block;

class Aollogin extends Sociallogin
{
    const XML_PATH_ALLOGIN = 'sociallogin/sociallogin/allogin';
    const XML_PATH_ALLOGIN_CREENNAME = 'sociallogin/allogin/setScreenName';
    const XML_PATH_ALLOGIN_SETBLOCK = 'sociallogin/allogin/setblock';

    public function getLoginUrl()
    {
        return $this->getUrl(self::XML_PATH_ALLOGIN);
    }

    public function getAlLoginUrl()
    {
        return $this->getUrl(self::XML_PATH_ALLOGIN_CREENNAME);
    }

    public function getEnterName()
    {
        return 'ENTER SCREEN NAME';
    }

    public function getName()
    {
        return 'Name';
    }

    public function getCheckName()
    {
        return $this->getUrl(self::XML_PATH_ALLOGIN_SETBLOCK);
    }

    protected function _beforeToHtml()
    {

        return parent::_beforeToHtml();
    }
}