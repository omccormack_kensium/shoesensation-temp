<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
namespace Magestore\Sociallogin\Block;

class Wplogin extends Sociallogin
{
    const XML_PATH_WPLOGIN_SETBLOCk = 'sociallogin/wplogin/setblock';
    const XML_PATH_WPLOGIN = 'sociallogin/sociallogin/wplogin';

    public function getLoginUrl()
    {
        return $this->getUrl(self::XML_PATH_WPLOGIN);
    }

    public function getAlLoginUrl()
    {
        return $this->getUrl(self::XML_PATH_WPLOGIN_SETBLOCk);
    }

    public function getCheckName()
    {
        return $this->getUrl(self::XML_PATH_WPLOGIN_SETBLOCk);
    }

    public function getEnterName()
    {
        return 'ENTER YOUR BLOG NAME';
    }

    public function getName()
    {
        return 'Name';
    }
}