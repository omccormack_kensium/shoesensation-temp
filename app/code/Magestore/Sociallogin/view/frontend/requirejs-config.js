/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Sociallogin
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

var config = {
    map: {
       '*': {
       	popupSocials :  'Magestore_Sociallogin/js/sociallogin',
       	LightboxSocial: 'Magestore_Sociallogin/js/lightbox',
       	PopupLogin:      'Magestore_Sociallogin/js/popuplogin'
       }
    },
    paths: {
    },
};