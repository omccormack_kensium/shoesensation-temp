<?php

/**
 * Magestore.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Storepickup
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

namespace Kensium\Storepickup\Controller\Index;


/**
 * @category Magestore
 * @package  Magestore_Storepickup
 * @module   Storepickup
 * @author   Magestore Developer
 */
class Loadstores extends \Kensium\Storepickup\Controller\Index\Loadstore
{
    public function execute()
    {
		$html = "";
		/** @var \Magestore\Storepickup\Model\ResourceModel\Store\Collection $collection */
        $collection = $this->_filterStoreCollection($this->_storeCollectionFactory->create());
        if(count($collection))
        {
			$count = 0;
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			$mediaUrl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
			foreach($collection as $store)
			{
				
				$html .= "<li>";
				$html .= "<input type='checkbox' data-id='".$store->getStorepickupId()."' name='selected-store' class='store-checkbox'/>";
				$html .= "<div class='store-detail'>";
				if($store->getStoreName())
				$html .= " <h3>".$store->getStoreName()."</h3>";
				if($store->getAddress())
				$html .= "<p>".$store->getAddress()."</p>";
				if($store->getPhone())
				$html .= "<p class='phone-blk'><span class='label'>Phone:</span><span class='value'>".$store->getPhone()."</span></p>";
				if(count($store->getDates()))
				{
					$html .= "<p><span class='label'>Store hours:</span></p>";
					foreach($store->getDates() as $date)
					{
						$html .= "<p><span class='value'>".$date['key']."(".$date['val'].")</span></p>";
					}
				}
				$html .= "<p><span class='pick-date'>Pick-up date:</span><span class='value'> 3 business days from purchase</span></p>";
				$html .= "</div><div class='store-thumb'>";
				if($store->getBaseimage())
				$html .= "<p><img src='".$mediaUrl.$store->getBaseimage()."'></p>";
				$html .= "</div>";
				
				$html .= "</li>";
				$count++;
				if($count == 3)
				break;
			}
		}
		if($html)
		echo  $html;
		else
		echo FALSE;
        exit;
    }
}
