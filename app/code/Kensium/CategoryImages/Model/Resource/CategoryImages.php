<?php
/**
 * Copyright © 2016 Kensium . All rights reserved.
*/
namespace Kensium\CategoryImages\Model\Resource;

class CategoryImages extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Model Initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('catalog_category_entity', 'entity_id');
    }
}
