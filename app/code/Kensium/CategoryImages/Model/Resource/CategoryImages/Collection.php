<?php
/**
 * Copyright © 2016 Kensium . All rights reserved.
*/
namespace Kensium\CategoryImages\Model\Resource\CategoryImages;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Kensium\CategoryImages\Model\CategoryImages', 'Kensium\CategoryImages\Model\Resource\CategoryImages');
    }
}
