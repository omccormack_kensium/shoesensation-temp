<?php
/**
 * Copyright © 2016 Kensium . All rights reserved.
*/
namespace Kensium\CategoryImages\Model\Resource;

class CategoryImageDetails extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Model Initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('category_images', 'category_entity_id');
    }
}
