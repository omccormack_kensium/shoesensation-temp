<?php
/**
 * Copyright © 2016 Kensium . All rights reserved.
*/
namespace Kensium\CategoryImages\Model\Resource;

use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\ObjectManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;


class Databaseaccess extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @var \Kensium\Synclog\Model\CronScheduleFactory
     */
    protected $cronScheduleFactory;

    /**
     * @var null
     */
    protected $cronSchedule = null;

    /**
     * @var \Kensium\Synclog\Model\CategoryFactory
     */
    protected $categoryLogFactory;

    /**
     * @var \Kensium\Synclog\Model\CategorysynclogdetailsFactory
     */
    protected $categorysynclogdetailsFactory;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfigInterface;
    /**
     * @var \Kensium\Synclog\Model\Category
     */
    protected $categoryLogModel;
    /**
     * @var \Kensium\Kems\Helper\Data
     */
    protected $helperData;

    protected $resource;
    /**
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param ObjectManagerInterface $objectManagerInterface
     * @param ScopeConfigInterface $scopeConfigInterface
     */
    public function __construct(\Magento\Framework\Model\ResourceModel\Db\Context $context,
                                ObjectManagerInterface $objectManagerInterface,
                                ScopeConfigInterface $scopeConfigInterface
    )
    {
       
        $this->scopeConfigInterface = $scopeConfigInterface;
        $this->_objectManager = $objectManagerInterface;
        $this->resource = $context->getResources();
        parent::__construct($context);
    }

    public function _construct()
    {

    }
    /**
     * @param array $categoryArray
     * @return string
     */
    
    public function addImage($data)
    {
            $this->resource->getConnection()->query("INSERT INTO category_images (sort_order,image,is_featured,category_entity_id,tag_line) VALUES ( " . $data['sort_order'] . ",'" . $data['image_name'] . "'," . $data['is_featured'] . "," . $data['entity_id'] . "," . $data['tag_line'] . " )");

    }
    
    public function getImageName($id)
    {
       $query =  $this->resource->getConnection()->query("SELECT image FROM category_images WHERE category_entity_id = $id");
       $result =$query->fetch();
       return $result['image'];
    }
    
    public function getImageName2($id)
    {
       $query  =  $this->resource->getConnection()->query("SELECT image FROM category_images WHERE category_entity_id = $id");
       $result = $query->fetch();
       return $result;
    }
    
    public function updateImage($data)
    {
        if (isset($data['image_name']))
            $query =  $this->resource->getConnection()->query("UPDATE category_images SET sort_order = ".$data['sort_order'].", is_featured = ".$data['is_featured'].", tag_line = ".$data['tag_line'].",  image = '".$data['image_name']."' WHERE category_entity_id = ".$data['entity_id']."");
        else
            $query =  $this->resource->getConnection()->query("UPDATE category_images SET sort_order = ".$data['sort_order'].",tag_line = ".$data['tag_line'].", is_featured = ".$data['is_featured']." WHERE category_entity_id = ".$data['entity_id']."");
    }
    
    public function  getFeaturedCount()
    {
        $query =  $this->resource->getConnection()->query("SELECT count(*) as count FROM category_images WHERE is_featured = 1 ");
        $result =$query->fetch();
        return $result['count'];
    }

    /**
     * delete category based on id in category image table
     */
    public function  DeleteCategoryById($categoryid)
    {
        if ($categoryid) {
            $this->resource->getConnection()->query("Delete FROM category_images WHERE category_entity_id = $categoryid ");
        }
    }

    public function getIsFeatured($id)
    {
        $query =  $this->resource->getConnection()->query("SELECT is_featured FROM category_images WHERE category_entity_id = $id");
        $result =$query->fetch();
        return $result['is_featured'];
    }

}
