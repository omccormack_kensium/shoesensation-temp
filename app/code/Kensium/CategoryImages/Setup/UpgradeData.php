<?php
/**
 * @category   Amconnector
 * @package    Kensium_Amconnector
 * @copyright  Copyright (c) 2016 Kensium Solution Pvt.Ltd. (http://www.kensiumsolutions.com/)
 */

namespace Kensium\CategoryImages\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;


/**
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var
     */
    public $eavSetup;

    /**
     * @param EavSetupFactory $eavSetupFactory
     * @param \Magento\Eav\Model\Entity\Attribute\SetFactory $attributeSetFactory
     * @param \Magento\Eav\Model\Config $eavConfig
     */
    public function __construct(EavSetupFactory $eavSetupFactory,
                                \Magento\Eav\Model\Entity\Attribute\SetFactory $attributeSetFactory,
                                \Magento\Eav\Model\Config $eavConfig
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->eavConfig = $eavConfig;
    }


    /**
     * {@inheritdoc}
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

        //$setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            'category_sort_order',
            ['type' => 'varchar',
                'label' => 'Category Sort Order',
                'input' => 'text',
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => '0'
            ]
        );
        $attributeId = $eavSetup->getAttributeId('catalog_category', 'category_sort_order');
        $attributeSetId = $eavSetup->getAttributeSetId('catalog_category', 'Default');
        $attributeGroupId = $eavSetup->getAttributeGroupId('catalog_category', $attributeSetId, 'Content');
        $eavSetup->addAttributeToSet('catalog_category', $attributeSetId, $attributeGroupId, $attributeId);
        //$setup->endSetup();
        }


        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Category::ENTITY,
                'category_featured_brands',
                ['type' => 'varchar',
                    'label' => 'Category Featured Brands',
                    'input' => 'text',
                    'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'default' => '0'
                ]
            );
            $attributeId = $eavSetup->getAttributeId('catalog_category', 'category_featured_brands');
            $attributeSetId = $eavSetup->getAttributeSetId('catalog_category', 'Default');
            $attributeGroupId = $eavSetup->getAttributeGroupId('catalog_category', $attributeSetId, 'Content');
            $eavSetup->addAttributeToSet('catalog_category', $attributeSetId, $attributeGroupId, $attributeId);
            //$setup->endSetup();
        }

    }
}
