<?php
/**
 * Copyright © 2016 Kensium . All rights reserved.
 */

namespace Kensium\CategoryImages\Block\Adminhtml\CategoryImages\Renderer;

use Magento\Framework\DataObject;

class ImageView extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $session;

    /**
     * @var \Magento\Backend\Helper\Data
     */
    protected $backendHelper;

    /**
     * @param \Magento\Backend\Model\Session $session
     * @param \Magento\Backend\Helper\Data $backendHelper
     */
    public function __construct(
        \Magento\Backend\Model\Session $session,
        \Magento\Backend\Helper\Data $backendHelper
    )
    {
        $this->session = $session;
        $this->backendHelper = $backendHelper;
    }


    /**
     * @param Varien_Object $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $rowData = $row->getData();
        echo "<img src='" . "https://www.google.co.in/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwjls-iSk73NAhUBp48KHWJxB-QQjRwIBw&url=http%3A%2F%2Fwww.newsread.in%2Fimages-4545.html&psig=AFQjCNGnbgncsWuCXqT50WgU3mbBM6LnfA&ust=1466736970422647" . "''/>" ;
    }
}
