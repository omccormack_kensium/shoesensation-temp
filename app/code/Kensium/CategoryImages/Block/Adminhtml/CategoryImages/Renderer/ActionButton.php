<?php
/**
 * Copyright © 2016 Kensium . All rights reserved.
*/
namespace Kensium\CategoryImages\Block\Adminhtml\CategoryImages\Renderer;

use Magento\Framework\DataObject;

class ActionButton extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $session;

    /**
     * @var \Magento\Backend\Helper\Data
     */
    protected $backendHelper;

    /**
     * @param \Magento\Backend\Model\Session $session
     * @param \Magento\Backend\Helper\Data $backendHelper
     */
    public function __construct(
        \Magento\Backend\Model\Session $session,
        \Magento\Backend\Helper\Data $backendHelper
    )
    {
        $this->session = $session;
        $this->backendHelper = $backendHelper;
    }

    /**
     * @param Varien_Object $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $rowData = $row->getData();
        $url = 'categoryimages/categoryimages/edit/entity_id/' . $rowData['entity_id'].'/category_name/'. $rowData['value'];
        $url = $this->backendHelper->getUrl($url); 
        $usermsg = "<span style='color:green;'>Edit</span>";
        echo "<a href='" . $url . "' style='text-decoration:none'>" . $usermsg . "</a>";
    }
}
