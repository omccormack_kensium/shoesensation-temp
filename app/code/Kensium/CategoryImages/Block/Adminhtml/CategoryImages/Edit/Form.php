<?php
/**
 * Copyright © 2016 Kensium . All rights reserved.
 */
namespace Kensium\CategoryImages\Block\Adminhtml\CategoryImages\Edit;

/**
 * Class \Magento\Testimonial\Block\Adminhtml\Testimonial\Edit\Form
 *
 * @SuppressWarnings(PHPMD.DepthOfInheritance)
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * Prepare form before rendering HTML
     *
     * @return \Magento\Backend\Block\Widget\Form
     */
    protected $categoryImageDetails;
    protected $databaseaccess;
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Kensium\CategoryImages\Model\Resource\Databaseaccess  $databaseaccess,
        \Kensium\CategoryImages\Model\CategoryImageDetailsFactory  $categoryImageDetailsFactory,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
        $this->categoryImageDetailsFactory = $categoryImageDetailsFactory;
        $this->databaseaccess = $databaseaccess;
    }

    protected function _prepareForm()
    {
        $model = $this->categoryImageDetailsFactory->create()->load($this->_request->getParam('entity_id'));
        $totalFeat = $this->databaseaccess->getFeaturedCount();
      
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'edit_form',
                    'action' => $this->getUrl('categoryimages/categoryimages/save'),
                    'method' => 'post',
                    'enctype' => 'multipart/form-data'
                ],
            ]
        );
         $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __($this->_request->getParam('category_name')), 'class' => 'fieldset-wide']
        );


        $fieldset->addField(
            'tag_line',
            'hidden',
            ['name' => 'tag_line', 'label' => __('Tag Line'), 'title' => __('Tag_Line'), 'required' => false,'width'=>'10px']
        );

        $fieldset->addField(
            'sort_order',
            'hidden',
            ['name' => 'sortorder','label' => __('Sort Order'), 'title' => __('Sort Order'), 'required' => false,'width'=>'10px']


        );

        if ($model->getCategoryEntityId()) // set the form values only if that record exists in the database table category_images
        $form->setValues($model->getData());
        
        $isFeatured = $model->getIsFeatured();
        if ($isFeatured == NULL)
        {
            $isFeatured = 0;
        }
        $fieldset->addField('is_featured', 'hidden', array(
          'label'     => 'Is Featured',
          'class'     => '',
          'required'  => false,
          'name'      => 'featured',
          'onclick' => "",
          'onchange' => "",
          'value' => $isFeatured,
          'values' => array('0'=>'No','1' => 'Yes'),
          'disabled' => false,
          'readonly' => false,
          'tabindex' => 0

        ));
       
        if ($model->getImage() != NULL)
        {
              $fieldset->addField(
            'image',
            'file',
            ['name' => 'cateimage', 'label' => __('Thumbnail Image'), 'title' => __('Thumbnail Image')]
        );
        }
        else
        {
              $fieldset->addField(
            'image',
            'file',
            ['name' => 'cateimage', 'label' => __('Thumbnail Image'), 'title' => __('Thumbnail Image'), 'required' => true]
        );
        }
        $fieldset->addField(
            'entity_id',
            'hidden',
            ['name' => 'entityid','value'=>$this->_request->getParam('entity_id')]
        );

        
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
