<?php
/**
 * Copyright © 2016 Kensium. All rights reserved.
 */
namespace Kensium\CategoryImages\Block\Adminhtml\CategoryImages\Edit\Tab;


use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class Main extends Generic implements TabInterface
{
    /**
     * Banner config
     *
     * @var \Magento\Testimonial\Model\Config
     */
    protected $_testimonialConfig;

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Banner\Model\Config $testimonialConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Banner\Model\Config $testimonialConfig,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_testimonialConfig = $testimonialConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('Category Image');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('Category Image');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    
}
