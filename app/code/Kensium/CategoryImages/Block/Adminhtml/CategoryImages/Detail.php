<?php
/**
 * Copyright © 2016 Kensium . All rights reserved.
*/
namespace Kensium\CategoryImages\Block\Adminhtml\CategoryImages;
class Detail extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection
     */
    protected $_collection;

    /**
     * @var \Magento\Backend\Helper\Data
     */
    protected $backendHelper;

    /**
     * @var \Kensium\Kems\Model\ResourceModel\Customer
     */
    protected $resourceModelCustomer;

    /**
     * @var \Magento\Framework\Data\Collection
     */
    protected $dataCollection;

    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    protected $dataObjectFactory;

    /**
     * @var \Magento\Cron\Model\ConfigInterface
     */
    protected $cronConfig;

    /**
     * @var \Magento\Framework\Dataobject
     */
    protected $dataObject;

    /**
     * @var \Kensium\Scheduler\Block\Adminhtml\CronData
     */
    protected $cronData;
    /**
     * @var \Kensium\Synclog\Model\CategoryFactory
     */
    protected $categoryFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Framework\Data\Collection $dataCollection
     * @param \Magento\Framework\DataObjectFactory $dataObjectFactory
     * @param \Kensium\CategoryImages\Model\CategoryImagesFactory $categoryFactory
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param \Magento\Cron\Model\ConfigInterface $cronConfig
     * @param \Magento\Framework\Dataobject $dataObject
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Data\Collection $dataCollection,
        \Magento\Framework\DataObjectFactory $dataObjectFactory,
        \Kensium\CategoryImages\Model\CategoryImagesFactory $categoryFactory,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Cron\Model\ConfigInterface $cronConfig,
        \Magento\Framework\Dataobject $dataObject,
        $data = array()
    )
    {
        $this->moduleManager = $moduleManager;
        $this->backendHelper = $backendHelper;
        $this->dataCollection = $dataCollection;
        $this->dataObjectFactory = $dataObjectFactory;
        $this->cronConfig = $cronConfig;
        $this->dataObject = $dataObject;
        $this->categoryFactory = $categoryFactory;
        parent::__construct($context, $backendHelper, $data = array());
    }

    /**
     * @return void
     */
    protected function _construct()
    {

        parent::_construct();
        $this->setId('categoryGrid');
        $this->setDefaultSort('frontend_label');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setVarNameFilter('frontend_label');
        $storeId = $this->getRequest()->getParam('store');
        if ($storeId == 0)
        {
            $storeId = 1;
        }
        $this->_session->setData('storeId', $storeId);
    }


    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $storeId = $this->_request->getParam('store');
        if ($storeId == 1)
            $storeId = 0;
        if ($storeId == NULL)
            $storeId = 0;
       $collection = $this->categoryFactory->create()->getCollection(); 
       $collection->getSelect()->join('catalog_category_entity_varchar','main_table.entity_id = catalog_category_entity_varchar.row_id AND catalog_category_entity_varchar.store_id = '.$storeId)->join('eav_attribute','catalog_category_entity_varchar.attribute_id = eav_attribute.attribute_id AND eav_attribute.attribute_code = "name" AND catalog_category_entity_varchar.value != "Root Catalog" AND catalog_category_entity_varchar.value != "Default Category" ')->joinLeft('category_images','main_table.entity_id = category_images.category_entity_id');
       $collection = $collection;
       $this->setCollection($collection);
       return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {


        $this->addColumn("entity_id", array(
            "header" => __("Entity Id"),
            "align" => "left",
            "index" => "entity_id",
            'width' => '10'
        ));

        $this->addColumn("value", array(
            "header" => __("Category Name"),
            "align" => "left",
            'width' => '40',
            "index" => "value",
            'type' => 'text'
        ));


        $this->addColumn("edit", array(
            "header" => __("Action"),
            "align" => "left",
            "index" => "edit",
            'width' => '10',
            'renderer' => 'Kensium\CategoryImages\Block\Adminhtml\CategoryImages\Renderer\ActionButton',
        ));
        return parent::_prepareColumns();
    }

    //the below method return an empty url as the method is empty here so the grid becomes unclickable here
    public function getRowUrl($item)
    {
        parent::getRowUrl($item);
    }
}
?>
