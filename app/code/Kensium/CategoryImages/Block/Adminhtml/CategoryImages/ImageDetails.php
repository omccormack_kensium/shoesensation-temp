<?php
/**
 * Copyright © 2016 Kensium . All rights reserved.
*/
namespace Kensium\CategoryImages\Block\Adminhtml\CategoryImages;
class ImageDetails extends \Magento\Framework\View\Element\Template
{
    /**
     * @param Template\Context $context
     * @param array $data
     */
    protected $databaseaccess;
    public function __construct(\Kensium\CategoryImages\Model\Resource\Databaseaccess $databaseaccess , \Magento\Framework\View\Element\Template\Context $context,\Magento\Cron\Model\ConfigInterface $config,array $data = [])
    {
        parent::__construct($context, $data);
        $this->databaseaccess = $databaseaccess;
    }
    
    protected function _prepareLayout() {
        parent::_prepareLayout();
    }

    public function getImageName()
    {
       $name = $this->databaseaccess->getImageName($data = $this->_request->getParam('entity_id'));
       
       return $name;
       
    }
    
    function getMediaBaseUrl() {
	/** @var \Magento\Framework\ObjectManagerInterface $om */
	$om = \Magento\Framework\App\ObjectManager::getInstance();
	/** @var \Magento\Store\Model\StoreManagerInterface $storeManager */
	$storeManager = $om->get('Magento\Store\Model\StoreManagerInterface');
	/** @var \Magento\Store\Api\Data\StoreInterface|\Magento\Store\Model\Store $currentStore */
	$currentStore = $storeManager->getStore();
	return $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
}
}