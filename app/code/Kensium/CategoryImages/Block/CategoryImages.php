<?php
/**
 * Kensium
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Kensium.com license that is
 * available through the world-wide-web at this URL:
 * http://www.kensium.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Kensium
 * @package     Kensium_CategoryImages
 * @copyright   Copyright (c) 2012 Kensium (http://www.kensium.com/)
 * @license     http://www.kensium.com/license-agreement.html
 */

namespace Kensium\CategoryImages\Block;

class CategoryImages extends \Magento\Framework\View\Element\Template
{

    protected $_CategoryImagecollection;
    protected $categoryCollection;

    /**
     * @var \Magento\Framework\Url\Helper\Data
     */
    protected $urlHelper;
    protected $_registry;
    /**
     * Catalog product visibility
     *
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $_catalogProductVisibility;

    protected $__scopeConfig;
    protected $catCollection;
    protected $_resource;
    protected $_storeManager;    

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $_registry,
        \Kensium\CategoryImages\Model\CategoryImageDetailsFactory $categoryImageFactory,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollection,
        \Kensium\CategoryImages\Model\Resource\CategoryImages\CollectionFactory $catCollection,
        \Magento\Framework\App\ResourceConnection $resource,
        array $data = []
    )
    {
        $this->categoryImageFactory = $categoryImageFactory;
        $this->_resource = $resource;
        $this->_storeManager = $context->getStoreManager();
        $this->_registry = $_registry;
        $this->categoryCollection = $categoryCollection;
        $this->catCollection = $catCollection;
        parent::__construct($context, $data);
    }

    public function getCategoryImageCollection()
    {
        $limit = 3;
        $collection = $this->categoryImageFactory->create()->getCollection()->setOrder('sort_order', 'ASC');
        $collection->getSelect()->join('catalog_category_entity_varchar', 'main_table.category_entity_id = catalog_category_entity_varchar.row_id and main_table.is_featured="1"')->join('eav_attribute', 'catalog_category_entity_varchar.attribute_id = eav_attribute.attribute_id AND eav_attribute.attribute_code = "name" AND catalog_category_entity_varchar.value != "Root Catalog" AND catalog_category_entity_varchar.value != "Default Category" ');
        return $collection;
    }

    public function getCurrentCategoryChildrenImages()
    {
        $collection = $this->_registry->registry('current_category')->getChildrenCategories()->setOrder('category_sort_order', 'DESC');

        //    ->setOrder('category_sort_order', 'ASC');
        $collection->getSelect()->joinLeft(
            ['img'=>'category_images'],
            'e.entity_id = img.category_entity_id');
        return $collection;
    }
    
    public function getCurrentCategoryBanner()
    {
		$id = $this->_registry->registry('current_category')->getId();
		$category_image_path = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'categoryimages/';
		$category_image_path = str_replace("pub/","",$category_image_path);
		$connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
		$category_image = $connection->getTableName('category_images');
		$result1 = $connection->fetchone('SELECT image FROM `'.$category_image.'` WHERE category_entity_id='.$id);
		if($result1)
		return  $category_image_path.$result1;
		else
		return false;
        
    }




}

?>
