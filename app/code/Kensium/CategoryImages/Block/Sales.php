<?php
/** * @category  Sales
* @package    Kensium_CategoryImages
* @copyright  Copyright (c) 2016 Kensium Solution Pvt.Ltd. (http://www.kensiumsolutions.com/)
*/

namespace Kensium\CategoryImages\Block;

use Magento\Catalog\Model\Category as ModelCategory;

class Sales extends \Magento\Framework\View\Element\Template
{

    protected $_categoryHelper;
    protected $categoryFlatConfig;
    protected $topMenu;

    /**
     * Category factory
     *
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Catalog\Helper\Category $categoryHelper
     * @param \Magento\Catalog\Model\Indexer\Category\Flat\State $categoryFlatState
     * @param \Magento\Theme\Block\Html\Topmenu $topMenu
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Helper\Category $categoryHelper,
        \Magento\Catalog\Model\Indexer\Category\Flat\State $categoryFlatState,
        \Magento\Theme\Block\Html\Topmenu $topMenu,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory
    ) {

        $this->_categoryHelper = $categoryHelper;
        $this->_categoryFactory = $categoryFactory;
        $this->categoryFlatConfig = $categoryFlatState;
        $this->topMenu = $topMenu;
        parent::__construct($context);
    }
    /**
     * Return categories helper
     */
    public function getCategoryHelper()
    {
        return $this->_categoryHelper;
    }

    /**
     * Return categories helper
     * getHtml($outermostClass = '', $childrenWrapClass = '', $limit = 0)
     * example getHtml('level-top', 'submenu', 0)
     */
    public function getHtml()
    {
        return $this->topMenu->getHtml();
    }
    /**
     * Retrieve current store categories
     *
     * @param bool|string $sorted
     * @param bool $asCollection
     * @param bool $toLoad
     * @return \Magento\Framework\Data\Tree\Node\Collection|\Magento\Catalog\Model\Resource\Category\Collection|array
     */
    public function getStoreCategories($sorted = false, $asCollection = false, $toLoad = true)
    {
        $category = $this->_categoryHelper->getStoreCategories($sorted , $asCollection, $toLoad);

        return $this->_categoryHelper->getStoreCategories($sorted , $asCollection, $toLoad);
    }
    /**
     * Retrieve child store categories
     *
     */
    public function getChildCategories($category)
    {
        if ($this->categoryFlatConfig->isFlatEnabled() && $category->getUseFlatResource()) {
            $subcategories = (array)$category->getChildrenNodes();
        } else {
            $subcategories = $category->getChildren();
        }
        return $subcategories;
    }

    /**
     * category image url
     */
    public function getImageUrl($category){
        $category = $this->_categoryFactory->create()->load($category->getId());
        return $category->getImageUrl();
    }
    public function isOnHomePage($categoryId)
    {
        $category = $this->_categoryFactory->create()->load($categoryId);
        if($category->getCategoryOnHomepage()==1){
            return $category->getCategoryOnHomepage();
        }
    }

    public function getCategoryList()
    {
        $catData = $this->_categoryFactory->create()->getCollection()->addFieldToFilter('name','Sale')->getData();
        $categoryId =0;
        if(isset($catData[0]['entity_id'])) {
            $categoryId = $catData[0]['entity_id'];
        }
        $_category  = $this->_categoryFactory->create()->load($categoryId);
        $collection = $this->_categoryFactory->create()->getCollection()->addAttributeToSelect('*')
          ->addAttributeToFilter('is_active', 1)
          ->addAttributeToFilter('category_on_homepage', 1)
          ->setOrder('position', 'ASC')
          ->addIdFilter($_category->getChildren());
        return $collection;

    }

}
