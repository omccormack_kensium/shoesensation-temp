<?php
/**
 * @category   Kensium
 * @package    Kensium_CategoryImages
 * @copyright  Copyright (c) 2016 Kensium Solution Pvt.Ltd. (http://www.kensiumsolutions.com/)
 */
namespace Kensium\CategoryImages\Observer;

use Magento\Framework\Event\ObserverInterface;
use Kensium\CategoryImages\Model\Resource\Databaseaccess;

class DeleteCategory implements ObserverInterface
{

    protected $Databaseaccess;

    public function __construct(
        Databaseaccess $Databaseaccess
    ) {
        $this->Databaseaccess = $Databaseaccess;
    }

    /**
     * Display a custom message when customer log in
     *
     * @param  \Magento\Framework\Event\Observer $observer Observer
     *
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $childCategoryids = array();
        $event = $observer->getEvent();
        $currentCategory = $event->getCategory();
        $childCategories = $currentCategory->getChildren();
        $currentCategoryId = $currentCategory->getEntityId();
        if($childCategories){
            $this->Databaseaccess->DeleteCategoryById($currentCategoryId);
            $childCategoryids = explode(",",$childCategories);
            foreach($childCategoryids as $childCategoryids){
                $this->Databaseaccess->DeleteCategoryById($childCategoryids);
            }
        }else{
            $currentCategoryId = $currentCategory->getEntityId();
            $this->Databaseaccess->DeleteCategoryById($currentCategoryId);
        }
    }

}