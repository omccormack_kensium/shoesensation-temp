<?php
/**
 * Copyright © 2016 Kensium . All rights reserved.
*/
namespace Kensium\CategoryImages\Controller\Adminhtml\CategoryImages;

class Save extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    
    protected $file;
    
    protected $uploader;
    protected $adapterFactory;
    protected $fileSystem;
    public function __construct(
        
        \Magento\MediaStorage\Model\File\UploaderFactory $uploader,
        \Magento\Framework\Filesystem $fileSystem,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Filesystem\Io\File $file,
        \Kensium\CategoryImages\Model\Resource\Databaseaccess $databaseAccess,
        \Magento\Backend\App\Action\Context $context
        
    ) {
         parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->file = $file;
        $this->uploader = $uploader;
        $this->fileSystem = $fileSystem;
        $this->databaseAccess = $databaseAccess;
        
    }
    public function execute()
    {
       $categoryImages = $this->fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath('categoryimages/');
       $this->file->checkAndCreateFolder($categoryImages);
       $featuredCount = $this->databaseAccess->getFeaturedCount();
       $backurl = $this->_request->getParam('back');
       $imageName = $this->databaseAccess->getImageName($this->_request->getParam('entityid'));
       $ext = pathinfo($_FILES['cateimage']['name'], PATHINFO_EXTENSION);
       $imageName   = $this->_request->getParam('entityid').".".$ext;
       $_FILES['cateimage']['name'] = $imageName;
        if ($this->_request->getParam('entityid') == NULL)
        {
            $entityId = 0;
        }
        else
        {
            $entityId = $this->_request->getParam('entityid');
        }

       if ($this->_request->getParam('featured') == NULL)
       {
           $featuredValue = 0;
       }
       else
       {
           $featuredValue = $this->_request->getParam('featured');
       }
        try{ 
            
        $imageName = $this->databaseAccess->getImageName2($this->_request->getParam('entityid'));
         
        if ($imageName == false)
        {           
       $uploader = $this->uploader->create(['fileId' => 'cateimage']);
       $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
       $uploader->setAllowRenameFiles(false);
       $uploader->setFilesDispersion(false);
       $mediaDirectory = $categoryImages;
       $uploader->save($mediaDirectory);
       $imageData['tag_line']= "'test'" ;
       $imageData['image_name'] = $_FILES['cateimage']['name'];
       $imageData['sort_order'] = '0';
       $imageData['entity_id'] = $this->_request->getParam('entityid');
       $imageData['is_featured'] = $featuredValue;
        try{
            $this->databaseAccess->addImage($imageData);
        }catch (Exception $e){
            echo $e->getMessage();
        }


       }
       else
       {
        if ($_FILES['cateimage']['error'] <= 0)
        {

            $uploader = $this->uploader->create(['fileId' => 'cateimage']);
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
            $uploader->setAllowRenameFiles(false);
            $uploader->setFilesDispersion(false);
            $mediaDirectory = $categoryImages;
            $uploader->save($mediaDirectory);
            $imageData['image_name'] = $_FILES['cateimage']['name'];     
        }
       
       $imageData['sort_order'] = $this->_request->getParam('sortorder');
       $imageData['entity_id'] = $this->_request->getParam('entityid');
       $imageData['tag_line']= "'".$this->_request->getParam('tag_line')."'";
       $imageData['is_featured'] = $featuredValue;
       $this->databaseAccess->updateImage($imageData);

        }
       } catch (\Exception $e) {
       if ($e->getCode() == 0) {
       $this->messageManager->addError($e->getMessage());
       }
       }
       
    
    if ($backurl == NULL)
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $this->messageManager->addSuccess( __('Successfully Saved.'));
        $resultRedirect->setPath('categoryimages/categoryimages/index');
    }
    else
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $this->messageManager->addSuccess( __('Successfully Saved.'));
        $resultRedirect->setPath('categoryimages/categoryimages/edit/entity_id/'.$this->_request->getParam('entityid'));
    }
    return $resultRedirect;

    }
}