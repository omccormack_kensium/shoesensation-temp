<?php
/**
 * Copyright © 2015 Kensium . All rights reserved.
 */
namespace Kensium\PrintingCarts\Helper;
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_postDataHelper;
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper
    ) {
        $this->_postDataHelper = $postDataHelper;
        parent::__construct($context);
    }

    public function getMoveFromCartParams($itemIds)
    {
        $url = $this->_getUrl('wishlist/index/fromcart');
        $params = ['item' => $itemIds];
        return $this->_postDataHelper->getPostData($url, $params);
    }
}
