<?php
/**
 *
 * Copyright © 2015 Kensiumcommerce. All rights reserved.
 */
namespace Kensium\PrintingCarts\Controller\Printing;

class Carts extends \Magento\Framework\App\Action\Action
{

	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    protected $storeManager;
   
    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    protected $checkSession;
    protected $scopeConfig;
    protected $currency;
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
       \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Checkout\Model\Session $checkSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
            \Magento\Directory\Model\Currency $currency
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->cart = $cart;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
        $this->checkSession = $checkSession;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->currency = $currency;
        
    }
	
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {

        $currencyCode = $this->storeManager->getStore()->getCurrentCurrencyCode();
        $currencySymbol = $this->currency->getCurrencySymbol();
	$quote = $this->checkSession->getQuote();
	$quote->collectTotals();

        $pdf = new \Zend_Pdf();
        $page = $pdf->newPage('a4');
        $pdf->pages[] = $page;
        $zendResourceFont = new \Zend_Pdf_Resource_Font_Simple_Standard_TimesRoman();
        $page->setFont($zendResourceFont,12);
        $page->setFillColor(new \Zend_Pdf_Color_Rgb(0.3, 0.3, 0.3));
        $page->drawText("Shopping Cart",45,$page->getHeight()-20);
        $page->setFillColor(new \Zend_Pdf_Color_Rgb(0.6, 0.6, 0.6));
        $page->drawRectangle(45, $page->getHeight()-50, $page->getWidth()-45, $page->getHeight()-75);
        $page->setFillColor(new \Zend_Pdf_Color_Rgb(0, 0, 0));
        $page->setFont($zendResourceFont,9);
        $page->drawText("Product Name",50,$page->getHeight()-65)->setFont($zendResourceFont, 9);
        $page->drawText("Unit Price",350,$page->getHeight()-65)->setFont($zendResourceFont, 9);
        $page->drawText("QTY",450,$page->getHeight()-65)->setFont($zendResourceFont, 9);
        $page->drawText("Sub Total",$page->getWidth()-110,$page->getHeight()-65)->setFont($zendResourceFont, 9);

          $items = $this->cart->getItems();
          $gap = 105;
          $realTotalPrice = 0;
          $priceAfterDiscount = 0;
          $totalDiscountAmount = 0;
          $totalSpecialPrice = 0;
          $totalRegularPrice = 0;

            foreach($items as $item)
            {
                
                if(!$item->getParentItemId()) {
                //echo"<pre>";print_r($item->getProduct()->getData());
                $totalSpecialPrice = $totalSpecialPrice + $item->getProduct()->getFinalPrice() * $item->getQty();
                $totalRegularPrice = $totalRegularPrice + $item->getProduct()->getPrice() * $item->getQty();
                $realTotalPrice = $realTotalPrice + $item->getProduct()->getPrice() * $item->getQty(); // this part of the code gets the real price of the product without the discount being applied to the product
                $priceAfterDiscount = $priceAfterDiscount + $item->getPrice() * $item->getQty();
                $totalDiscountAmount = $totalDiscountAmount + ($realTotalPrice - $priceAfterDiscount);
                $page->drawText($item->getName(), 50, $page->getHeight() - $gap)->setFont($zendResourceFont, 9);
                $page->drawText($currencySymbol . $item->getProduct()->getFinalPrice(), 350, $page->getHeight() - $gap)->setFont($zendResourceFont, 9);
                $page->drawText($item->getQty(), 450, $page->getHeight() - $gap)->setFont($zendResourceFont, 9);
                $page->drawText($currencySymbol . ($item->getProduct()->getFinalPrice() * $item->getQty()), $page->getWidth() - 110, $page->getHeight() - $gap)->setFont($zendResourceFont, 9);
                $gap = $gap + 40;

            }
            }//exit;

            $totalDiscount = $totalRegularPrice - $totalSpecialPrice;
        //    $page->drawLine(50, $page->getHeight()-200, $page->getWidth()-45, $page->getHeight()-200);

              $gap = $gap+40;
             $page->drawText("Subtotal:".$currencySymbol."".$totalSpecialPrice,$page->getWidth()-150,$page->getHeight()-($gap))->setFont($zendResourceFont, 9);
             $gap = $gap+40;
             /* Discount calculation */
		$totals = $quote->getTotals(); 
		$totalDiscountAmount = $totals["subtotal"]->getValue() - $totals["grand_total"]->getValue(); 
             $page->drawText("Discount:".$currencySymbol."".$totalDiscountAmount,$page->getWidth()-150,$page->getHeight()-($gap))->setFont($zendResourceFont, 9);

             $gap = $gap+40;
             $page->drawText("Grand Total:".$currencySymbol."".$totals["grand_total"]->getValue(),$page->getWidth()-160,$page->getHeight()-($gap))->setFont($zendResourceFont, 9);

        $content = $pdf->render();
        $contentType = 'Content-type:application/pdf'; $contentLength = 'application/pdf';
         $fileName = "yourcart.pdf";
         $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', $contentType, true)
            ->setHeader('Content-Length', is_null($contentLength) ? strlen($content) : $contentLength)
            ->setHeader('Content-Disposition', 'attachment; filename=' . $fileName)
            ->setHeader('Last-Modified', date('r'));
        $this->getResponse()->setBody($content);
        
    }
    
}
