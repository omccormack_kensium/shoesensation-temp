<?php
/**
 * Created by mahendraj@kensium.com
 * Date: 04/10/2016
 * Time: 7:00 PM
 */
namespace Kensium\PrintingCarts\Controller\Wishlist;
use Magento\Framework\Controller\ResultFactory;
class Extendfromcart extends \Magento\Wishlist\Controller\Index\Fromcart
{
    public function execute()
    {

        $items = array_filter(explode(',',$this->getRequest()->getParam('item')));
        //print_r($items);exit;
        if(count($items) <= 1)
        {
          return parent::execute();
        }
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        if (!$this->formKeyValidator->validate($this->getRequest())) {
            return $resultRedirect->setPath('*/*/');
        }

        $wishlist = $this->wishlistProvider->getWishlist();
        if (!$wishlist) {
            throw new NotFoundException(__('Page not found.'));
        }

        try {

            foreach($items as $itemId)
            {

            $item = $this->cart->getQuote()->getItemById($itemId);
            if (!$item) {
                throw new LocalizedException(
                    __('The requested cart item doesn\'t exist.')
                );
            }

            $productId = $item->getProductId();
            $buyRequest = $item->getBuyRequest();
            $parentId = $item->getParentItemId();
            //$prodType = $item->getProductType();
            if(!isset($parentId)) {
                $wishlist->addNewItem($productId, $buyRequest);

                $this->cart->getQuote()->removeItem($itemId)->delete();
                //$this->cart->save();

                $this->wishlistHelper->calculate();
                $wishlist->save();

                $this->messageManager->addSuccessMessage(__(
                    "%1 has been moved to your wish list.",
                    $this->escaper->escapeHtml($item->getProduct()->getName())
                ));
            }
            }
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('We can\'t move the item to the wish list.'));
        }
        return $resultRedirect->setUrl($this->cartHelper->getCartUrl());

    }
} 
