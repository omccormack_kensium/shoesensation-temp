/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global define*/
define(
    [
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/quote'
    ],
    function (Component, quote) {
        "use strict";
        return Component.extend({
            defaults: {
                template: 'Magento_SalesRule/summary/discount'
            },
            totals: quote.getTotals(),
            items : quote.getItems(),
            isDisplayed: function() {
                return this.isFullMode() && this.getPureValue() != 0;
            },
            getCouponCode: function() {
                if (!this.totals()) {
                    return null;
                }
                return this.totals()['coupon_code'];
            },
            getPureValue: function() {
                var savedPrice = 0;
                for(var i=0;i<this.items.length;i++){
                    console.log(this.items[i]);
                    var basePrice = this.items[i].base_price;
                    var bmsrp = this.items[i].product.msrp;
                    var splcprice = this.items[i].product.special_priwce;
                    if(bmsrp == 'undefined' || !bmsrp){
                        bmsrp = 0;
                    }
                    if(splcprice == 'undefined' || !splcprice){
                        splcprice = 0;
                    }
                    if(bmsrp > 0 && splcprice == 0) {
                        savedPrice += parseFloat(bmsrp)-parseFloat(basePrice);
                    }
                    if(splcprice > 0 && bmsrp == 0) {
                        savedPrice += parseFloat(basePrice)-parseFloat(splcprice);
                    }
                }
                /*var price = 0;
                 if (this.totals() && this.totals().discount_amount) {
                 price = parseFloat(this.totals().discount_amount);
                 }*/
                return savedPrice;
            },
            getValue: function() {
                return this.getFormattedPrice(this.getPureValue());
            }
        });
    }
);
