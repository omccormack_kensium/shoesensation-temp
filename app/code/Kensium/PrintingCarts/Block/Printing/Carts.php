<?php
/**
 * Copyright © 2015 Kensium . All rights reserved.
 */
namespace Kensium\PrintingCarts\Block\Printing;
use Kensium\PrintingCarts\Helper\Data as PrintingCartsHelper;
class Carts extends \Magento\Framework\View\Element\Template
{
    protected $modCart;
    protected $checkSession;
    protected $printCont;
    protected $printingCartsHelper;
    public function __construct(
     \Kensium\PrintingCarts\Block\Context $context,
     \Magento\Checkout\Model\Cart $modCart,
     \Magento\Checkout\Model\Session $checkSession,
     \Kensium\PrintingCarts\Controller\Printing\Carts $printCont
    ) 
    {
        
        $this->modCart = $modCart;
        $this->checkSession = $checkSession;
        $this->printCont = $printCont;
        $this->printingCartsHelper = $context->_devToolHelper;
        parent::__construct($context);
    }
        
    function testBlock()
    {
        $cartQuote = $this->modCart->getQuote();
        $items = $this->checkSession->getQuote()->getAllItems();
        
        foreach($items as $item)
        {
            echo "Name:".$item->getName()."<br>";
            echo "Quantity:".$item->getQty()."<br>";
            echo "Price:".$item->getPrice()."<br>";
        }

          $pdf = new \Zend_Pdf();
          $pdf->save('test.pdf');
          $page = new \Zend_Pdf_Page('a4');
          $zendResource = new \Zend_Pdf_Resource_Font_Simple_Standard_TimesBold();
          $page->setFont($zendResource, 16) ;
          $width = $page->getWidth();
          $i=0;
          $pdf->pages[] = $page;
          $pdf->render();
    }

    function createPDF()
    {
        ini_set('display_errors', 'on');
        $this->printCont->justTest();
    }
    /**
     * Get JSON POST params for moving from cart
     *
     * @return string
     */
    public function getMoveFromCartParams()
    {
        $cartQuote = $this->modCart->getQuote();
        $items = $this->checkSession->getQuote()->getAllItems();
        $itemIDs = array();
        foreach($items as $item)
        {
            $itemIDs[] = $item->getId();
           
        }
        if(count($itemIDs) > 0){
            return $this->printingCartsHelper->getMoveFromCartParams($itemIDs);
        }
        else
        return false;
    }
}
