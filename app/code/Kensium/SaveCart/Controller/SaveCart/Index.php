<?php

/** 
* @category  Cart
* @package    Kensium_SaveCart
* @copyright  Copyright (c) 2016 Kensium Solution Pvt.Ltd. (http://www.kensiumsolutions.com/)
*/

namespace Kensium\SaveCart\Controller\SaveCart;

class Index extends \Magento\Framework\App\Action\Action
{



    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $session
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->customeSession = $session;
    }

    public function execute()
    {

        if($this->customeSession->isLoggedIn()){
            $this->messageManager->addSuccess("Your cart is saved");
            $resultRedirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;
        }else{
            $this->messageManager->addSuccess("Your cart will be saved after successful login");
            return $this->resultRedirectFactory->create()->setPath('customer/account/login', ['_current' => true]);
        }
    }


}
