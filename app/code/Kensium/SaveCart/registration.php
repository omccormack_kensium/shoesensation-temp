<?php

/** 
* @category  Cart
* @package    Kensium_SaveCart
* @copyright  Copyright (c) 2016 Kensium Solution Pvt.Ltd. (http://www.kensiumsolutions.com/)
*/

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Kensium_SaveCart',
    __DIR__
);
