<?php

/**
 * @category   Kensium
 * @package    Kensium_Bestseller
 * @copyright  Copyright (c) 2016 Kensium Solution Pvt.Ltd. (http://www.kensiumsolutions.com/)
 */
namespace Kensium\Productcollection\Block;


class Bestseller extends \Magento\Catalog\Block\Product\AbstractProduct
{
    protected $_productcollection;

    /**
     * @var \Magento\Framework\Url\Helper\Data
     */
    protected $urlHelper;
    /**
     * Catalog product visibility
     *
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $_catalogProductVisibility;

    protected $__scopeConfig;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productcollection,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        array $data = []
    )
    {
        $this->_productcollection = $productcollection;
        $this->urlHelper = $urlHelper;
        $this->_catalogProductVisibility = $catalogProductVisibility;
        $this->_scopeConfig = $context->getScopeConfig();
        parent::__construct($context, $data);
    }

    public function getBestSeller()
    {
        $limit = $this->_scopeConfig->getValue('promotions/kensium_productcollection/bestseller_limit');
        $collection = $this->_productcollection->create()
            ->addAttributeToFilter('status', '1');
            
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $bests_seller_id = $this->getBestSellerId("Best Sellers");
            
        $collection->addCategoriesFilter(['eq' => $bests_seller_id]);
        
        
        $current_category_obj = $objectManager->get('\Magento\Framework\Registry')->registry('current_category');
        
        if($current_category_obj)
        {
			$collection->addCategoriesFilter(['eq' => $current_category_obj->getId()]);
		}
		        
        
        $collection->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());
        $collection = $this->_addProductAttributesAndPrices($collection)
            ->setPageSize($limit)
            ->setCurPage(1);

        return $collection;
    }
    
    public function getBestSellerByCategory($current_category)
    {
        $collection = $this->getBestSeller();
       
        $collection->addCategoriesFilter(['eq' => $current_category]);
         
        return $collection;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return array
     *
     */
    public function getAddToCartPostParams(\Magento\Catalog\Model\Product $product)
    {
        $url = $this->getAddToCartUrl($product);
        return [
            'action' => $url,
            'data' => [
                'product' => $product->getEntityId(),
                \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED =>
                    $this->urlHelper->getEncodedUrl($url),
            ]
        ];
    }
    
    public function getIsNew($product)
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
		$date = $objDate->gmtDate();
		if (($date > $product->getNewsFromDate()) && ($date < $product->getNewsToDate()))
		{
			return TRUE;
		}
		else
		{
			return False;
		}
	}
	
	public function getIsBestSeller($product)
    {
		$bests_seller_id = $this->getBestSellerId("Best Sellers");
		if (in_array($bests_seller_id, $product->getCategoryIds()))
		{
			return TRUE;
		}
		else
		{
			return False;
		}
	}
	
	public function getBestSellerId($title)
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cat_obj = $objectManager->create('Magento\Catalog\Model\CategoryFactory');
        $cat_collection = $cat_obj->create()->getCollection()->addFieldToFilter('name',$title);
        return $cat_collection->getFirstItem()->getId();
	}
	
	
	
	public function getSaleProducts()
    {
		$collection = $this->_productcollection->create()
						->addAttributeToSelect('*')
						->addAttributeToFilter('status', '1')
						->addAttributeToFilter('is_sale', '1');
		$collection->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());
		return $collection;
	}
}

?>
