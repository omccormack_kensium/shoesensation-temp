<?php
/**
 * @category   Kensium
 * @package    Kensium_Bestseller
 * @copyright  Copyright (c) 2016 Kensium Solution Pvt.Ltd. (http://www.kensiumsolutions.com/)
 */
namespace Kensium\BestSeller\Setup;
 
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
 
/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;
     /**
     * @var \Magento\Eav\Model\Entity\Attribute\SetFactory
     */

    protected $attributeSetFactory;

    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $eavConfig;

    /*
    * @var int
    */
    protected $entityTypeId;

    /**
     * @param EavSetupFactory $eavSetupFactory
     * @param \Magento\Eav\Model\Entity\Attribute\SetFactory $attributeSetFactory
     * @param \Magento\Eav\Model\Config $eavConfig
     */
 
    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory,
                                \Magento\Eav\Model\Entity\Attribute\SetFactory $attributeSetFactory,
                                \Magento\Eav\Model\Config $eavConfig
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->eavConfig = $eavConfig;
    }
 
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
 
        /**
         * Add attributes to the eav/attribute
         */
 
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'best_seller',
            [
                'type' => 'int',
                'backend_type' => 'int',
                'frontend' => '',
                'label' => 'Best Seller',
                'input' => 'boolean',
                'class' => '',
                'source' => '',
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => 0,
                'searchable' => false,
                'filterable' => true,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => '',
                'is_configurable'   => false                
            ]
        );
        $attributeId = $eavSetup->getAttributeId('catalog_product', 'best_seller');
        $attributeSetId = $eavSetup->getAttributeSetId('catalog_product', 'Default');
        $attributeGroupId = $eavSetup->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
        $eavSetup->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
    }
}
