<?php
/**
 * @category   Kensium
 * @package    Kensium_Bestseller
 * @copyright  Copyright (c) 2016 Kensium Solution Pvt.Ltd. (http://www.kensiumsolutions.com/)
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Kensium_BestSeller',
    __DIR__
);
