<?php

/**
 * @category   Kensium
 * @package    Kensium_Bestseller
 * @copyright  Copyright (c) 2016 Kensium Solution Pvt.Ltd. (http://www.kensiumsolutions.com/)
 */
namespace Kensium\BestSeller\Block;


class Bestseller extends \Magento\Catalog\Block\Product\AbstractProduct
{
    protected $_productcollection;

    /**
     * @var \Magento\Framework\Url\Helper\Data
     */
    protected $urlHelper;
    /**
     * Catalog product visibility
     *
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $_catalogProductVisibility;

    protected $__scopeConfig;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productcollection,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        array $data = []
    )
    {
        $this->_productcollection = $productcollection;
        $this->urlHelper = $urlHelper;
        $this->_catalogProductVisibility = $catalogProductVisibility;
        $this->_scopeConfig = $context->getScopeConfig();
        parent::__construct($context, $data);
    }

    public function getBestSeller()
    {
        $limit = $this->_scopeConfig->getValue('promotions/kensium_bestseller/bestseller_limit');
        $collection = $this->_productcollection->create()
            ->addAttributeToFilter('status', '1')
            ->addAttributeToFilter('best_seller', '1');
        $collection->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());
        $collection = $this->_addProductAttributesAndPrices($collection)
            ->setPageSize($limit)
            ->setCurPage(1);
        return $collection;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return array
     *
     */
    public function getAddToCartPostParams(\Magento\Catalog\Model\Product $product)
    {
        $url = $this->getAddToCartUrl($product);
        return [
            'action' => $url,
            'data' => [
                'product' => $product->getEntityId(),
                \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED =>
                    $this->urlHelper->getEncodedUrl($url),
            ]
        ];
    }


}

?>
