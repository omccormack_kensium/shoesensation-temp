<?php
/**
 * Copyright © 2015 Kensium . All rights reserved.
 */
namespace Kensium\ColorSwatches\Block\Style;
use Magento\Catalog\Block\Product\AbstractProduct;
use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Helper\Stock;
use Magento\Framework\Registry;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;
class ColorSwatches extends AbstractProduct
{

    protected  $_registry;

    protected $_productFactory;

    /**
     * @var \Magento\CatalogInventory\Helper\Stock
     */
    protected $_stockFilter;

    protected $_productStatus;

    protected $_productVisibility;

    public function __construct(
        Context $context,
        ProductFactory $productFactory,
        Stock $stockFilter,
        Status $productStatus,
        Visibility $productVisibility,
        array $data = []
    ) {
        $this->_registry = $context->getRegistry();
        $this->_productFactory   = $productFactory;
        $this->_stockFilter = $stockFilter;
        $this->_productStatus = $productStatus;
        $this->_productVisibility = $productVisibility;
        parent::__construct($context, $data);
    }


    /**
     * Fetch all the configurable products
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function buildColorSwatches()
    {
        $product =  $this->_registry->registry('current_product');
        $getStyleId =  $product->getAcuInStylegroup();
        if(!empty($getStyleId) && $getStyleId != NULL && $getStyleId !='NULL'){
            $productCollection = $this->_productFactory->create()->getCollection();
            $productCollection->addAttributeToSelect(array("name","image","url_key","sku","color"));
	        $productCollection->addFieldToFilter("type_id","configurable");
            $productCollection->addFieldToFilter(array(
                array(
                    'attribute' => 'acu_in_stylegroup',
                    'eq'        =>  $getStyleId
                )
                )
            );
            $productCollection->addAttributeToFilter("entity_id",array("neq"=>$product->getId()));
            $productCollection->addAttributeToFilter('status', ['in' => $this->_productStatus->getVisibleStatusIds()]);
            $productCollection->setVisibility($this->_productVisibility->getVisibleInSiteIds());
           // echo $productCollection->getSelect();exit;
            $this->_stockFilter->addInStockFilterToCollection($productCollection);
            return $productCollection;
        }else{
            return "";
        }
        
        
    }

    /**
     * Fetch all the configurable products
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function buildProductImages()
    {
        $product =  $this->_registry->registry('current_product');
        
        return $product;
    }
}
