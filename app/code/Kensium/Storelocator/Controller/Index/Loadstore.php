<?php

/**
 * Magestore.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Storelocator
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

namespace Kensium\Storelocator\Controller\Index;
use Magento\Framework\Controller\ResultFactory;
use Magestore\Storelocator\Model\Config\Source\OrderTypeStore;

/**
 * @category Magestore
 * @package  Magestore_Storelocator
 * @module   Storelocator
 * @author   Magestore Developer
 */
class Loadstore extends \Magestore\Storelocator\Controller\Index
{
    /**
     * Default current page.
     */
    const DEFAULT_CURRENT_PAGINATION = 1;

    /**
     * Default range pagination.
     */
    const DEFAULT_RANGE_PAGINATION = 5;

    /**
     * @return \Magento\Framework\Controller\Result\Raw
     */

    public function execute()
    {
        /** @var \Magestore\Storelocator\Model\ResourceModel\Store\Collection $collection */
        $collection = $this->_filterStoreCollection($this->_storeCollectionFactory->create());

        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        $pager = $resultPage->getLayout()->createBlock(
            'Magestore\Storelocator\Block\ListStore\Pagination',
            'storelocator.pager',
            [
                'collection' => $collection,
                'data' => ['range' => self::DEFAULT_RANGE_PAGINATION],
            ]
        );

        /** @var \Magento\Framework\Controller\Result\Raw $response */
        $response = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $response->setHeader('Content-type', 'text/plain');

        $response->setContents(
            $this->_jsonHelper->jsonEncode(
                [
                    'storesjson' => $collection->prepareJson(),
                    'pagination' => $pager->toHtml(),
                    'num_store' => $collection->getSize(),
                ]
            )
        );

        return $response;
    }

    /**
     * filter store.
     *
     * @param \Magestore\Storelocator\Model\ResourceModel\Store\Collection $collection
     *
     * @return \Magestore\Storelocator\Model\ResourceModel\Store\Collection
     */
    protected function _filterStoreCollection(
        \Magestore\Storelocator\Model\ResourceModel\Store\Collection $collection
    ) {
        //~ $collection->addFieldToSelect([
        //~ 'store_name',
        //~ 'phone',
        //~ 'address',
        //~ 'latitude',
        //~ 'longitude',
        //~ 'marker_icon',
        //~ 'zoom_level',
        //~ 'rewrite_request_path',
        //~ ]);

        $collection->addFieldToSelect('*');



        $curPage = $this->getRequest()->getParam('curPage', self::DEFAULT_CURRENT_PAGINATION);
        $collection->setPageSize($this->_systemConfig->getPainationSize())->setCurPage($curPage);

        /*
         * Filter store enabled
         */
        $collection->addFieldToFilter('status', \Magestore\Storelocator\Model\Status::STATUS_ENABLED);

        /*
         * filter by radius
         */
        if ($radius = $this->getRequest()->getParam('radius')) {
            $latitude = $this->getRequest()->getParam('latitude');
            $longitude = $this->getRequest()->getParam('longitude');
            $collection->addLatLngToFilterDistance($latitude, $longitude, $radius);
        }

        /*
         * filter by tags
         */
        $tagIds = $this->getRequest()->getParam('tagIds');
        if (!empty($tagIds)) {
            $collection->addTagsToFilter($tagIds);
        }

        /*
         * filter by store information
         */

        if ($countryId = $this->getRequest()->getParam('countryId')) {
            $collection->addFieldToFilter('country_id', $countryId);
        }

        if ($storeName = $this->getRequest()->getParam('storeName')) {
            $collection->addFieldToFilter('store_name', ['like' => "%$storeName%"]);
        }
        if ($state = $this->getRequest()->getParam('state')) {
            $collection->addFieldToFilter('state', ['like' => "%$state%"]);
        }
        if ($city = $this->getRequest()->getParam('city')) {
            $collection->addFieldToFilter('city', ['like' => "%$city%"]);
        }
        if ($zipcode = $this->getRequest()->getParam('zipcode')) {
            $collection->addFieldToFilter('zipcode', ['like' => "%$zipcode%"]);
        }
        // Set sort type for list store
        switch ($this->_systemConfig->getSortStoreType()) {
            case OrderTypeStore::SORT_BY_ALPHABETICAL:
                $collection->setOrder('store_name', \Magento\Framework\Data\Collection\AbstractDb::SORT_ORDER_ASC);
                break;

            case OrderTypeStore::SORT_BY_DISTANCE:
                if ($radius) {
                    $collection->setOrder('distance', \Magento\Framework\Data\Collection\AbstractDb::SORT_ORDER_ASC);
                }
                break;
            default:
                $collection->setOrder('sort_order', \Magento\Framework\Data\Collection\AbstractDb::SORT_ORDER_ASC);
        }

        // Allow load base image for each store
        $collection->setLoadBaseImage(true);
        $merged_times = array();
        $schedule = array();
        foreach($collection as $str)
        {
            $unmerged_times = array();
            $merged_times = array();
            $storeObj = $this->_objectManager->create('Magestore\Storelocator\Model\Store')->load($str['storelocator_id']);
            if($storeObj->getMondayStatus())
            {
                $str['monday'] = '';
                if($storeObj->getMondayOpen() < 12 )
                    $str['monday'] .= ($storeObj->getMondayOpen()-0).'am - ';
                elseif($storeObj->getMondayOpen()-12 == 0)
                    $str['monday'] .= ($storeObj->getMondayOpen()-0).'pm - ';
                else
                    $str['monday'] .= ($storeObj->getMondayOpen()-12).'pm - ';
                if($storeObj->getMondayClose() < 12 )
                    $str['monday'] .= ($storeObj->getMondayClose()-0).'am';
                elseif($storeObj->getMondayClose()-12 == 0)
                    $str['monday'] .= ($storeObj->getMondayClose()-0).'pm';
                else
                    $str['monday'] .= ($storeObj->getMondayClose()-12).'pm';
                if (array_key_exists($str['monday'], $unmerged_times))
                    $unmerged_times[$str['monday']] = $unmerged_times[$str['monday']].' , Mon';
                else
                    $unmerged_times[$str['monday']] = 'Mon';
            }
            
            if($storeObj->getTuesdayStatus())
            {
                $str['tuesday'] = '';
                if($storeObj->getTuesdayOpen() < 12 )
                    $str['tuesday'] .= ($storeObj->getTuesdayOpen()-0).'am - ';
                elseif($storeObj->getTuesdayOpen()-12 == 0)
                    $str['tuesday'] .= ($storeObj->getTuesdayOpen()-0).'pm - ';
                else
                    $str['tuesday'] .= ($storeObj->getTuesdayOpen()-12).'pm - ';
                if($storeObj->getTuesdayClose() < 12 )
                    $str['tuesday'] .= ($storeObj->getTuesdayClose()-0).'am';
                elseif($storeObj->getTuesdayClose()-12 == 0)
                    $str['tuesday'] .= ($storeObj->getTuesdayClose()-0).'pm';
                else
                    $str['tuesday'] .= ($storeObj->getTuesdayClose()-12).'pm';
                if (array_key_exists($str['tuesday'], $unmerged_times))
                    $unmerged_times[$str['tuesday']] = $unmerged_times[$str['tuesday']].' , Tue';
                else
                    $unmerged_times[$str['tuesday']] = 'Tue';
            }
            
            if($storeObj->getWednesdayStatus())
            {
                $str['wednesday'] = '';
                if($storeObj->getWednesdayOpen() < 12 )
                    $str['wednesday'] .= ($storeObj->getWednesdayOpen()-0).'am - ';
                elseif($storeObj->getWednesdayOpen()-12 == 0)
                    $str['wednesday'] .= ($storeObj->getWednesdayOpen()-0).'pm - ';
                else
                    $str['wednesday'] .= ($storeObj->getWednesdayOpen()-12).'pm - ';
                if($storeObj->getWednesdayClose() < 12 )
                    $str['wednesday'] .= ($storeObj->getWednesdayClose()-0).'am';
                elseif($storeObj->getWednesdayClose()-12 == 0)
                    $str['wednesday'] .= ($storeObj->getWednesdayClose()-0).'pm';
                else
                    $str['wednesday'] .= ($storeObj->getWednesdayClose()-12).'pm';
                if (array_key_exists($str['wednesday'], $unmerged_times))
                    $unmerged_times[$str['wednesday']] = $unmerged_times[$str['wednesday']].' , Wed';
                else
                    $unmerged_times[$str['wednesday']] = 'Wed';
            }
            
            if($storeObj->getThursdayStatus())
            {
                $str['thursday'] = '';
                if($storeObj->getThursdayOpen() < 12 )
                    $str['thursday'] .= ($storeObj->getThursdayOpen()-0).'am - ';
                elseif($storeObj->getThursdayOpen()-12 == 0)
                    $str['thursday'] .= ($storeObj->getThursdayOpen()-0).'pm - ';
                else
                    $str['thursday'] .= ($storeObj->getThursdayOpen()-12).'pm - ';
                if($storeObj->getThursdayClose() < 12 )
                    $str['thursday'] .= ($storeObj->getThursdayClose()-0).'am';
                elseif($storeObj->getThursdayClose()-12 == 0)
                    $str['thursday'] .= ($storeObj->getThursdayClose()-0).'pm';
                else
                    $str['thursday'] .= ($storeObj->getThursdayClose()-12).'pm';
                if (array_key_exists($str['thursday'], $unmerged_times))
                    $unmerged_times[$str['thursday']] = $unmerged_times[$str['thursday']].' , Thurs';
                else
                    $unmerged_times[$str['thursday']] = 'Fri';
            }
            
            if($storeObj->getFridayStatus())
            {
                $str['friday'] = '';
                if($storeObj->getFridayOpen() < 12 )
                    $str['friday'] .= ($storeObj->getFridayOpen()-0).'am - ';
                elseif($storeObj->getFridayOpen()-12 == 0)
                    $str['friday'] .= ($storeObj->getFridayOpen()-0).'pm - ';
                else
                    $str['friday'] .= ($storeObj->getFridayOpen()-12).'pm - ';
                if($storeObj->getFridayClose() < 12 )
                    $str['friday'] .= ($storeObj->getFridayClose()-0).'am';
                elseif($storeObj->getFridayClose()-12 == 0)
                    $str['friday'] .= ($storeObj->getFridayClose()-0).'pm';
                else
                    $str['friday'] .= ($storeObj->getFridayClose()-12).'pm';
                if (array_key_exists($str['friday'], $unmerged_times))
                    $unmerged_times[$str['friday']] = $unmerged_times[$str['friday']].' , Fri';
                else
                    $unmerged_times[$str['friday']] = 'Fri';
            }
            
            if($storeObj->getSaturdayStatus())
            {
                $str['saturday'] = '';
                if($storeObj->getSaturdayOpen() < 12 )
                    $str['saturday'] .= ($storeObj->getSaturdayOpen()-0).'am - ';
                elseif($storeObj->getSaturdayOpen()-12 == 0)
                    $str['saturday'] .= ($storeObj->getSaturdayOpen()-0).'pm - ';
                else
                    $str['saturday'] .= ($storeObj->getSaturdayOpen()-12).'pm - ';
                if($storeObj->getSaturdayClose() < 12 )
                    $str['saturday'] .= ($storeObj->getSaturdayClose()-0).'am';
                elseif($storeObj->getSaturdayClose()-12 == 0)
                    $str['saturday'] .= ($storeObj->getSaturdayClose()-0).'pm';
                else
                    $str['saturday'] .= ($storeObj->getSaturdayClose()-12).'pm';
                if (array_key_exists($str['saturday'], $unmerged_times))
                    $unmerged_times[$str['saturday']] = $unmerged_times[$str['saturday']].' , Sat';
                else
                    $unmerged_times[$str['saturday']] = 'Sat';
            }
            if($storeObj->getSundayStatus())
            {
                $str['sunday'] = '';
                if($storeObj->getSundayOpen() < 12 )
                    $str['sunday'] .= ($storeObj->getSundayOpen()-0).'am - ';
                elseif($storeObj->getSundayOpen()-12 == 0)
                    $str['sunday'] .= ($storeObj->getSundayOpen()-0).'pm - ';
                else
                    $str['sunday'] .= ($storeObj->getSundayOpen()-12).'pm - ';
                if($storeObj->getSundayClose() < 12 )
                    $str['sunday'] .= ($storeObj->getSundayClose()-0).'am';
                elseif($storeObj->getSundayClose()-12 == 0)
                    $str['sunday'] .= ($storeObj->getSundayClose()-0).'pm';
                else
                    $str['sunday'] .= ($storeObj->getSundayClose()-12).'pm';
                if (array_key_exists($str['sunday'], $unmerged_times))
                    $unmerged_times[$str['sunday']] = $unmerged_times[$str['sunday']].' , Sun';
                else
                    $unmerged_times[$str['sunday']] = 'Sun';

            }
            
            $count_i = 0;
            foreach($unmerged_times as $key => $val)
            {
				$merged_times[$count_i]= array('key'=>$key,'val'=>$val);
				$count_i++;
			}
            $str->setDates($merged_times);
        }
       return $collection;
    }

}
