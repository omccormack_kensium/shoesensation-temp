<?php
/**
 * Copyright © 2016 Kensium . All rights reserved.
 */
namespace Kensium\EmailCart\Controller\EmailCart;

class Index extends \Magento\Framework\App\Action\Action
{

    /**
     * Recipient email config path
     */
    const XML_PATH_EMAIL_RECIPIENT = 'contact/email/recipient_email';
    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var \Magento\Framework\Escaper
     */
    protected $_escaper;

    protected $checkSession;

    protected $currency;
    protected $resultPageFactory;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Checkout\Model\Session $checkSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Escaper $escaper,
        \Magento\Directory\Model\Currency $currency
    ) {
        parent::__construct($context);
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->_escaper = $escaper;
        $this->_cacheTypeList = $cacheTypeList;
        $this->cart = $cart;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
        $this->checkSession = $checkSession;
        $this->scopeConfig = $scopeConfig;

        $this->currency = $currency;
    }

    /**
     * Post user question
     *
     * @return void
     * @throws \Exception
     */
    public function execute()
    {

        $storeId = $this->_request->getParam('store');
        if ($storeId == NULL || $storeId == 1 )
        {
            $storeId = 0;
        }

        if ($storeId == 0)
        {
            $scopeType = "default";
        }
        else
        {
            $scopeType = "stores";
        }
        $quote = $this->checkSession->getQuote();
        $quote->collectTotals();

        $items = $this->cart->getItems();
        $gap = 105;
        $realTotalPrice = 0;
        $priceAfterDiscount = 0;
        $totalDiscountAmount = 0;
        $totalSpecialPrice = 0;
        $totalRegularPrice = 0;

        //print_r($items);
        if(count($items) <= 0)
        {
            echo "ttttt";
            $this->messageManager->addError(
                __('We can\'t process your request right now. Sorry, that\'s all we know.')
            );
            $resultRedirect = $this->resultRedirectFactory->create();

            return $resultRedirect->setPath('checkout/cart/');
        }
        foreach($items as $item)
        {
            if(!$item->getParentItemId()) {
                $totalSpecialPrice = $totalSpecialPrice + $item->getProduct()->getFinalPrice() * $item->getQty();
                $totalRegularPrice = $totalRegularPrice + $item->getProduct()->getPrice() * $item->getQty();
                $realTotalPrice = $realTotalPrice + $item->getProduct()->getPrice() * $item->getQty(); // this part of the code gets the real price of the product without the discount being applied to the product
                $priceAfterDiscount = $priceAfterDiscount + $item->getPrice() * $item->getQty();
                //$totalDiscountAmount = $totalDiscountAmount + ($realTotalPrice - $priceAfterDiscount);


            }
        }
        /** @var \Magento\Quote\Model\Quote  */
        $totals = $quote->getTotals();

        $post = $this->getRequest()->getPostValue();
        $emailIds = $this->_request->getParam('eamilids'); // array mail ids submited for the purpose of sending mail.
        $message = $this->_request->getParam('msgtosend');
        $emailIdsArr = explode(',', $emailIds);


        /* Format Price */
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of Object Manager
        $priceHelper = $objectManager->create('Magento\Framework\Pricing\Helper\Data'); // Instance of Pricing Helper

        $totalDiscountAmount = $totals["subtotal"]->getValue() - $totals["grand_total"]->getValue();
        $senderType = $this->scopeConfig->getValue('emailcart/emailcart_sender/emailcart_sender_type',$scopeType,$storeId);
        $senderEmail = $this->scopeConfig->getValue('emailcart/emailcart_sender/emailcart_sender_email',$scopeType,$storeId);
        $checkouturl = $this->_url->getUrl('checkout/cart', ['_secure' => true]);
        foreach($emailIdsArr as $emailId){
            $this->inlineTranslation->suspend();
            try {
                $post['message'] = $message;
                $post['totalSpecialPrice'] = $priceHelper->currency($totalSpecialPrice, true, false);
                $post['totalRegularPrice'] = $priceHelper->currency($totalRegularPrice, true, false);
                $post['realtotalprice'] = $priceHelper->currency($realTotalPrice, true, false);
                $post['grandTotal'] = $priceHelper->currency($totals["grand_total"]->getValue(), true, false);
                $post['totaldiscountamount'] = $priceHelper->currency($totalDiscountAmount, true, false);
                $post['checkouturl'] = $checkouturl;
                $postObject = new \Magento\Framework\DataObject();
                $postObject->setData($post);

                $error = false;

                $sender = [
                    'name' => $this->_escaper->escapeHtml($senderType),
                    'email' => $this->_escaper->escapeHtml($senderEmail),
                ];
                $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
                $transport = $this->_transportBuilder
                    ->setTemplateIdentifier('send_email_email_template') // this code we have mentioned in the email_templates.xml
                    ->setTemplateOptions(
                        [
                            'area' => \Magento\Framework\App\Area::AREA_FRONTEND, // this is using frontend area to get the template file
                            'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                        ]
                    )
                    ->setTemplateVars(['data' => $postObject, 'items' => $items])
                    ->setFrom($sender)
                    ->addTo($emailId)
                    ->getTransport();

                //print_r($transport->getData());exit;
                $transport->sendMessage();

                $this->inlineTranslation->resume();

            } catch (\Exception $e) {
                $this->inlineTranslation->resume();
                $this->messageManager->addError(
                    __('We can\'t process your request right now. Sorry, that\'s all we know.'.$e->getMessage())
                );
            }
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $this->messageManager->addSuccess( __('Successfully Sent Mail.'));
        return $resultRedirect->setPath('checkout/cart/');
    }
}
