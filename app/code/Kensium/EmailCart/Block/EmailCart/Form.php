<?php
/**
 * Copyright © 2016 Kensium . All rights reserved.
*/
namespace Kensium\EmailCart\Block\EmailCart;

class Form extends \Magento\Framework\View\Element\Template
{

    public function __construct( \Magento\Framework\View\Element\Template\Context $context,array $data = [])
    {
            parent::__construct($context, $data);
    }

    public function _prepareLayout() {
       return parent::_prepareLayout();
    }
}

?>
