<?php
/**
 * Solwin Infotech
 * Solwin Customer Feedback Extension
 *
 * @category   Solwin
 * @package    Solwin_CustomerFeedback
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/ 
 */
?>
<?php
namespace Solwin\CustomerFeedback\Controller\Adminhtml;

abstract class Feedback extends \Magento\Backend\App\Action
{
    /**
     * Feedback Factory
     * 
     * @var \Solwin\CustomerFeedback\Model\FeedbackFactory
     */
    protected $_feedbackFactory;

    /**
     * Core registry
     * 
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * constructor
     * 
     * @param \Solwin\CustomerFeedback\Model\FeedbackFactory $feedbackFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Solwin\CustomerFeedback\Model\FeedbackFactory $feedbackFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->_feedbackFactory       = $feedbackFactory;
        $this->_coreRegistry          = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Init Feedback
     *
     * @return \Solwin\CustomerFeedback\Model\Feedback
     */
    protected function initFeedback()
    {
        $feedbackId  = (int) $this->getRequest()->getParam('feedback_id');
        /** @var \Solwin\CustomerFeedback\Model\Feedback $feedback */
        $feedback    = $this->_feedbackFactory->create();
        if ($feedbackId) {
            $feedback->load($feedbackId);
        }
        $this->_coreRegistry
                ->register('solwin_customerfeedback_feedback', $feedback);
        return $feedback;
    }
}