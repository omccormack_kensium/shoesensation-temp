<?php
/**
 * Solwin Infotech
 * Solwin Customer Feedback Extension
 *
 * @category   Solwin
 * @package    Solwin_CustomerFeedback
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/ 
 */
?>
<?php
namespace Solwin\CustomerFeedback\Controller\Adminhtml\Feedback;

class Delete extends \Solwin\CustomerFeedback\Controller\Adminhtml\Feedback
{
    /**
     * execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('feedback_id');
        if ($id) {
            $name = "";
            try {
                /** @var \Solwin\CustomerFeedback\Model\Feedback $feedback */
                $feedback = $this->_feedbackFactory->create();
                $feedback->load($id);
                $name = $feedback->getName();
                $feedback->delete();
                $this->messageManager
                        ->addSuccess(__('The Feedback has been deleted.'));
                $this->_eventManager->dispatch(
                    'adminhtml_solwin_customerfeedback_feedback_on_delete',
                    ['name' => $name, 'status' => 'success']
                );
                $resultRedirect->setPath('solwin_customerfeedback/*/');
                return $resultRedirect;
            } catch (\Exception $e) {
                $this->_eventManager->dispatch(
                    'adminhtml_solwin_customerfeedback_feedback_on_delete',
                    ['name' => $name, 'status' => 'fail']
                );
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                $resultRedirect->setPath('solwin_customerfeedback/*/edit',
                        ['feedback_id' => $id]);
                return $resultRedirect;
            }
        }
        // display error message
        $this->messageManager
                ->addError(__('Feedback to delete was not found.'));
        // go to grid
        $resultRedirect->setPath('solwin_customerfeedback/*/');
        return $resultRedirect;
    }
}