<?php
/**
 * Solwin Infotech
 * Solwin Customer Feedback Extension
 *
 * @category   Solwin
 * @package    Solwin_CustomerFeedback
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/ 
 */
?>
<?php
namespace Solwin\CustomerFeedback\Controller\Adminhtml\Feedback;

abstract class InlineEdit extends \Magento\Backend\App\Action
{
    /**
     * JSON Factory
     * 
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_jsonFactory;

    /**
     * Feedback Factory
     * 
     * @var \Solwin\CustomerFeedback\Model\FeedbackFactory
     */
    protected $_feedbackFactory;

    /**
     * constructor
     * 
     * @param \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
     * @param \Solwin\CustomerFeedback\Model\FeedbackFactory $feedbackFactory
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory,
        \Solwin\CustomerFeedback\Model\FeedbackFactory $feedbackFactory,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->_jsonFactory     = $jsonFactory;
        $this->_feedbackFactory = $feedbackFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->_jsonFactory->create();
        $error = false;
        $messages = [];
        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ]);
        }
        foreach (array_keys($postItems) as $feedbackId) {
            /** @var \Solwin\CustomerFeedback\Model\Feedback $feedback */
            $feedback = $this->_feedbackFactory->create()->load($feedbackId);
            try {
                $feedbackData = $postItems[$feedbackId];//todo: handle dates
                $feedback->addData($feedbackData);
                $feedback->save();
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $messages[] = $this->getErrorWithFeedbackId(
                        $feedback, $e->getMessage());
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithFeedbackId(
                        $feedback, $e->getMessage());
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithFeedbackId(
                    $feedback,
                    __('Something went wrong while saving the Feedback.')
                );
                $error = true;
            }
        }
        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * Add Feedback id to error message
     *
     * @param \Solwin\CustomerFeedback\Model\Feedback $feedback
     * @param string $errorText
     * @return string
     */
    protected function getErrorWithFeedbackId(
        \Solwin\CustomerFeedback\Model\Feedback $feedback,
        $errorText
    ) {
        return '[Feedback ID: ' . $feedback->getId() . '] ' . $errorText;
    }
}