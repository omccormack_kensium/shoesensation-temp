<?php
/**
 * Solwin Infotech
 * Solwin Customer Feedback Extension
 *
 * @category   Solwin
 * @package    Solwin_CustomerFeedback
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/ 
 */
?>
<?php
namespace Solwin\CustomerFeedback\Controller\Adminhtml\Feedback;

use Magento\Framework\Controller\Result\JsonFactory;

class Edit extends \Solwin\CustomerFeedback\Controller\Adminhtml\Feedback
{
    
    /**
     * Page factory
     * 
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Result JSON factory
     * 
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * constructor
     * 
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param JsonFactory $resultJsonFactory
     * @param \Solwin\CustomerFeedback\Model\FeedbackFactory $feedbackFactory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory,
        \Solwin\CustomerFeedback\Model\FeedbackFactory $feedbackFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        parent::__construct($feedbackFactory, $registry, $context);
    }

    /**
     * is action allowed
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization
                ->isAllowed('Solwin_CustomerFeedback::feedback');
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page|
     * \Magento\Backend\Model\View\Result\Redirect|
     * \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('feedback_id');
        /**
         * @var \Solwin\CustomerFeedback\Model\Feedback $feedback
         */
        $feedback = $this->initFeedback();
        /** @var \Magento\Backend\Model\View\Result\Page|
         * \Magento\Framework\View\Result\Page $resultPage
         */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Solwin_CustomerFeedback::feedback');
        $resultPage->getConfig()->getTitle()->set(__('Feedbacks'));
        if ($id) {
            $feedback->load($id);
            if (!$feedback->getId()) {
                $this->messageManager
                        ->addError(__('This Feedback no longer exists.'));
                $resultRedirect = $this->_resultRedirectFactory->create();
                $resultRedirect->setPath(
                    'solwin_customerfeedback/*/edit',
                    [
                        'feedback_id' => $feedback->getId(),
                        '_current' => true
                    ]
                );
                return $resultRedirect;
            }
        }
        $title = $feedback->getId() ? $feedback->getName() : __('New Feedback');
        $resultPage->getConfig()->getTitle()->prepend($title);
        $data = $this->_session
                ->getData('solwin_customerfeedback_feedback_data', true);
        if (!empty($data)) {
            $feedback->setData($data);
        }
        return $resultPage;
    }
}