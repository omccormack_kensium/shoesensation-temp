<?php
/**
 * Solwin Infotech
 * Solwin Customer Feedback Extension
 *
 * @category   Solwin
 * @package    Solwin_CustomerFeedback
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/ 
 */
?>
<?php
namespace Solwin\CustomerFeedback\Controller\Adminhtml\Feedback;

class Save extends \Solwin\CustomerFeedback\Controller\Adminhtml\Feedback
{

    /**
     * constructor
     * 
     * @param \Solwin\CustomerFeedback\Model\FeedbackFactory $feedbackFactory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Solwin\CustomerFeedback\Model\FeedbackFactory $feedbackFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\App\Action\Context $context
    ) {
        parent::__construct($feedbackFactory, $registry, $context);
    }

    /**
     * run the action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $data = $this->getRequest()->getPost('feedback');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $feedback = $this->initFeedback();
            $feedback->setData($data);
            $this->_eventManager->dispatch(
                'solwin_customerfeedback_feedback_prepare_save',
                [
                    'feedback' => $feedback,
                    'request' => $this->getRequest()
                ]
            );
            try {
                $feedback->save();
                $this->messageManager
                        ->addSuccess(__('The Feedback has been saved.'));
                $this->_session
                        ->setSolwinCustomerFeedbackFeedbackData(false);
                if ($this->getRequest()->getParam('back')) {
                    $resultRedirect->setPath(
                        'solwin_customerfeedback/*/edit',
                        [
                            'feedback_id' => $feedback->getId(),
                            '_current' => true
                        ]
                    );
                    return $resultRedirect;
                }
                $resultRedirect->setPath('solwin_customerfeedback/*/');
                return $resultRedirect;
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager
                        ->addException($e,
                        __('Something went wrong while saving the Feedback.'));
            }
            $this->_getSession()->setSolwinCustomerFeedbackFeedbackData($data);
            $resultRedirect->setPath(
                'solwin_customerfeedback/*/edit',
                [
                    'feedback_id' => $feedback->getId(),
                    '_current' => true
                ]
            );
            return $resultRedirect;
        }
        $resultRedirect->setPath('solwin_customerfeedback/*/');
        return $resultRedirect;
    }
}
