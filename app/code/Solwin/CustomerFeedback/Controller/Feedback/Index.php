<?php
/**
 * Solwin Infotech
 * Solwin Customer Feedback Extension
 *
 * @category   Solwin
 * @package    Solwin_CustomerFeedback
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/ 
 */
?>
<?php
namespace Solwin\CustomerFeedback\Controller\Feedback;

use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;

class Index extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

     /**
     * @var StateInterface
     */
    protected $_inlineTranslation;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
    
    /**
     * @var \Solwin\CustomerFeedback\Helper\Data
     */
    protected $_helper;
    
    const EMAIL_TEMPLATE = 'customerfeedbacksection/emailopt/template';
    const EMAIL_SENDER = 'customerfeedbacksection/emailopt/emailsender';
    const EMAIL_RECIPIENT = 'customerfeedbacksection/emailopt/emailto';
    const REQUEST_URL = 'https://www.google.com/recaptcha/api/siteverify';
    const REQUEST_RESPONSE = 'g-recaptcha-response';

    /**
     * @param Action\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
     public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Solwin\CustomerFeedback\Helper\Data $helper
    ) {
        parent::__construct($context);
        $this->_customerSession = $customerSession;
        $this->_transportBuilder = $transportBuilder;
        $this->_inlineTranslation = $inlineTranslation;
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
        $this->_helper = $helper;
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function getCustId() {
        return $this->_customerSession->getCustomerId();
    }

    public function execute() {
       
        $data = $this->getRequest()->getPostValue();

        if (isset($data['callme']) && $data['callme'] == 'Yes') {
            $data['callme'] = 'Yes';
        }
        else {
            $data['callme'] = 'No';
        }
        
        $resultRedirect = $this->resultRedirectFactory->create();
        $enableCaptcha = $this->_helper
                ->getConfigValue('customerfeedbacksection/recaptcha/enable');
	$secretkey = $this->_helper
            ->getConfigValue('customerfeedbacksection/recaptcha/'
                    . 'recaptcha_secretkey');
        $captchaErrorMsg = $this->_helper
                ->getConfigValue('customerfeedbacksection/recaptcha/'
                        . 'recaptcha_errormessage');

        if (!$data) {
            return $resultRedirect->setRefererUrl();
        }
	
        if ($enableCaptcha) {
            if ($captchaErrorMsg == '') {
                $captchaErrorMsg = 'Invalid captcha. Please try again.';
            }
            $captcha = '';
            if (isset($_POST['g-recaptcha-response'])) {
                $captcha = $_POST['g-recaptcha-response'];
            }

            if (!$captcha) {
                $this->messageManager->addError($captchaErrorMsg);
                return $resultRedirect->setUrl($redirectUrl);
            } else {
                if ($secretkey != '') {
                    $response = file_get_contents("https://www.google.com/"
                            . "recaptcha/api/siteverify?secret=" 
                            . $secretkey . "&response=" .
                            $captcha . "&remoteip=" .
                            $_SERVER['REMOTE_ADDR']);
                    $response = json_decode($response, true);
                } else {
                    $this->messageManager
                            ->addError('Please enter secret key to'
                                    . ' validate form data using recaptcha.');
                    return $resultRedirect->setUrl($redirectUrl);
                }
                if ($response["success"] === false) {
                    $this->messageManager->addError($captchaErrorMsg);
                    return $resultRedirect->setUrl($redirectUrl);
                }
            }
        }
        
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        try {
            $model = $this->_objectManager
                    ->create('Solwin\CustomerFeedback\Model\Feedback');

            $postObject = new \Magento\Framework\DataObject();
            $postObject->setData($data);

            $error = false;

            if (!\Zend_Validate::is(trim($data['name']), 'NotEmpty')) {
                $error = true;
            }

            if (!\Zend_Validate::is(trim($data['email']), 'NotEmpty')) {
                $error = true;
            }
            
            if (!\Zend_Validate::is(trim($data['phone']), 'NotEmpty')) {
                $error = true;
            }
            
            if (!\Zend_Validate::is(trim($data['callme']), 'NotEmpty')) {
                $error = true;
            }

            if (!\Zend_Validate::is(trim($data['website']), 'NotEmpty')) {
                $error = true;
            }
            
            if (!\Zend_Validate::is(trim($data['message']), 'NotEmpty')) {
                $error = true;
            }

            if ($error) {
                throw new \Exception();
            }

            $model->setData($data);
            $model->setData('callme',$data['callme']);
            $model->save();
            
              // send mail to recipients
            $this->_inlineTranslation->suspend();
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $transport = $this->_transportBuilder->setTemplateIdentifier(
                            $this->_scopeConfig
                    ->getValue(self::EMAIL_TEMPLATE, $storeScope)
                    )->setTemplateOptions(
                        [
                           'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                           'store' => $this->_storeManager->getStore()->getId(),
                        ]
                    )->setTemplateVars(['data' => $postObject])
                    ->setFrom($this->_scopeConfig
                           ->getValue(self::EMAIL_SENDER, $storeScope))
                    ->addTo(
                            $this->_scopeConfig
                            ->getValue(
                                    self::EMAIL_RECIPIENT,
                                $storeScope))
                    ->getTransport();

            $transport->sendMessage();
            $this->_inlineTranslation->resume();
            
            $this->messageManager
                    ->addSuccess(__('Customer Feedback has been submitted.'));
            $this->_objectManager
                    ->get('Magento\Backend\Model\Session')->setFormData(false);
            
            return $resultRedirect->setRefererUrl();
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\RuntimeException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager
                ->addException($e,
                __('Something went wrong while saving the customer feedback.'));
        }
        return $resultRedirect->setRefererUrl();
    }

}