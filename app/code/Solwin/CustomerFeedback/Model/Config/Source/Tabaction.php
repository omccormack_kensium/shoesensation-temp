<?php
/**
 * Solwin Infotech
 * Solwin Customer Feedback Extension
 *
 * @category   Solwin
 * @package    Solwin_CustomerFeedback
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/ 
 */
?>
<?php

/**
 * Tab Action options
 *
 */

namespace Solwin\CustomerFeedback\Model\Config\Source;

class Tabaction implements \Magento\Framework\Option\ArrayInterface 
{
    /**
     * @return array
     */
    public function toOptionArray() {
        return [
            ['value' => 'click', 'label' => __('Click')],
            ['value' => 'hover', 'label' => __('Hover')]
        ];
    
    }

}