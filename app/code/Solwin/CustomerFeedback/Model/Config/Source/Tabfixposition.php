<?php
/**
 * Solwin Infotech
 * Solwin Customer Feedback Extension
 *
 * @category   Solwin
 * @package    Solwin_CustomerFeedback
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/ 
 */
?>
<?php

/**
 * Tab Fix position options
 *
 */

namespace Solwin\CustomerFeedback\Model\Config\Source;

class Tabfixposition implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray() {
        return [
            ['value' => 'false', 'label' => __('False')],
            ['value' => 'true', 'label' => __('True')]
        ];
    
    }

}