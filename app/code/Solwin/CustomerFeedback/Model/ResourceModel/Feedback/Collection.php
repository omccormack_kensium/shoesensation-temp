<?php
/**
 * Solwin Infotech
 * Solwin Customer Feedback Extension
 *
 * @category   Solwin
 * @package    Solwin_CustomerFeedback
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/ 
 */
?>
<?php
namespace Solwin\CustomerFeedback\Model\ResourceModel\Feedback;

class Collection
extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * ID Field Name
     * 
     * @var string
     */
    protected $_idFieldName = 'feedback_id';

    /**
     * Event prefix
     * 
     * @var string
     */
    protected $_eventPrefix = 'solwin_customerfeedback_feedback_collection';

    /**
     * Event object
     * 
     * @var string
     */
    protected $_eventObject = 'feedback_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Solwin\CustomerFeedback\Model\Feedback',
                'Solwin\CustomerFeedback\Model\ResourceModel\Feedback');
    }

    /**
     * Get SQL for get record count.
     * Extra GROUP BY strip added.
     *
     * @return \Magento\Framework\DB\Select
     */
    public function getSelectCountSql()
    {
        $countSelect = parent::getSelectCountSql();
        $countSelect->reset(\Zend_Db_Select::GROUP);
        return $countSelect;
    }
    /**
     * @param string $valueField
     * @param string $labelField
     * @param array $additional
     * @return array
     */
    protected function _toOptionArray(
        $valueField = 'feedback_id',
        $labelField = 'name',
        $additional = []
    ) {
        return parent::_toOptionArray($valueField, $labelField, $additional);
    }
}
