<?php
/**
 * Solwin Infotech
 * Solwin Customer Feedback Extension
 *
 * @category   Solwin
 * @package    Solwin_CustomerFeedback
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/ 
 */
?>
<?php
namespace Solwin\CustomerFeedback\Model;

/**
 * @method Feedback setName($name)
 * @method Feedback setEmail($email)
 * @method Feedback setPhone($phone)
 * @method Feedback setCallme($callme)
 * @method Feedback setWebsite($website)
 * @method Feedback setMessage($message)
 * @method Feedback setStatus($status)
 * @method mixed getName()
 * @method mixed getEmail()
 * @method mixed getPhone()
 * @method mixed getCallme()
 * @method mixed getWebsite()
 * @method mixed getMessage()
 * @method mixed getStatus()
 * @method Feedback setCreatedAt(\string $createdAt)
 * @method string getCreatedAt()
 * @method Feedback setUpdatedAt(\string $updatedAt)
 * @method string getUpdatedAt()
 */
class Feedback extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Cache tag
     * 
     * @var string
     */
    const CACHE_TAG = 'solwin_customerfeedback_feedback';

    /**
     * Cache tag
     * 
     * @var string
     */
    protected $_cacheTag = 'solwin_customerfeedback_feedback';

    /**
     * Event prefix
     * 
     * @var string
     */
    protected $_eventPrefix = 'solwin_customerfeedback_feedback';


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Solwin\CustomerFeedback\Model\ResourceModel\Feedback');
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * get entity default values
     *
     * @return array
     */
    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}