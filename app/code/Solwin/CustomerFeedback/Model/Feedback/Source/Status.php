<?php
/**
 * Solwin Infotech
 * Solwin Customer Feedback Extension
 *
 * @category   Solwin
 * @package    Solwin_CustomerFeedback
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/ 
 */
?>
<?php
namespace Solwin\CustomerFeedback\Model\Feedback\Source;

class Status implements \Magento\Framework\Option\ArrayInterface
{
    const OPEN = 1;
    const RESOLVED = 2;


    /**
     * to option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            [
                'value' => self::OPEN,
                'label' => __('Open')
            ],
            [
                'value' => self::RESOLVED,
                'label' => __('Resolved')
            ],
        ];
        return $options;

    }
}