<?php
/**
 * Solwin Infotech
 * Solwin Customer Feedback Extension
 *
 * @category   Solwin
 * @package    Solwin_CustomerFeedback
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/ 
 */
?>
<?php
namespace Solwin\CustomerFeedback\Block\Adminhtml;

class Feedback extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_feedback';
        $this->_blockGroup = 'Solwin_CustomerFeedback';
        $this->_headerText = __('Feedbacks');
        $this->_addButtonLabel = __('Create New Feedback');
        parent::_construct();
    }
}
