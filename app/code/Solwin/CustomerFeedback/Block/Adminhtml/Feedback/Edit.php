<?php
/**
 * Solwin Infotech
 * Solwin Customer Feedback Extension
 *
 * @category   Solwin
 * @package    Solwin_CustomerFeedback
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/ 
 */
?>
<?php
namespace Solwin\CustomerFeedback\Block\Adminhtml\Feedback;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     * 
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * constructor
     * 
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Backend\Block\Widget\Context $context,
        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $data);
    }

    /**
     * Initialize Feedback edit block
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'feedback_id';
        $this->_blockGroup = 'Solwin_CustomerFeedback';
        $this->_controller = 'adminhtml_feedback';
        parent::_construct();
        $this->buttonList->update('save', 'label', __('Save Feedback'));
        $this->buttonList->add(
            'save-and-continue',
            [
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => [
                            'event' => 'saveAndContinueEdit',
                            'target' => '#edit_form'
                        ]
                    ]
                ]
            ],
            -100
        );
        $this->buttonList->update('delete', 'label', __('Delete Feedback'));
    }
    /**
     * Retrieve text for header element depending on loaded Feedback
     *
     * @return string
     */
    public function getHeaderText()
    {
        /** @var \Solwin\CustomerFeedback\Model\Feedback $feedback */
        $feedback = $this->_coreRegistry
                ->registry('solwin_customerfeedback_feedback');
        if ($feedback->getId()) {
            return __("Edit Feedback '%1'",
                    $this->escapeHtml($feedback->getName()));
        }
        return __('New Feedback');
    }
}
