<?php
/**
 * Solwin Infotech
 * Solwin Customer Feedback Extension
 *
 * @category   Solwin
 * @package    Solwin_CustomerFeedback
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/ 
 */
?>
<?php
namespace Solwin\CustomerFeedback\Block\Adminhtml\Feedback\Edit\Tab;

use Solwin\CustomerFeedback\Model\Feedback\Source\Status;

class Feedback extends \Magento\Backend\Block\Widget\Form\Generic
implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * Wysiwyg config
     * 
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    /**
     * Country options
     * 
     * @var \Magento\Config\Model\Config\Source\Yesno
     */
    protected $_booleanOptions;

    /**
     * Status options
     * 
     * @var \Solwin\CustomerFeedback\Model\Feedback\Source\Status
     */
    protected $_statusOptions;

    /**
     * constructor
     * 
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Magento\Config\Model\Config\Source\Yesno $booleanOptions
     * @param Status $statusOptions
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Magento\Config\Model\Config\Source\Yesno $booleanOptions,
        Status $statusOptions,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    )
    {
        $this->_wysiwygConfig  = $wysiwygConfig;
        $this->_booleanOptions = $booleanOptions;
        $this->_statusOptions  = $statusOptions;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Solwin\CustomerFeedback\Model\Feedback $feedback */
        $feedback = $this->_coreRegistry
                ->registry('solwin_customerfeedback_feedback');
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('feedback_');
        $form->setFieldNameSuffix('feedback');
        $fieldset = $form->addFieldset(
            'base_fieldset',
            [
                'legend' => __('Feedback Information'),
                'class'  => 'fieldset-wide'
            ]
        );
        if ($feedback->getId()) {
            $fieldset->addField(
                'feedback_id',
                'hidden',
                ['name' => 'feedback_id']
            );
        }
        $fieldset->addField(
            'name',
            'label',
            [
                'name'  => 'name',
                'label' => __('Name'),
                'title' => __('Name'),
                'required' => true,
            ]
        );
        $fieldset->addField(
            'emailid',
            'label',
            [
                'label' => __('Email'),
                'title' => __('Email'),
                'after_element_html' => ' <a href="mailto:' . $feedback->getEmail() . '" style="text-decoration: none;">'.$feedback->getEmail().'</a>'
            ]
        );
        $fieldset->addField(
            'phone',
            'label',
            [
                'name'  => 'phone',
                'label' => __('Phone'),
                'title' => __('Phone'),
                'required' => true,
            ]
        );
        $fieldset->addField(
            'callme',
            'label',
            [
                'name'  => 'callme',
                'label' => __('Call Me'),
                'title' => __('Call Me'),
                'required' => true,
                'values' => $this->_booleanOptions->toOptionArray(),
            ]
        );
        $fieldset->addField(
            '',
            'label',
            [
                'label' => __('Website'),
                'title' => __('Website'),
                'after_element_html' => ' <a href="' . $feedback->getWebsite() . '" style="text-decoration: none;" target="_blank">'.$feedback->getWebsite().'</a>'
            ]
        );
        $fieldset->addField(
            'message',
            'label',
            [
                'name'  => 'message',
                'label' => __('Message'),
                'title' => __('Message'),
                'required' => true,
                'config'    => $this->_wysiwygConfig->getConfig()
            ]
        );
        $fieldset->addField(
            'status',
            'select',
            [
                'name'  => 'status',
                'label' => __('Status'),
                'title' => __('Status'),
                'required' => true,
                'values' => array_merge(['' => ''],
                        $this->_statusOptions->toOptionArray()),
            ]
        );

        $feedbackData = $this->_session
                ->getData('solwin_customerfeedback_feedback_data', true);
        if ($feedbackData) {
            $feedback->addData($feedbackData);
        } else {
            if (!$feedback->getId()) {
                $feedback->addData($feedback->getDefaultValues());
            }
        }
        $form->addValues($feedback->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Feedback');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
}