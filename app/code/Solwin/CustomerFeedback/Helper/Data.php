<?php
/**
 * Solwin Infotech
 * Solwin Customer Feedback Extension
 *
 * @category   Solwin
 * @package    Solwin_CustomerFeedback
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/ 
 */

namespace Solwin\CustomerFeedback\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * Get form action url
     */

    public function getFormAction() {
        return $this->_urlBuilder->getUrl('customerfeedback/feedback/index');
    }

    /** 
     * Get product url suffix
     */

    public function getConfigValue($value = '') {
        return $this->scopeConfig
                ->getValue(
                        $value, 
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                        );
    }

}