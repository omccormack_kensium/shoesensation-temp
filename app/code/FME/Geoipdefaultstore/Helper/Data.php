<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace FME\Geoipdefaultstore\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\View\Asset\Repository;
use Magento\Framework\App\Filesystem\DirectoryList;

class Data extends AbstractHelper {

    protected $_resource;

    /**
     * Logger
     *
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     *
     * @var Magento\Framework\View\Asset\Repository 
     */
    protected $_gdsAssetRepo;
    protected $_directoryList;

    const XML_PATH_ENABLED = 'geoipdefaultstore/general/enable_in_frontend';
    const XML_PATH_AUTO_REDIRECT_ENABLED = 'geoipdefaultstore/general/enable_autoredirect';
    const XML_PATH_POPUP_TITLE = 'geoipdefaultstore/popup/popup_title';
    const XML_PATH_POPUP_BTN_TEXT = 'geoipdefaultstore/popup/popup_btn_text';

    public function __construct(Context $context, Repository $repo, DirectoryList $directoryList) {
        parent::__construct($context);
        $this->_resource = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('\Magento\Framework\App\ResourceConnection');
        $this->_gdsAssetRepo = $repo;
        $this->_logger = $context->getLogger();
        $this->_directoryList = $directoryList;
    }

    public function getPopupTitle($store = null) {
        return $this->scopeConfig->getValue(self::XML_PATH_POPUP_TITLE, ScopeInterface::SCOPE_STORE);
    }

    public function getPopupButtonText($store = null) {
        return $this->scopeConfig->getValue(self::XML_PATH_POPUP_BTN_TEXT, ScopeInterface::SCOPE_STORE);
    }

    /**
     * 
     * check the module is enabled, frontend
     * 
     * @param mix $store 
     * @return string
     */
    public function isEnabledInFrontend($store = null) {
        $isEnabled = true;
        $enabled = $this->scopeConfig->getValue(self::XML_PATH_ENABLED, ScopeInterface::SCOPE_STORE);
        if ($enabled == NULL || $enabled == '0') {
            $isEnabled = false;
        }
        return $isEnabled;
    }

    public function isAutoRedirectEnabled($store = null) {
        return $this->scopeConfig->getValue(self::XML_PATH_AUTO_REDIRECT_ENABLED, ScopeInterface::SCOPE_STORE);
    }

    public function getContinentsName($key) {

        $continentsArr = array(
            1 => 'Africa',
            2 => 'Asia',
            3 => 'Europe',
            4 => 'North America',
            5 => 'Oceania',
            6 => 'South America',
            7 => 'Others'
        );

        if (isset($continentsArr[$key])) {

            return $continentsArr[$key];
        }
    }

    public function getcontinent($country = "") {

        switch ($country) {
            case "Algeria": {
                    $continent = 1;
                    break;
                }
            case "Angola": {
                    $continent = 1;
                    break;
                }
            case "Benin": {
                    $continent = 1;
                    break;
                }
            case "Botswana": {
                    $continent = 1;
                    break;
                }
            case "Burkina Faso": {
                    $continent = 1;
                    break;
                }
            case "Burundi": {
                    $continent = 1;
                    break;
                }
            case "Cameroon": {
                    $continent = 1;
                    break;
                }
            case "Cape Verde": {
                    $continent = 1;
                    break;
                }
            case "Central African Republic": {
                    $continent = 1;
                    break;
                }
            case "Chad": {
                    $continent = 1;
                    break;
                }
            case "Comoros": {
                    $continent = 1;
                    break;
                }
            case "Congo": {
                    $continent = 1;
                    break;
                }
            case "Congo, The Democratic Republic of the": {
                    $continent = 1;
                    break;
                }
            case "Djibouti": {
                    $continent = 1;
                    break;
                }
            case "Egypt": {
                    $continent = 1;
                    break;
                }
            case "Equatorial Guinea": {
                    $continent = 1;
                    break;
                }
            case "Eritrea": {
                    $continent = 1;
                    break;
                }
            case "Ethiopia": {
                    $continent = 1;
                    break;
                }
            case "Gabon": {
                    $continent = 1;
                    break;
                }
            case "Gambia": {
                    $continent = 1;
                    break;
                }
            case "Ghana": {
                    $continent = 1;
                    break;
                }
            case "Guinea": {
                    $continent = 1;
                    break;
                }
            case "Guinea-Bissau": {
                    $continent = 1;
                    break;
                }
            case "Cote D'Ivoire": {
                    $continent = 1;
                    break;
                }
            case "Kenya": {
                    $continent = 1;
                    break;
                }
            case "Lesotho": {
                    $continent = 1;
                    break;
                }
            case "Liberia": {
                    $continent = 1;
                    break;
                }
            case "Libyan Arab Jamahiriya": {
                    $continent = 1;
                    break;
                }
            case "Madagascar": {
                    $continent = 1;
                    break;
                }
            case "Malawi": {
                    $continent = 1;
                    break;
                }
            case "Mali": {
                    $continent = 1;
                    break;
                }
            case "Mauritania": {
                    $continent = 1;
                    break;
                }
            case "Mauritius": {
                    $continent = 1;
                    break;
                }
            case "Morocco": {
                    $continent = 1;
                    break;
                }
            case "Mozambique": {
                    $continent = 1;
                    break;
                }
            case "Namibia": {
                    $continent = 1;
                    break;
                }
            case "Niger": {
                    $continent = 1;
                    break;
                }
            case "Nigeria": {
                    $continent = 1;
                    break;
                }
            case "Rwanda": {
                    $continent = 1;
                    break;
                }
            case "Sao Tome and Principe": {
                    $continent = 1;
                    break;
                }
            case "Senegal": {
                    $continent = 1;
                    break;
                }
            case "Seychelles": {
                    $continent = 1;
                    break;
                }
            case "Sierra Leone": {
                    $continent = 1;
                    break;
                }
            case "Somalia": {
                    $continent = 1;
                    break;
                }
            case "South Africa": {
                    $continent = 1;
                    break;
                }
            case "Sudan": {
                    $continent = 1;
                    break;
                }
            case "Swaziland": {
                    $continent = 1;
                    break;
                }
            case "Tanzania, United Republic of": {
                    $continent = 1;
                    break;
                }
            case "Togo": {
                    $continent = 1;
                    break;
                }
            case "Tunisia": {
                    $continent = 1;
                    break;
                }
            case "Uganda": {
                    $continent = 1;
                    break;
                }
            case "Zambia": {
                    $continent = 1;
                    break;
                }
            case "Zimbabwe": {
                    $continent = 1;
                    break;
                }
            case "Afghanistan": {
                    $continent = 2;
                    break;
                }
            case "Bahrain": {
                    $continent = 2;
                    break;
                }
            case "Bangladesh": {
                    $continent = 2;
                    break;
                }
            case "Bhutan": {
                    $continent = 2;
                    break;
                }
            case "Brunei Darussalam": {
                    $continent = 2;
                    break;
                }
            case "Myanmar": {
                    $continent = 2;
                    break;
                }
            case "Cambodia": {
                    $continent = 2;
                    break;
                }
            case "China": {
                    $continent = 2;
                    break;
                }
            case "Timor-Leste": {
                    $continent = 2;
                    break;
                }
            case "India": {
                    $continent = 2;
                    break;
                }
            case "Indonesia": {
                    $continent = 2;
                    break;
                }
            case "Iran, Islamic Republic of": {
                    $continent = 2;
                    break;
                }
            case "Iraq": {
                    $continent = 2;
                    break;
                }
            case "Israel": {
                    $continent = 2;
                    break;
                }
            case "Japan": {
                    $continent = 2;
                    break;
                }
            case "Jordan": {
                    $continent = 2;
                    break;
                }
            case "Kazakhstan": {
                    $continent = 2;
                    break;
                }
            case "Korea, Democratic People's Republic of": {
                    $continent = 2;
                    break;
                }
            case "Korea, Republic of": {
                    $continent = 2;
                    break;
                }
            case "Kuwait": {
                    $continent = 2;
                    break;
                }
            case "Kyrgyzstan": {
                    $continent = 2;
                    break;
                }
            case "Lao People's Democratic Republic": {
                    $continent = 2;
                    break;
                }
            case "Lebanon": {
                    $continent = 2;
                    break;
                }
            case "Malaysia": {
                    $continent = 2;
                    break;
                }
            case "Maldives": {
                    $continent = 2;
                    break;
                }
            case "Mongolia": {
                    $continent = 2;
                    break;
                }
            case "Nepal": {
                    $continent = 2;
                    break;
                }
            case "Oman": {
                    $continent = 2;
                    break;
                }
            case "Pakistan": {
                    $continent = 2;
                    break;
                }
            case "Philippines": {
                    $continent = 2;
                    break;
                }
            case "Qatar": {
                    $continent = 2;
                    break;
                }
            case "Russian Federation": {
                    $continent = 2;
                    break;
                }
            case "Saudi Arabia": {
                    $continent = 2;
                    break;
                }
            case "Singapore": {
                    $continent = 2;
                    break;
                }
            case "Sri Lanka": {
                    $continent = 2;
                    break;
                }
            case "Syrian Arab Republic": {
                    $continent = 2;
                    break;
                }
            case "Tajikistan": {
                    $continent = 2;
                    break;
                }
            case "Thailand": {
                    $continent = 2;
                    break;
                }
            case "Turkey": {
                    $continent = 2;
                    break;
                }
            case "Turkmenistan": {
                    $continent = 2;
                    break;
                }
            case "United Arab Emirates": {
                    $continent = 2;
                    break;
                }
            case "Uzbekistan": {
                    $continent = 2;
                    break;
                }
            case "Vietnam": {
                    $continent = 2;
                    break;
                }
            case "Yemen": {
                    $continent = 2;
                    break;
                }
            case "Albania": {
                    $continent = 3;
                    break;
                }
            case "Andorra": {
                    $continent = 3;
                    break;
                }
            case "Armenia": {
                    $continent = 3;
                    break;
                }
            case "Austria": {
                    $continent = 3;
                    break;
                }
            case "Azerbaijan": {
                    $continent = 3;
                    break;
                }
            case "Belarus": {
                    $continent = 3;
                    break;
                }
            case "Belgium": {
                    $continent = 3;
                    break;
                }
            case "Bosnia and Herzegovina": {
                    $continent = 3;
                    break;
                }
            case "Bulgaria": {
                    $continent = 3;
                    break;
                }
            case "Croatia": {
                    $continent = 3;
                    break;
                }
            case "Cyprus": {
                    $continent = 3;
                    break;
                }
            case "Czech Republic": {
                    $continent = 3;
                    break;
                }
            case "Denmark": {
                    $continent = 3;
                    break;
                }
            case "Estonia": {
                    $continent = 3;
                    break;
                }
            case "Finland": {
                    $continent = 3;
                    break;
                }
            case "France": {
                    $continent = 3;
                    break;
                }
            case "Georgia": {
                    $continent = 3;
                    break;
                }
            case "Germany": {
                    $continent = 3;
                    break;
                }
            case "Greece": {
                    $continent = 3;
                    break;
                }
            case "Hungary": {
                    $continent = 3;
                    break;
                }
            case "Iceland": {
                    $continent = 3;
                    break;
                }
            case "Ireland": {
                    $continent = 3;
                    break;
                }
            case "Italy": {
                    $continent = 3;
                    break;
                }
            case "Latvia": {
                    $continent = 3;
                    break;
                }
            case "Liechtenstein": {
                    $continent = 3;
                    break;
                }
            case "Lithuania": {
                    $continent = 3;
                    break;
                }
            case "Luxembourg": {
                    $continent = 3;
                    break;
                }
            case "Macedonia": {
                    $continent = 3;
                    break;
                }
            case "Malta": {
                    $continent = 3;
                    break;
                }
            case "Moldova, Republic of": {
                    $continent = 3;
                    break;
                }
            case "Monaco": {
                    $continent = 3;
                    break;
                }
            case "Montenegro": {
                    $continent = 3;
                    break;
                }
            case "Netherlands": {
                    $continent = 3;
                    break;
                }
            case "Norway": {
                    $continent = 3;
                    break;
                }
            case "Poland": {
                    $continent = 3;
                    break;
                }
            case "Portugal": {
                    $continent = 3;
                    break;
                }
            case "Romania": {
                    $continent = 3;
                    break;
                }
            case "San Marino": {
                    $continent = 3;
                    break;
                }
            case "Serbia": {
                    $continent = 3;
                    break;
                }
            case "Slovakia": {
                    $continent = 3;
                    break;
                }
            case "Slovenia": {
                    $continent = 3;
                    break;
                }
            case "Spain": {
                    $continent = 3;
                    break;
                }
            case "Sweden": {
                    $continent = 3;
                    break;
                }
            case "Switzerland": {
                    $continent = 3;
                    break;
                }
            case "Ukraine": {
                    $continent = 3;
                    break;
                }
            case "United Kingdom": {
                    $continent = 3;
                    break;
                }
            case "Antigua and Barbuda": {
                    $continent = 4;
                    break;
                }
            case "Bahamas": {
                    $continent = 4;
                    break;
                }
            case "Barbados": {
                    $continent = 4;
                    break;
                }
            case "Belize": {
                    $continent = 4;
                    break;
                }
            case "Canada": {
                    $continent = 4;
                    break;
                }
            case "Costa Rica": {
                    $continent = 4;
                    break;
                }
            case "Cuba": {
                    $continent = 4;
                    break;
                }
            case "Dominica": {
                    $continent = 4;
                    break;
                }
            case "Dominican Republic": {
                    $continent = 4;
                    break;
                }
            case "El Salvador": {
                    $continent = 4;
                    break;
                }
            case "Grenada": {
                    $continent = 4;
                    break;
                }
            case "Guatemala": {
                    $continent = 4;
                    break;
                }
            case "Haiti": {
                    $continent = 4;
                    break;
                }
            case "Honduras": {
                    $continent = 4;
                    break;
                }
            case "Jamaica": {
                    $continent = 4;
                    break;
                }
            case "Mexico": {
                    $continent = 4;
                    break;
                }
            case "Nicaragua": {
                    $continent = 4;
                    break;
                }
            case "Panama": {
                    $continent = 4;
                    break;
                }
            case "Saint Kitts and Nevis": {
                    $continent = 4;
                    break;
                }
            case "Saint Lucia": {
                    $continent = 4;
                    break;
                }
            case "Saint Vincent and the Grenadines": {
                    $continent = 4;
                    break;
                }
            case "Trinidad and Tobago": {
                    $continent = 4;
                    break;
                }
            case "United States": {
                    $continent = 4;
                    break;
                }
            case "Australia": {
                    $continent = 5;
                    break;
                }
            case "Fiji": {
                    $continent = 5;
                    break;
                }
            case "Kiribati": {
                    $continent = 5;
                    break;
                }
            case "Marshall Islands": {
                    $continent = 5;
                    break;
                }
            case "Micronesia, Federated States of": {
                    $continent = 5;
                    break;
                }
            case "Nauru": {
                    $continent = 5;
                    break;
                }
            case "New Zealand": {
                    $continent = 5;
                    break;
                }
            case "Palau": {
                    $continent = 5;
                    break;
                }
            case "Papua New Guinea": {
                    $continent = 5;
                    break;
                }
            case "Samoa": {
                    $continent = 5;
                    break;
                }
            case "Solomon Islands": {
                    $continent = 5;
                    break;
                }
            case "Tonga": {
                    $continent = 5;
                    break;
                }
            case "Tuvalu": {
                    $continent = 5;
                    break;
                }
            case "Vanuatu": {
                    $continent = 5;
                    break;
                }
            case "Argentina": {
                    $continent = 6;
                    break;
                }
            case "Bolivia": {
                    $continent = 6;
                    break;
                }
            case "Brazil": {
                    $continent = 6;
                    break;
                }
            case "Chile": {
                    $continent = 6;
                    break;
                }
            case "Colombia": {
                    $continent = 6;
                    break;
                }
            case "Ecuador": {
                    $continent = 6;
                    break;
                }
            case "Guyana": {
                    $continent = 6;
                    break;
                }
            case "Paraguay": {
                    $continent = 6;
                    break;
                }
            case "Peru": {
                    $continent = 6;
                    break;
                }
            case "Suriname": {
                    $continent = 6;
                    break;
                }
            case "Uruguay": {
                    $continent = 6;
                    break;
                }
            case "Venezuela": {
                    $continent = 6;
                    break;
                }

            default: {
                    $continent = 7;
                }
        }

        return $continent;
    }

    /**
     * get countries in an array
     * @return array $results
     */
    public function getCountries() {
        //get read connection
        $read = $this->_resource->getConnection('core_read');
        $cl = $this->_resource->getTableName('geoip_cl');
        $query = "SELECT * FROM {$cl} ORDER BY cn";
        $results = $read->fetchAll($query);

        return $results;
    }

    /**
     * get countries list by main_table.id
     * @param int $groupId
     * @return array
     */
    public function getGroupedCountries($groupId) {
        //get read connection
        $read = $this->_resource->getConnection('core_read');

        $table = $this->_resource->getTableName('geoipdefaultstore');

        $query = "SELECT countries_list"
                . " FROM {$table} "
                . " WHERE id = '{$groupId}'";

        $results = $read->fetchRow($query);

        return unserialize($results['countries_list']);
    }

    public function getInfoByIp($remoteIp) {
        $result = array();

        if (filter_var($remoteIp, FILTER_VALIDATE_IP)) {
            $read = $this->_resource->getConnection('core_read');
            $select = $read->select()
                    ->from(array('gcsv' => $this->_resource->getTableName('geoip_csv')))
                    ->where('gcsv.end >= INET_ATON(?)', $remoteIp) // reference: http://andy.wordpress.com/2007/12/16/fast-mysql-range-queries-on-maxmind-geoip-tables/
                    ->order('gcsv.end ASC')
                    ->limit(1);

            return $read->fetchRow($select);
        }

        return $result;
    }

    public function isWebCrawler() {

        $exceptionUrlList = ['/api/', '/paypal/'];
        $remoteAddress = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\Http\PhpEnvironment\RemoteAddress');
        //robot ip address is assigned to $ipaddress
        $ipaddress = $remoteAddress->getRemoteAddress();

        //hostname is assigned to $hostname
        $hostname = $remoteAddress->getRemoteHost();

        if (preg_match("/Googlebot/", $_SERVER['HTTP_USER_AGENT'])) {

            if (preg_match("/googlebot.com/", $hostname)) {
                // returns true if googlebot.com is found in hostname
                return true;
            }
        }
        
        foreach ($exceptionUrlList as $url) {

            if (preg_match($url, $hostname)) {

                return true;
            }
        }

        return false;
    }

    public function isIpAnException($ip, $exceptionIps) {
        $flag = false;
        $exceptionIpsArr = preg_split('@,@', $exceptionIps, NULL, PREG_SPLIT_NO_EMPTY);
        //$validatedIps = $this->validateIps($exceptionIpsArr);
        /* if current ip is not an exception, proceed */
        if (in_array($ip, $exceptionIpsArr)) {
            $flag = true;
        }

        return $flag;
    }

    public function validateIps($ips) {
        $validatedIps = [];
        foreach ($ips as $ip) {
            if (filter_var($ip, FILTER_VALIDATE_IP)) {
                $validatedIps[] = $ip;
            }
        }
        return $validatedIps;
    }

    public function getMediaType($type = 'url') {
        $media = $this->_urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]);
        if ($type == 'path') {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $dir = $objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');

            $media = $dir->getPath($dir::MEDIA);
        }

        return $media;
    }

    /**
     * Retrieve url of a view file
     *
     * @param string $fileId
     * @param array $params
     * @return string
     */
    public function getViewFileUrl($fileId, array $params = []) {
        try {
            $params = array_merge(['_secure' => $this->_getRequest()->isSecure()], $params);
            return $this->_gdsAssetRepo->getUrlWithParams($fileId, $params);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->_logger->critical($e);
            return $this->_getNotFoundUrl();
        }
    }

    /**
     * Get 404 file not found url
     *
     * @param string $route
     * @param array $params
     * @return string
     */
    protected function _getNotFoundUrl($route = '', $params = ['_direct' => 'core/index/notFound']) {
        return $this->_getUrl($route, $params);
    }

//    public function prepareCsv($fileName = '')
//    {
//
//        $fileId = 'FME_Geoipdefaultstore::' . $fileName . '.csv';
//        //$media = $this->_urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]); 
//
//        $media = $this->getMediaType('path');
//
//        $csvPath = $this->getViewFileUrl($fileId);//$media . $file;
//        //ini_set('max_input_vars', '10000');
//
//        return $csvPath;
//    }

    public function prepareCsv($fileName = '') {
        $file = '/geoipdefaultstore/' . $fileName . '.csv';
        //$media = $this->_urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]); 
        $media = $this->getMediaType('path');
        $csvPath = $media . $file;

        return $csvPath;
    }

    public function getPathToFile() {
        return $this->_directoryList->getPath('app') . '/code/FME/Geoipdefaultstore/view/adminhtml/web/geoipdefaultstore/';
    }

//    public function prepareCsv($fileName = '')
//    {
//        $file = $fileName . '.csv';
//        //$media = $this->_urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]); 
//        $media = $this->getPathToFile();
//        $csvPath = $media . $file; //echo $csvPath;exit;
//
//        return $csvPath;
//    }

    public function prepareCsvParts($csvFilePath) {
        $fh = fopen($csvFilePath, 'r');
        //set_time_limit(72000);
        if ($fh) {
            $fileno = 0;
            $lineno = 0;
            $startofnewfile = true;
            $lastlineno = 0;
            $media = $this->getMediaType('path') . '/geoipdefaultstore/';

            //$media = $this->getPathToFile();

            while ($rowData = fgets($fh)) {
                //Create new file
                if ($startofnewfile) {
                    $startofnewfile = false;
                    $lastlineno = 0;
                    //Create a file with unique name
                    $file = $media . "GeoIPCountryWhois_" . $fileno . ".csv";

                    $fw = fopen($file, 'w');
                }
                //write csv Line to the taret file in append mode.
                $fwrite = fwrite($fw, $rowData);
                //Count line numbers
                $lineno++;
                //Reached the limit of file now prepare to start new file
                if ($lineno == 2000) {
                    $lastlineno = $lineno;
                    fclose($fw);
                    $lineno = 0;
                    $startofnewfile = true;
                    $fileno++;
                }
            }

            if ($lastlineno == 0) {
                fclose($fw);
            }

            return true;
        } else {

            throw new \Magento\Exception(__('File GeoIPCountryWhois.csv do not exists'));
        }

        return false;
    }

}
