/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
var config = {
    map: {
        "*": {
            popup: 'FME_Geoipdefaultstore/js/resposive_popup'
        }
    },
    paths: {
        "popup": 'js/resposive_popup',
    },
    shim: {
        "popup": {
            deps: ["jquery"]
        },
    },
};
