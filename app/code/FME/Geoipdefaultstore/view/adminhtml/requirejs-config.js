/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
var config = {
    map: {
        '*': {
            gdsaccordian: 'FME_Geoipdefaultstore/js/jquery.accordion.source'
            
        }
    },
    shim: {
        "gdsaccordian": {
            deps: ["jquery"]
        }
    }
};