<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace FME\Geoipdefaultstore\Block\Adminhtml\Geoipdefaultstore\Edit\Tab;

use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use FME\Geoipdefaultstore\Helper\Data;

class Countries extends \Magento\Backend\Block\Template implements TabInterface {

    protected $_helper;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \FME\Geoipdefaultstore\Helper\Data $helper
     * @param array $data
     */
    public function __construct(
    Context $context, Registry $registry, Data $helper, array $data = []
    ) {

        $this->_helper = $helper;
        $this->_coreRegistry = $registry;

        
        parent::__construct($context, $data);
        
    }
    
    public function _construct() {
        parent::_construct();
        //$this->setTemplate('geoipdefaultstore/edit/form/tab/countries.phtml');
        $this->setUseAjax(true);
    }
    public function getHelper() {
        return $this->_helper;
    }
    /**
     * Prepare form
     *
     * @return $this
     */
//    protected function _prepareForm() {
//        /** @var $model \Magento\Cms\Model\Page */
//        $model = $this->_coreRegistry->registry('geoipdefaultstore_data');
//
//        /*
//         * Checking if user have permissions to save information
//         */
//        if ($this->_isAllowedAction('FME_Geoipdefaultstore::save')) {
//            $isElementDisabled = false;
//        } else {
//            $isElementDisabled = true;
//        }
//
//        /** @var \Magento\Framework\Data\Form $form */
//        $form = $this->_formFactory->create();
//
//        $form->setHtmlIdPrefix('geoipdefaultstore_');
//
//        $fieldset = $form->addFieldset('content_fieldset', [
//            'legend' => __('Countries'), 
//            'class' => 'fieldset-wide'
//        ]);
//
//        $countriesField = $fieldset->addField(
//            'content', 'editor', [
//            'name' => 'countries',
//            'style' => 'height:36em;',
//            'required' => true,
//            'disabled' => $isElementDisabled,
//        ]);
//
//        // Setting custom renderer for content field to remove label column
//        $renderer = $this->getLayout()->createBlock(
//                        'Magento\Backend\Block\Widget\Form\Renderer\Fieldset\Element'
//                )->setTemplate(
//                'FME_Geoipdefaultstore::geoipdefaultstore/edit/form/renderer/countries.phtml'
//        );
//        $countriesField->setRenderer($renderer);
//
//        $form->setValues($model->getData());
//        $this->setForm($form);
//
//        return parent::_prepareForm();
//    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel() {
        return __('Countries');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle() {
        return __('Countries');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return bool
     */
    public function canShowTab() {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return bool
     */
    public function isHidden() {
        return false;
    }

    /**
     * Return URL link to Tab content
     *
     * @return string
     */
    public function getTabUrl() {
        return $this->_urlBuilder->getUrl('geoipdefaultstore/*/countries', ['_current' => true]);
    }

    /**
     * Tab should be loaded trough Ajax call
     *
     * @return bool
     */
    public function isAjaxLoaded() {
        return true;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId) {
        return $this->_authorization->isAllowed($resourceId);
    }

}
