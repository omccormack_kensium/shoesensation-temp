<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace FME\Geoipdefaultstore\Block\Adminhtml\Geoipdefaultstore\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Store\Model\System\Store;
use Magento\Customer\Model\GroupFactory;
use FME\Geoipdefaultstore\Model\System\Config\Status;

class Info extends Generic implements TabInterface {

    protected $_groupFactory;
    protected $_systemStore;

    /**
     * @var \Fme\News\Model\Config\Status
     */
    protected $_geoipDefaultStoreStatus;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Status $newsStatus
     * @param array $data
     */
    public function __construct(Context $context, 
            Registry $registry, 
            FormFactory $formFactory, 
            Status $geoipDefaultStoreStatus, 
            Store $systemStore, 
            GroupFactory $groupFactory, 
            array $data = []
    ) {
        $this->_geoipDefaultStoreStatus = $geoipDefaultStoreStatus;
        $this->_systemStore = $systemStore;
        $this->_groupFactory = $groupFactory;
        
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form fields
     *
     * @return \Magento\Backend\Block\Widget\Form
     */
    protected function _prepareForm() {
        /** @var $model \Fme\News\Model\News */
        $model = $this->_coreRegistry->registry('geoipdefaultstore_data');

        /*
         * Checking if user have permissions to save information
         */
        if ($this->_isAllowedAction('FME_Geoipdefaultstore::manage_geoipdefaultstore')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('geoipdefaultstore_');
        //$form->setFieldNameSuffix('news');

        $fieldset = $form->addFieldset('base_fieldset', [
            'legend' => __('Geo-IP Default Store Information')
        ]);

        if ($model->getId()) {

            $fieldset->addField('id', 'hidden', [
                'name' => 'id'
            ]);
        }

        $fieldset->addField('title', 'text', [
            'name' => 'title',
            'label' => __('Title'),
            'required' => true
        ]);
        
        $fieldset->addField('priority', 'text', [
            'name' => 'priority',
            'label' => __('Priority'),
            'required' => true,
            'style' => 'width:5em',
            'class' => 'required-entry validate-not-negative-number'
        ]);
        /**
         * Check is single store mode
         */
        if (!$this->_storeManager->isSingleStoreMode()) {

            $field = $fieldset->addField('store_id', 'select', [
                'name' => 'stores[]',
                'label' => __('Store Group'),
                'title' => __('Store Group'),
                'required' => true,
                'values' => $this->_systemStore->getStoreGroupOptionHash(),
                'disabled' => $isElementDisabled
            ]);
            $renderer = $this->getLayout()->createBlock(
                    'Magento\Backend\Block\Store\Switcher\Form\Renderer\Fieldset\Element'
            );
            $field->setRenderer($renderer);
        } else {

            $fieldset->addField('store_id', 'hidden', [
                'name' => 'stores[]',
                'value' => $this->_storeManager->getStore(true)->getId()
            ]);
            $model->setStoreId($this->_storeManager->getStore(true)->getId());
        }

        $fieldset->addField('is_active', 'select', [
            'name' => 'is_active',
            'label' => __('Status'),
            'options' => $this->_geoipDefaultStoreStatus->toOptionArray(),
            'disabled' => $isElementDisabled
        ]);
        
        $fieldset->addField('exception_ips', 'textarea', [
            'name' => 'exception_ips',
            'label' => __('Exceptions'),

            'disabled' => $isElementDisabled,
            'note' => __('State IPs (comma separated only) here for exception')
        ]);
        
        $fieldset->addField('description', 'textarea', [
            'name' => 'description',
            'label' => __('Description'),
            'disabled' => $isElementDisabled
        ]);

        if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }

        $data = $model->getData();
        $form->setValues($data);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel() {
        return __('Information');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle() {
        return __('Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab() {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden() {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId) {
        return $this->_authorization->isAllowed($resourceId);
    }

    protected function _getCustomerGroupIds() {
        $options = $this->_groupFactory->create()
                ->getCollection()
                ->toOptionArray();
        return $options;
    }

    /**
     * Retrieve additional element types
     *
     * @return array
     */
//    protected function _getAdditionalElementTypes() { echo 'here';exit;
//        $result = [
//            'gallery' => 'Magento\Catalog\Block\Adminhtml\Product\Helper\Form\Gallery',
//            'image' => 'Magento\Catalog\Block\Adminhtml\Product\Helper\Form\Image',
//        ];
//
//        $response = new \Magento\Framework\DataObject();
//        $response->setTypes([]);
//        //$this->_eventManager->dispatch('adminhtml_catalog_product_edit_element_types', ['response' => $response]);
//
//        foreach ($response->getTypes() as $typeName => $typeClass) {
//
//            $result[$typeName] = $typeClass;
//        }
//
//        return $result;
//    }

}
