<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace FME\Geoipdefaultstore\Block\Adminhtml\Geoipdefaultstore\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs {

    /**
     * @return void
     */
    protected function _construct() { 
        parent::_construct();
        $this->setId('geoipdefaultstore_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Geo-IP Default Store Information'));
    }

    protected function _beforeToHtml() {
        
        $this->addTab('country_section', [
            'label' => 'Countries',
            //'content' => $this->getLayout()->createBlock('FME\Geoipdefaultstore\Block\Adminhtml\Geoipdefaultstore\Edit\Tab\Countries', 'country_section', ['template' => 'FME_Geoipdefaultstore::edit/form/tab/countries.phtml'])->toHtml(),
            'url' => $this->_urlBuilder->getUrl('*/*/countries', ['_current' => true]),
            'class' => 'ajax',
        ]);
        
        parent::_beforeToHtml();
    }
}
