<?php

namespace FME\Geoipdefaultstore\Block\Adminhtml\Geoipdefaultstore;

//use Magento\Backend\Block\Widget\Grid as WidgetGrid;
// 
//class Grid extends WidgetGrid {
//   
//    
//}
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Adminhtml geoipdefaultstore grid
 */
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended {

    /**
     * @var \FME\Geoipdefaultstore\Model\ResourceModel\Geoipdefaultstore\CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @var \FME\Geoipdefaultstore\Model\Geoipdefaultstore
     */
    protected $_geoipDefaultStore;

    /**
     * @var \Magento\Framework\View\Model\PageLayout\Config\BuilderInterface
     */
    protected $pageLayoutBuilder;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \FME\Geoipdefaultstore\Model\Geoipdefaultstore $geoipDefaultStore
     * @param \FME\Geoipdefaultstore\Model\ResourceModel\Geoipdefaultstore\CollectionFactory $collectionFactory
     * @param \Magento\Framework\View\Model\PageLayout\Config\BuilderInterface $pageLayoutBuilder
     * @param array $data
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, \Magento\Backend\Helper\Data $backendHelper, \FME\Geoipdefaultstore\Model\Geoipdefaultstore $geoipDefaultStore, \FME\Geoipdefaultstore\Model\ResourceModel\Geoipdefaultstore\CollectionFactory $collectionFactory, \Magento\Framework\View\Model\PageLayout\Config\BuilderInterface $pageLayoutBuilder, array $data = []
    ) {
        $this->_collectionFactory = $collectionFactory;
        $this->_geoipDefaultStore = $geoipDefaultStore;
        $this->pageLayoutBuilder = $pageLayoutBuilder;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct() {
        parent::_construct();
        $this->setId('geoipdefaultstoreGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
    }

    /**
     * Prepare collection
     *
     * @return \Magento\Backend\Block\Widget\Grid
     */
    protected function _prepareCollection() {
        $collection = $this->_collectionFactory->create();
        /* @var $collection \Magento\Cms\Model\ResourceModel\Page\Collection */
        $collection->setFirstStoreFlag(true);
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Prepare columns
     *
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     */
    protected function _prepareColumns() {
        $this->addColumn('title', ['header' => __('Title'), 'index' => 'title']);

        /**
         * Check is single store mode
         */
        if (!$this->_storeManager->isSingleStoreMode()) {
            $this->addColumn(
                    'store_id', [
                'header' => __('Store View'),
                'index' => 'store_id',
                'type' => 'store',
                'store_all' => true,
                'store_view' => true,
                'sortable' => false,
                'filter_condition_callback' => [$this, '_filterStoreCondition']
                    ]
            );
        }

        $this->addColumn(
                'is_active', [
            'header' => __('Status'),
            'index' => 'is_active',
            'type' => 'options',
            'options' => $this->_geoipDefaultStore->getAvailableStatuses()
                ]
        );

        $this->addColumn(
                'creation_time', [
            'header' => __('Created'),
            'index' => 'creation_time',
            'type' => 'datetime',
            'header_css_class' => 'col-date',
            'column_css_class' => 'col-date'
                ]
        );

        $this->addColumn(
                'update_time', [
            'header' => __('Modified'),
            'index' => 'update_time',
            'type' => 'datetime',
            'header_css_class' => 'col-date',
            'column_css_class' => 'col-date'
                ]
        );

        $this->addColumn(
                'page_actions', [
            'header' => __('Action'),
            'sortable' => false,
            'filter' => false,
            'renderer' => 'FME\Geoipdefaultstore\Block\Adminhtml\Geoipdefaultstore\Grid\Renderer\Action',
            'header_css_class' => 'col-action',
            'column_css_class' => 'col-action'
                ]
        );

        return parent::_prepareColumns();
    }

    /**
     * After load collection
     *
     * @return void
     */
    protected function _afterLoadCollection() {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

    /**
     * Filter store condition
     *
     * @param \Magento\Framework\Data\Collection $collection
     * @param \Magento\Framework\DataObject $column
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function _filterStoreCondition($collection, \Magento\Framework\DataObject $column) {
        if (!($value = $column->getFilter()->getValue())) {
            return;
        }

        $this->getCollection()->addStoreFilter($value);
    }

    /**
     * Row click url
     *
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', ['id' => $row->getId()]);
    }

}
