<?php

namespace FME\Geoipdefaultstore\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

class Geoipdefaultstore extends Container {

    protected function _construct() {

        $this->_controller = 'adminhtml_geoipDefaultStore';
        $this->_headerText = __('Geo-IP Default Store');
        $this->_blockGroup = 'FME_Geoipdefaultstore';
        $this->_addButtonLabel = __('Add Rule');

        parent::_construct();
    }

}
