<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace FME\Geoipdefaultstore\Block;
use \Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use FME\Geoipdefaultstore\Model\CookieModel;
use FME\Geoipdefaultstore\Helper\Data;

class Geoipdefaultstore extends Template {

    /**
     * Geoipdefaultstore model factory
     *
     * @var \FME\Geoipdefaultstore\Model\GeoipdefaultstoreFactory
     */
    protected $_geoipdefaultstoreFactory;
    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    public $storeManager;
    /**
     * URL builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    public $urlBuilder;
    /**
     *
     * @var FME\Geoipdefaultstore\Model\CookieModel
     */
    public $cookieModel;
    
    /**
     *
     * @var FME\Geoipdefaultstore\Helper\Data
     */
    public $geoipdefaultstoreHelper;
    /**
     * 
     * @param Context $context
     * @param array $data
     */
    public function __construct(Context $context, CookieModel $cookieModel, Data $helper, $data = [])
    {
        parent::__construct($context, $data);
        $this->storeManager = $context->getStoreManager();
        $this->urlBuilder = $context->getUrlBuilder();
        $this->cookieModel = $cookieModel;
        $this->geoipdefaultstoreHelper = $helper;
    }
    
    public function _prepareLayout() {
        return parent::_prepareLayout();
    }


    public function getStoreGroups() {
        return $this->storeManager->getGroups();
    }
    
    public function isCookieExists() {
        return $this->cookieModel->get(CookieModel::COOKIE_NAME);
    }
    
    public function getAssignedStoreViewCode() {
        return $this->cookieModel->getCode();
    }
}
