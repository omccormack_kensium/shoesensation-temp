<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace FME\Geoipdefaultstore\Controller;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Result\PageFactory;
use FME\Geoipdefaultstore\Model\GeoipdefaultstoreFactory;
use FME\Geoipdefaultstore\Helper\Data;

abstract class Geoipdefaultstore extends Action {

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_pageFactory;

    /**
     * @var \FME\Geoipdefaultstore\Helper\Data
     */
    protected $_dataHelper;

    /**
     * @var \FME\Geoipdefaultstore\Model\GeoipdefaultstoreFactory
     */
    protected $_geoipDefaultStoreFactory;

    /**
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param Data $dataHelper
     * @param GeoipdefaultstoreFactory $geoipdefaultstoreFactory
     */
    public function __construct(
            Context $context, 
            PageFactory $pageFactory, 
            Data $dataHelper, 
            GeoipdefaultstoreFactory $geoipdefaultstoreFactory
    ) {
        parent::__construct($context);
        $this->_pageFactory = $pageFactory;
        $this->_dataHelper = $dataHelper;
        $this->_geoipDefaultStoreFactory = $geoipDefaultStoreFactory;
    }

    /**
     * Dispatch request
     *
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(RequestInterface $request) { 
        // Check this module is enabled in frontend
        if ($this->_dataHelper->isEnabledInFrontend()) { 
            $result = parent::dispatch($request); 
            return $result;
        } else {
            $this->_forward('noroute');
        }
    }
}
