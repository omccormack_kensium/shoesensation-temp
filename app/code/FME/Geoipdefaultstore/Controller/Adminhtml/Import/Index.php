<?php
 
namespace FME\Geoipdefaultstore\Controller\Adminhtml\Import;
 
use FME\Geoipdefaultstore\Controller\Adminhtml\Geoipdefaultstore;
 
class Index extends Geoipdefaultstore {
    /**
     * @return void
     */
   public function execute() {
	   
      if ($this->getRequest()->getQuery('ajax')) {
            $this->_forward('grid');
            return;
        }
        
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('FME_Geoipdefaultstore::main_menu');
        $resultPage->getConfig()
                ->getTitle()
                ->prepend(__('Import Countries List'));
 
        return $resultPage;
   }
   
   /**
     * News access rights checking
     *
     * @return bool
     */
    protected function _isAllowed() {
		
        return $this->_authorization->isAllowed('FME_Geoipdefaultstore::geoipdefaultstore_importcountrieslist');
    }
}
