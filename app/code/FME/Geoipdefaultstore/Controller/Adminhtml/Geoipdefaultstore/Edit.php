<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace FME\Geoipdefaultstore\Controller\Adminhtml\Geoipdefaultstore;

use FME\Geoipdefaultstore\Controller\Adminhtml\Geoipdefaultstore;

class Edit extends Geoipdefaultstore {

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('FME_Geoipdefaultstore::manage_geoipdefaultstore');
    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction() {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('FME_Geoipdefaultstore::geoipdefaultstore_geoipdefaultstore')
                ->addBreadcrumb(__('Geo-IP Default Store'), __('Geo-IP Default Store'))
                ->addBreadcrumb(__('Manage Geo-IP Default Store'), __('Manage Geo-IP Default Store'));
        return $resultPage;
    }

    /**
     * @return void
     */
    public function execute() {

        $geoipdefaultstoreId = $this->getRequest()->getParam('id');
        /** @var \FME\Geoipdefaultstore\Model\Geoipdefaultstore $model */
        $model = $this->_geoipDefaultStoreFactory->create();

        if ($geoipdefaultstoreId) {
            $model->load($geoipdefaultstoreId);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This Geo-IP Default Store rule no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }

        // Restore previously entered form data from session
        //$data = $this->_session->getNewsData(true);
        $data = $this->_objectManager->get('Magento\Backend\Model\Session')
                ->getFormData(true); //echo '<pre>';var_dump($data);echo '</pre>';exit;
        if (!empty($data)) {
            $model->setData($data);
        }
        $this->_coreRegistry->register('geoipdefaultstore_data', $model);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('FME_Geoipdefaultstore::main_menu');
        $resultPage->getConfig()
                ->getTitle()
                ->prepend(__('Geo-IP Default Store'));

        return $resultPage;
    }

}
