<?php

namespace FME\Geoipdefaultstore\Controller\Adminhtml\Geoipdefaultstore;

use FME\Geoipdefaultstore\Controller\Adminhtml\Geoipdefaultstore;

class Save extends Geoipdefaultstore
{

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('FME_Geoipdefaultstore::manage_geoipdefaultstore');
    }

    /**
     * Filter category data
     *
     * @param array $rawData
     * @return array
     */
    protected function _filterGeoipdefaultstorePostData(array $rawData)
    {
        $data = $rawData;
        // @todo It is a workaround to prevent saving this data in category model and it has to be refactored in future
        if (isset($data['image']) && is_array($data['image']))
        {
            $data['image_additional_data'] = $data['image'];
            unset($data['image']);
        }
        return $data;
    }

    public function execute()
    {

        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($data)
        {
            $model = $this->_objectManager->create('FME\Geoipdefaultstore\Model\Geoipdefaultstore');

            $id = $this->getRequest()->getParam('id');

            if ($id)
            {
                $model->load($id);
            }

            $data['store_group_id'] = $data['stores'][0];
            unset($data['stores']);
            if (isset($data['countries_list']) && count($data['countries_list']) > 0)
            {
                $data['countries_list'] = serialize($data['countries_list']);
            }

            if (isset($data['exception_ips']))
            {
                $exceptionIps = preg_split('@,@', $data['exception_ips'], NULL, PREG_SPLIT_NO_EMPTY);
                foreach ($exceptionIps as $ip)
                {
                    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE)
                    {
                        $this->messageManager->addError(__('Ip has wrong format.'));
                        $this->_getSession()->setFormData($data);
                        return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
                    }
                }
            }

            $model->setData($data);

            try
            {
                if ($model->getCreatedAt() == NULL || $model->getUpdatedAt() == NULL)
                {
                    $model->setCreatedAt(date('y-m-d h:i:s'))
                            ->setUpdatedAt(date('y-m-d h:i:s'));
                } 
                else 
                {
                    $model->setUpdatedAt(date('y-m-d h:i:s'));
                }
                $model->save();
                $this->messageManager->addSuccess(__('You saved this Geo-IP Default Store rule.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back'))
                {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } 
            catch (\Magento\Framework\Exception\LocalizedException $e)
            {
                $this->messageManager->addError($e->getMessage());
            } 
            catch (\RuntimeException $e)
            {
                $this->messageManager->addError($e->getMessage());
            } 
            catch (\Exception $e)
            {
                $this->messageManager->addException($e, __('Something went wrong while saving the Geo-IP Default Store  rule.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}
