<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace FME\Geoipdefaultstore\Controller\Adminhtml\Geoipdefaultstore;

use FME\Geoipdefaultstore\Controller\Adminhtml\Geoipdefaultstore;

class NewAction extends Geoipdefaultstore {
    
    /**
     * Create Geoipdefaultstore action
     *
     * @return void
     */
    public function execute() {
        
        $this->_forward('edit');
    }
}