<?php
 
namespace FME\Geoipdefaultstore\Controller\Adminhtml\Geoipdefaultstore;
 
use FME\Geoipdefaultstore\Controller\Adminhtml\Geoipdefaultstore;
 
class Grid extends Geoipdefaultstore {
   /**
     * @return void
     */
   public function execute() { 
      return $this->_resultPageFactory->create();
   }
}
