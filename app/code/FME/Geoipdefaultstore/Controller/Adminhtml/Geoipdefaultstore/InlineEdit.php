<?php

/* ////////////////////////////////////////////////////////////////////////////////
  \\\\\\\\\\\\\\\\\\\\\\\\\  FME Productattachments Module  \\\\\\\\\\\\\\\\\\\\\\\\\
  /////////////////////////////////////////////////////////////////////////////////
  \\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  ///////                                                                   ///////
  \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
  ///////   that is bundled with this package in the file LICENSE.txt.      ///////
  \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
  ///////          http://opensource.org/licenses/osl-3.0.php               ///////
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  ///////                      * @category   FME                            ///////
  \\\\\\\                      * @package    FME_Productattachments              \\\\\\\
  ///////    * @author    FME Extensions <support@fmeextensions.com>   ///////
  \\\\\\\                                                                   \\\\\\\
  /////////////////////////////////////////////////////////////////////////////////
  \\* @copyright  Copyright 2015 © fmeextensions.com All right reserved\\\
  /////////////////////////////////////////////////////////////////////////////////
 */

namespace FME\Geoipdefaultstore\Controller\Adminhtml\Geoipdefaultstore;

use Magento\Backend\App\Action\Context;
use FME\Geoipdefaultstore\Model\Geoipdefaultstore;
use Magento\Framework\Controller\Result\JsonFactory;

class InlineEdit extends \Magento\Backend\App\Action {

    /** @var PostDataProcessor */
    protected $dataProcessor;

    /** @var Geoipdefaultstore  */
    protected $_geoipDefaultStore;

    /** @var JsonFactory  */
    protected $jsonFactory;

    /**
     * @param Context $context
     * @param PostDataProcessor $dataProcessor
     * @param Geoipdefaultstore $geoipDefaultStore
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
    Context $context, PostDataProcessor $dataProcessor, Geoipdefaultstore $geoipDefaultStore, JsonFactory $jsonFactory
    ) {
        parent::__construct($context);
        $this->dataProcessor = $dataProcessor;
        $this->_geoipDefaultStore = $geoipDefaultStore;
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                        'messages' => [__('Please correct the data sent.')],
                        'error' => true,
            ]);
        }

        foreach (array_keys($postItems) as $id) {
            /** @var \Magento\Geoipdefaultstore\Model\Geoipdefaultstore $geoipDefaultStore */
            $geoipDefaultStore = $this->_geoipDefaultStore->load($id);
            try {
                $geoipDefaultStoreData = $this->dataProcessor->filter($postItems[$id]);
                $geoipDefaultStore->setData($geoipDefaultStoreData);
                $geoipDefaultStore->save();
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $messages[] = $this->getErrorWithGeoipdefaultstoreId($geoipDefaultStore, $e->getMessage());
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithGeoipdefaultstoreId($geoipDefaultStore, $e->getMessage());
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithGeoipdefaultstoreId(
                        $geoipDefaultStore, __('Something went wrong while saving the productattachments.')
                );
                $error = true;
            }
        }

        return $resultJson->setData([
                    'messages' => $messages,
                    'error' => $error
        ]);
    }

    /**
     * Add productattachments title to error message
     *
     * @param ProductattachmentsInterface $productattachments
     * @param string $errorText
     * @return string
     */
    protected function getErrorWithGeoipdefaultstoreId(Geoipdefaultstore $geoipDefaultStore, $errorText) {
        return '[Geoipdefaultstore ID: ' . $geoipDefaultStore->getId() . '] ' . $errorText;
    }

    protected function _isAllowed() {
        return true;
    }

}
