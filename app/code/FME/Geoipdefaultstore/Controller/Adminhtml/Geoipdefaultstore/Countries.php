<?php

namespace FME\Geoipdefaultstore\Controller\Adminhtml\Geoipdefaultstore;

use FME\Geoipdefaultstore\Controller\Adminhtml\Geoipdefaultstore;

class Countries extends Geoipdefaultstore {

    /**
     * @return void
     */
    public function execute() {

        $resultLayout = $this->_resultLayoutFactory->create();

        return $resultLayout;
    }

}
