<?php
 
namespace FME\Geoipdefaultstore\Controller\Adminhtml\Geoipdefaultstore;
 
use FME\Geoipdefaultstore\Controller\Adminhtml\Geoipdefaultstore;
 
class Index extends Geoipdefaultstore {

   /**
     * Check the permission to run it
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('FME_Geoipdefaultstore::manage_geoipdefaultstore');
    } 
    /**
     * @return void
     */
   public function execute() {
	   
      if ($this->getRequest()->getQuery('ajax')) {
            $this->_forward('grid');
            return;
        }
        
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('FME_Geoipdefaultstore::main_menu');
        $resultPage->getConfig()
                ->getTitle()
                ->prepend(__('FME Geo-IP Default Store'));
 
        return $resultPage;
   }
}
