<?php

namespace FME\Geoipdefaultstore\Controller\Adminhtml;
 
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\View\Result\LayoutFactory;
use FME\Geoipdefaultstore\Model\GeoipdefaultstoreFactory;
use FME\Geoipdefaultstore\Helper\Data;


abstract class Geoipdefaultstore extends Action 
{
    protected $_geoipdefaultstoreHelper;
    /**
     * @var PostDataProcessor
     */
    protected $dataProcessor;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;
 
    /**
     * Result page factory
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;
    
    /**
     * Result layout factory
     *
     * @var \Magento\Framework\View\Result\LayoutFactory
     */
    protected $_resultLayoutFactory;
 
    /**
     * News model factory
     *
     * @var \FME\Geoipdefaultstore\Model\GeoipdefaultstoreFactory
     */
    protected $_geoipDefaultStoreFactory;
 
    protected $_resource;
    
    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PageFactory $resultPageFactory
     * @param GeoipdefaultstoreFactory $geoipDefaultStoreFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        GeoipdefaultstoreFactory $geoipDefaultStoreFactory,
        Data $helper,
        \Magento\Framework\App\ResourceConnection $resource,
        LayoutFactory $layoutFactory
    ) {
       parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_geoipDefaultStoreFactory = $geoipDefaultStoreFactory;
        $this->_geoipdefaultstoreHelper = $helper;
        $this->_resource = $resource;
        $this->_resultLayoutFactory = $layoutFactory;
    }
 
    /**
     * News access rights checking
     *
     * @return bool
     */
    protected function _isAllowed() {
		
        return $this->_authorization->isAllowed('FME_Geoipdefaultstore::manage_geoipdefaultstore');
    }
}
