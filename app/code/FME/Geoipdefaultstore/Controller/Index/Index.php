<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace FME\Geoipdefaultstore\Controller\Index;

use \Magento\Framework\App\Action\Context;
//use Magento\Framework\Json\Helper\Data as JsonData;
use \Magento\Framework\Controller\Result\RawFactory;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Store\Api\StoreCookieManagerInterface;
use \Magento\Store\Model\StoreRepository;
use Magento\Store\Model\StoreIsInactiveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Store\Model\Store;
use FME\Geoipdefaultstore\Model\CookieModel;
/**
 * Responsible for loading page content.
 *
 * This is a basic controller that only loads the corresponding layout file. It may duplicate other such
 * controllers, and thus it is considered tech debt. This code duplication will be resolved in future releases.
 */
class Index extends \Magento\Framework\App\Action\Action
{
    protected $_storeManager;
    protected $_jsonHelper;

    /**
     *
     * @var FME\Geoipdefaultstore\Model\CookieModel
     */
    protected $_cookieModel;
    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $_resultRawFactory;

    /**
     * @var \Magento\Store\Api\StoreCookieManagerInterface
     */
    protected $_storeCookieManager;

    /**
     * @var StoreRepositoryInterface
     */
    protected $_storeRepository;
    /**
     * @var HttpContext
     */
    protected $_httpContext;
    
    public function __construct(Context $context, 
            RawFactory $resultRawFactory, 
            StoreManagerInterface $storeManager, 
            StoreCookieManagerInterface $storeCookie, 
            StoreRepository $storeRepository,
            HttpContext $httpContext,
            CookieModel $cookieModel) {
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        //$this->_jsonHelper = $jsonHelper;
        $this->_resultRawFactory = $resultRawFactory;
        $this->_storeCookieManager = $storeCookie;
        $this->_storeRepository = $storeRepository;
        $this->_httpContext = $httpContext;
        $this->_cookieModel = $cookieModel;
    }

    public function execute() {
        $data = $this->getRequest()->getParams();
        $storeCode = $data['defaultStore'];
        // cookie not accessible in same request
        // so this request will get cookie value
        if ($storeCode == '') {
            $storeCode = $this->_cookieModel->getCode();
        }
        
        try {
            $store = $this->_storeRepository->get($storeCode);
        } catch (StoreIsInactiveException $e) {
            $error = __('Requested store is inactive');
        } catch (NoSuchEntityException $e) {
            $error = __('Requested store is not found');
        }
        
        if (isset($error)) {
            $this->messageManager->addError($error);
            //$this->getResponse()->setRedirect($this->_redirect->getRedirectUrl());
            /** @var \Magento\Framework\Controller\Result\Raw $response */
            $response = $this->_resultRawFactory->create();
            $response->setHeader('Content-type', 'text/plain');
            $response->setContents(json_encode(['error' => $error]));
            return $response;
        }
        
        $baseUrl = $store->getBaseUrl();
        $this->_httpContext->setValue(Store::ENTITY, $store->getCode(), null);
        $this->_storeCookieManager->setStoreCookie($store);
        $this->_cookieModel->set(1);
        /** @var \Magento\Framework\Controller\Result\Raw $response */
        $response = $this->_resultRawFactory->create();
        $response->setHeader('Content-type', 'text/plain');
        $response->setContents(json_encode(['redirect' => $baseUrl]));
        return $response;
    }

}
