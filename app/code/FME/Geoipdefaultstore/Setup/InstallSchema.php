<?php

/*
 * This file will declare and create your custom table
 */

namespace FME\Geoipdefaultstore\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface {

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {

        $installer = $setup;
        $installer->startSetup();

        // Get fme_news table
        $tableName = $installer->getTable('geoipdefaultstore');
        // Check if the table already exists
        if ($installer->getConnection()->isTableExists($tableName) != true) {
            // Create fme_news table
            $table = $installer->getConnection()
                    ->newTable($tableName)
                    ->addColumn('id', Table::TYPE_INTEGER, null, [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                            ], 'ID'
                    )
                    ->addColumn('title', Table::TYPE_TEXT, null, [
                        'nullable' => false, 'default' => ''
                            ], 'Title'
                    )
                    ->addColumn('priority', Table::TYPE_SMALLINT, null, [
                        'nullable' => false, 
                        'default' => 0,
                    ], 'priority')
                    ->addColumn('store_group_id', Table::TYPE_INTEGER, null, [
                        'nullable' => false, 'default' => 0
                            ], 'store group id'
                    )
                    ->addColumn('exception_ips', Table::TYPE_TEXT, null, [
                        'nullable' => false, 'default' => ''
                            ], 'IPs, given exception'
                    )
                    ->addColumn('exception_cms_pages', Table::TYPE_TEXT, null, [
                        'nullable' => false, 'default' => ''
                            ], 'cms pages, given exception'
                    )
                    ->addColumn('description', Table::TYPE_TEXT, null, [
                        'nullable' => false, 'default' => ''
                            ], 'Description'
                    )
                    ->addColumn('countries_list', Table::TYPE_TEXT, null, [
                        'nullable' => false, 
                        'default' => ''
                        ], 'Countries list')
                   ->addColumn(
                        'created_at',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        null,
                        ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                        'Item Creation Time'
                    )->addColumn(
                        'updated_at',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        null,
                        ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                        'Item Modification Time'
                    )
                    ->addColumn('is_active', Table::TYPE_SMALLINT, null, [
                        'nullable' => false, 'default' => '0'
                            ], 'Status'
                    )
                    ->setComment('main Table')
                    ->setOption('type', 'InnoDB')
                    ->setOption('charset', 'utf8');
            $installer->getConnection()->createTable($table);
        }
        
        $geoipCl = $installer->getTable('geoip_cl');
        
        if (!$installer->getConnection()->isTableExists($geoipCl)) {
            
            $tableGeoipCl = $installer->getConnection()
                    ->newTable($geoipCl)
                    ->addColumn('ci', Table::TYPE_INTEGER, null, [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                            ], 'ID'
                    )
                    ->addColumn('cc', Table::TYPE_TEXT, null, [
                        'nullable' => false, 
                    ], 'country code')
                    ->addColumn('cn', Table::TYPE_TEXT, null, [
                        'nullable' => false,
                    ], 'country name');
            $installer->getConnection()->createTable($tableGeoipCl);
        }
        
        $geoipIp = $installer->getTable('geoip_ip');
        
        if (!$installer->getConnection()->isTableExists($geoipIp)) {
            
            $tableGeoipIp = $installer->getConnection()
                    ->newTable($geoipIp)
                    ->addColumn('start', Table::TYPE_INTEGER, null, [
                        'nullable' => false, 
                    ], 'start')
                    ->addColumn('end', Table::TYPE_INTEGER, null, [
                        'nullable' => false,
                    ], 'end')
                    ->addColumn('ci', Table::TYPE_INTEGER, null, [
                        'nullable' => false, 
                    ], 'ci ');
            $installer->getConnection()->createTable($tableGeoipIp);
        }

        $geoipCsv = $installer->getTable('geoip_csv');
        
        if (!$installer->getConnection()->isTableExists($geoipCsv)) {
            
            $tabelGeoipCsv = $installer->getConnection()
                    ->newTable($geoipCsv)
                    ->addColumn('start_ip', Table::TYPE_TEXT, null, [
                        'nullable' => false, 
                    ], 'start IP')
                    ->addColumn('end_ip', Table::TYPE_TEXT, null, [
                        'nullable' => false,
                    ], 'end IP')
                    ->addColumn('start', Table::TYPE_INTEGER, null, [
                        'nullable' => false, 
                    ], 'start')
                    ->addColumn('end', Table::TYPE_INTEGER, null, [
                        'nullable' => false,
                    ], 'end')
                    ->addColumn('cc', Table::TYPE_TEXT, null, [
                        'nullable' => false, 
                    ], 'country code ')
                    ->addColumn('cn', Table::TYPE_TEXT, null, [
                        'nullable' => false, 
                    ], 'country name ');
            
            $installer->getConnection()
                    ->createTable($tabelGeoipCsv);
        }
        
        $installer->endSetup();
    }

}
