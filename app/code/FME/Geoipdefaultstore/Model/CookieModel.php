<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace FME\Geoipdefaultstore\Model;

use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\Cookie\PublicCookieMetadata;
use Magento\Framework\Stdlib\CookieManagerInterface;

class CookieModel
{
    /**
     * Name of cookie that holds private content version
     */
    const COOKIE_NAME = 'geoipdefaultstore_assigned';

    const COOKIE_GEOIPDEFAULTSTORE_CODE = 'geoipdefaultstore_code';
    /**
     * CookieManager
     *
     * @var CookieManagerInterface
     */
    private $cookieManager;

    /**
     * @var CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var SessionManagerInterface
     */
    private $sessionManager;

    /**
     * @param CookieManagerInterface $cookieManager
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param SessionManagerInterface $sessionManager
     */
    public function __construct(
        CookieManagerInterface $cookieManager, 
        CookieMetadataFactory $cookieMetadataFactory, 
        SessionManagerInterface $sessionManager
    )
    {
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->sessionManager = $sessionManager;
    }

    /**
     * Get form key cookie
     *
     * @return string
     */
    public function get()
    {
        return $this->cookieManager->getCookie(self::COOKIE_NAME);
    }

    /**
     * @param string $value
     * @param int $duration
     * @return void
     */
    public function set($value, $duration = 86400)
    {
        $metadata = $this->cookieMetadataFactory
                ->createPublicCookieMetadata()
//                ->setHttpOnly(true)
//            ->setDurationOneYear()
                ->setDuration($duration)
                ->setPath($this->sessionManager->getCookiePath())
                ->setDomain($this->sessionManager->getCookieDomain());

        $this->cookieManager->setPublicCookie(
                self::COOKIE_NAME, $value, $metadata
        );
    }
    
    /**
     * @param string $value
     * @param int $duration
     * @return void
     */
    public function setCode($value, $duration = 86400)
    {
        $metadata = $this->cookieMetadataFactory
                ->createPublicCookieMetadata()
                ->setDuration($duration)
                ->setPath($this->sessionManager->getCookiePath())
                ->setDomain($this->sessionManager->getCookieDomain());

        $this->cookieManager->setPublicCookie(
                self::COOKIE_GEOIPDEFAULTSTORE_CODE, $value, $metadata
        );
    }

    /**
     * Get form key cookie
     *
     * @return string
     */
    public function getCode()
    {
        return $this->cookieManager->getCookie(self::COOKIE_GEOIPDEFAULTSTORE_CODE);
    }
    /**
     * @return void
     */
    public function delete()
    {
        $this->cookieManager->deleteCookie(
                self::COOKIE_NAME, $this->cookieMetadataFactory
                        ->createCookieMetadata()
                        ->setPath($this->sessionManager->getCookiePath())
                        ->setDomain($this->sessionManager->getCookieDomain())
        );
    }

}
