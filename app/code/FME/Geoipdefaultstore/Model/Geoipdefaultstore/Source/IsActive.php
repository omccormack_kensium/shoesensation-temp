<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace FME\Geoipdefaultstore\Model\Geoipdefaultstore\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class IsActive
 */
class IsActive implements OptionSourceInterface {

    /**
     * @var \FME\Geoipdefaultstore\Model\Geoipdefaultstore
     */
    protected $geoipDefaultStore;

    /**
     * Constructor
     *
     * @param \FME\Geoipdefaultstore\Model\Geoipdefaultstore $geoipDefaultStore
     */
    public function __construct(\FME\Geoipdefaultstore\Model\Geoipdefaultstore $geoipDefaultStore) {
        $this->geoipDefaultStore = $geoipDefaultStore;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray() {
        $availableOptions = $this->geoipDefaultStore->getAvailableStatuses();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }

}
