<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace FME\Geoipdefaultstore\Model\ResourceModel\Geoipdefaultstore;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection {
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    protected function _construct() {
        $this->_init(
                'FME\Geoipdefaultstore\Model\Geoipdefaultstore',
                'FME\Geoipdefaultstore\Model\ResourceModel\Geoipdefaultstore'
        );
    }
    
    public function addCountryFilter($country) {
        $this->getSelect()
                ->where('main_table.countries_list LIKE \'%'. $country .'%\' ');
        
        return $this;
    }
    
    public function addStatusFilter($isActive = true) {
        
        $this->getSelect()
                ->where('main_table.is_active = ? ', $isActive);
        
        return $this;
    }
    
    public function addPriorityFilter($dir = 'ASC') {
        
        $this->getSelect()
                ->order('main_table.priority '. $dir);
        
        return $this;
    }
    
    public function addLimit($limit = 1) {
        
        $this->getSelect()
                ->limit($limit);
        
        return $this;
    }
}