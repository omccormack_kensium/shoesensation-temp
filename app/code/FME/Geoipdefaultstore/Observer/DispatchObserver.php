<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace FME\Geoipdefaultstore\Observer;

use Magento\Store\Model\Store;
use FME\Geoipdefaultstore\Model\CookieModel;

class DispatchObserver implements \Magento\Framework\Event\ObserverInterface {

    protected $_geoipdefaultstoreFactory;
    protected $_geoipdefaultstoreHelper;
    protected $_storeManager;
    protected $_activeGroup;
    protected $_layoutFactory;
    protected $_httpContext;

    /**
     * @var \Magento\Store\Api\StoreCookieManagerInterface
     */
    protected $_storeCookieManager;

    /**
     *
     * @var FME\Geoipdefaultstore\Model\CookieModel
     */
    protected $_cookieModel;

    public function __construct(\FME\Geoipdefaultstore\Model\GeoipdefaultstoreFactory $geoipdefaultstoreFactory, \FME\Geoipdefaultstore\Helper\Data $helper, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\View\Result\LayoutFactory $layoutFactory, \Magento\Store\Api\StoreCookieManagerInterface $storeCookieManager, \Magento\Framework\App\Http\Context $httpContext, CookieModel $cookieModel) {
        $this->_geoipdefaultstoreFactory = $geoipdefaultstoreFactory;
        $this->_geoipdefaultstoreHelper = $helper;
        $this->_storeManager = $storeManager;
        $this->_activeGroup = $this->_storeManager->getGroup();
        $this->_layoutFactory = $layoutFactory;
        $this->_storeCookieManager = $storeCookieManager;
        $this->_httpContext = $httpContext;
        $this->_cookieModel = $cookieModel;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {   //$this->_cookieModel->delete();
        
        if (!$this->_geoipdefaultstoreHelper->isEnabledInFrontend()) {
            return;
        }
        
        $remoteAddress = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\Http\PhpEnvironment\RemoteAddress');
        $visitorIp = $remoteAddress->getRemoteAddress(); //'12.130.147.244'; //
        
        if ($this->_geoipdefaultstoreHelper->isWebCrawler()) {
            return;
        }
		// exclude paypal from check
        if (in_array($visitorIp, $this->getPaypalIpList())) {
            return;
        }
        
        if ($this->_cookieModel->get() === '1') {
            return;
        }

        $request = $observer->getRequest(); //echo '<pre>';print_r($observer->getEvent()->getData());exit;
  
        $infoByIp = $this->_geoipdefaultstoreHelper->getInfoByIp($visitorIp); //echo '<pre>';print_r($infoByIp);exit;
        $geoipdefaultstore = $this->_geoipdefaultstoreFactory->create();
        $expression = new \Zend_Db_Expr('LOWER(countries_list)');
        $collection = $geoipdefaultstore->getCollection()
                ->addFieldToFilter('countries_list', ['neq' => ''])
                ->addFieldToFilter($expression, ['like' => '%' . strtolower($infoByIp['cn']) . '%'])
                ->addFieldToFilter($expression, ['nlike' => '%' . strtolower($infoByIp['cn']) . ' %']) // add space so the whole string matches
                ->addStatusFilter()
                ->addPriorityFilter()
                ->addLimit();
        /* @var object as the collection will always have one item */
        $item = $collection->getFirstItem(); //echo '<pre>'; print_r($item->getData()); exit;

        if ($item->getExceptionIps() != NULL) {
            $exceptionIps = $item->getExceptionIps();
            if ($this->_geoipdefaultstoreHelper->isIpAnException($visitorIp, $exceptionIps)) {
                $this->_cookieModel->set(1);
                return;
            }
        }
        /**
         * if the visitor and assigned store are same
         */
        if ($item->getStoreGroupId() == $this->_activeGroup->getId()) {
            $this->_cookieModel->set(1);
        }
        // if visitor is not already in the same store group assigned
        if ($item->getStoreGroupId() != $this->_activeGroup->getId()) {
            // set cookies
            $groupStore = $this->_storeManager->getGroup($item->getStoreGroupId());
            $groupDefaultStoreId = $groupStore->getDefaultStoreId();
            $groupDefaultStore = $this->_storeManager->getStore($groupDefaultStoreId);
            // get group default store view code
            $groupDefaultStoreCode = $groupDefaultStore->getCode();
            $this->_cookieModel->setCode($groupDefaultStoreCode);
            // if auto redirect enabled
            if ($this->_geoipdefaultstoreHelper->isAutoRedirectEnabled()) {
                $this->_httpContext->setValue(Store::ENTITY, $groupDefaultStoreCode, null);
                $this->_storeCookieManager->setStoreCookie($groupDefaultStore);
                //set the cookie
                $this->_cookieModel->set(1);
                $isFrontSecure = $groupDefaultStore->isFrontUrlSecure();
                $url = $groupDefaultStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_LINK, $isFrontSecure);
                $observer->getControllerAction()
                        ->getResponse()
                        ->setRedirect($url);
                return;
            }
        }
    }

	public function getPaypalIpList() {
        return [
            '173.0.81.1', // notify.paypal.com
            '173.0.81.33',
            '66.211.170.66',
            '173.0.84.8', //ipnpb.paypal.com
            '173.0.84.40',
            '173.0.88.8',
            '173.0.88.40',
            '173.0.92.8',
            '173.0.93.8',
            '64.4.249.8',
            '64.4.248.8',
            '173.0.88.66', //api.paypal.com
            '173.0.88.98',
            '173.0.84.66',
            '173.0.84.98',
            '66.211.168.91',
            '173.0.92.23',
            '173.0.93.23',
            '64.4.249.23',
            '64.4.248.23',
            '66.211.168.93', //reports.paypal.com
            '173.0.84.161',
            '173.0.84.198',
            '173.0.88.161',
            '173.0.88.198',
            '173.0.84.178', //mobile.paypal.com
            '173.0.84.212',
            '173.0.88.178',
            '173.0.88.212',
            '173.0.88.203', //m.paypal.com
            '173.0.84.171',
            '173.0.84.203',
            '173.0.88.171',
        ];
    }
}
